/*     */

/*     */
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ReporStatustHelper
/*     */ {
/*     */   private static final int STATUS_FREE = 0;
/*     */   private static final int STATUS_RENDER = 1;
/*     */   private static final int STATUS_EXPORT = 2;
/*     */   private static final int STATUS_FINISHED = 16;
/*  13 */   private final Object lock = new Object();
/*     */   private String pdfPath;
/*     */   private int part;
/*  16 */   private int pageCount = 0;
/*  17 */   private int partPageCount = 0;
/*     */   private int totalRenderedPages;
/*     */   private int partTotalRenderedPages;
/*     */   private int totalQueuedMessages;
/*     */   private int partTotalQueuedMessages;
/*  22 */   private int status = 0;
/*     */ 
/*     */ 
/*     */   
/*     */   public void nextPage() {
/*  27 */     synchronized (this.lock) {
/*  28 */       this.pageCount++;
/*  29 */       this.partPageCount++;
/*     */     } 
/*     */   }
/*     */   
/*     */   public String getFullPDFName() {
/*  34 */     return this.pdfPath + "\\" + this.part + ".pdf";
/*     */   }
/*     */   
/*     */   public boolean isFinished() {
/*  38 */     return (this.status == 16);
/*     */   }
/*     */   public boolean inRenderState() {
/*  41 */     return (this.status == 1);
/*     */   }
/*     */   
/*     */   public void startReder() {
/*  45 */     synchronized (this.lock) {
/*  46 */       this.totalRenderedPages = 0;
/*  47 */       this.status = 1;
/*     */     } 
/*     */   }
/*     */   public void startExport() {
/*  51 */     synchronized (this.lock) {
/*  52 */       this.status = 2;
/*     */     } 
/*     */   }
/*     */   public void finish() {
/*  56 */     synchronized (this.lock) {
/*  57 */       this.status = 16;
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public String getPdfPath() {
/*  66 */     return this.pdfPath;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPdfPath(String pdfPath) {
/*  73 */     this.pdfPath = pdfPath;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPart() {
/*  80 */     return this.part;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPart(int part) {
/*  87 */     this.part = part;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPageCount() {
/*  94 */     return this.pageCount;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPartPageCount() {
/* 101 */     return this.partPageCount;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getTotalRenderedPages() {
/* 108 */     return this.totalRenderedPages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setTotalRenderedPages(int totalRenderedPages) {
/* 115 */     this.totalRenderedPages = totalRenderedPages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPartTotalRenderedPages() {
/* 122 */     return this.partTotalRenderedPages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPartTotalRenderedPages(int partTotalRenderedPages) {
/* 129 */     synchronized (this.lock) {
/* 130 */       this.partTotalRenderedPages = partTotalRenderedPages;
/* 131 */       this.totalRenderedPages += partTotalRenderedPages;
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getTotalQueuedMessages() {
/* 139 */     return this.totalQueuedMessages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setTotalQueuedMessages(int totalQueuedMessages) {
/* 146 */     this.totalQueuedMessages = totalQueuedMessages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public int getPartTotalQueuedMessages() {
/* 153 */     return this.partTotalQueuedMessages;
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setPartTotalQueuedMessages(int partTotalQueuedMessages) {
/* 160 */     this.partTotalQueuedMessages = partTotalQueuedMessages;
/*     */   }
/*     */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\ReporStatustHelper.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */