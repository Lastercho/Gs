/*    */

/*    */
/*    */ public class ReportParamsEntity
/*    */ {
/*    */   private String groupSQL;
/*    */   private String reportTemplateName;
/*    */   private String reportSQLFileName;
/*    */   private String reportQuerySQL;
/*    */   private int reportQuerySQLParamCount;
/*    */   private int maxPagesInFile;
/*    */   private int pageSides;
/*    */   private int virtualizerPages;
/*    */   
/*    */   public String getGroupSQL() {
/* 15 */     return this.groupSQL;
/*    */   }
/*    */   
/*    */   public void setGroupSQL(String groupSQL) {
/* 19 */     this.groupSQL = groupSQL;
/*    */   }
/*    */   
/*    */   public String getReportTemplateName() {
/* 23 */     return this.reportTemplateName;
/*    */   }
/*    */   
/*    */   public void setReportTemplateName(String reportTemplateName) {
/* 27 */     this.reportTemplateName = reportTemplateName;
/*    */   }
/*    */   
/*    */   public String getReportSQLFileName() {
/* 31 */     return this.reportSQLFileName;
/*    */   }
/*    */   
/*    */   public void setReportSQLFileName(String reportSQLFileName) {
/* 35 */     this.reportSQLFileName = reportSQLFileName;
/*    */   }
/*    */   
/*    */   public String getReportQuerySQL() {
/* 39 */     return this.reportQuerySQL;
/*    */   }
/*    */   
/*    */   public void setReportQuerySQL(String reportQuerySQL) {
/* 43 */     this.reportQuerySQL = reportQuerySQL;
/*    */   }
/*    */   
/*    */   public int getReportQuerySQLParamCount() {
/* 47 */     return this.reportQuerySQLParamCount;
/*    */   }
/*    */   
/*    */   public void setReportQuerySQLParamCount(int reportQuerySQLParamCount) {
/* 51 */     this.reportQuerySQLParamCount = reportQuerySQLParamCount;
/*    */   }
/*    */   
/*    */   public int getMaxPagesInFile() {
/* 55 */     return this.maxPagesInFile;
/*    */   }
/*    */   
/*    */   public void setMaxPagesInFile(int maxPagesInFile) {
/* 59 */     this.maxPagesInFile = maxPagesInFile;
/*    */   }
/*    */   
/*    */   public int getPageSides() {
/* 63 */     return this.pageSides;
/*    */   }
/*    */   
/*    */   public void setPageSides(int pageSides) {
/* 67 */     this.pageSides = pageSides;
/*    */   }
/*    */   
/*    */   public int getVirtualizerPages() {
/* 71 */     return this.virtualizerPages;
/*    */   }
/*    */   
/*    */   public void setVirtualizerPages(int virtualizerPages) {
/* 75 */     this.virtualizerPages = virtualizerPages;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\ReportParamsEntity.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */