/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ 
/*    */ public class FillObjectDataDAO
/*    */   extends BaseYearDAO
/*    */ {
/*    */   public FillObjectDataDAO(Connection conn, int msgYear) {
/* 11 */     super(conn, msgYear);
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 18 */     String sql = "UPDATE dgp_msg.msg_zad z SET nasmi=adr.op_city,city_i_id=adr.op_ekkate,ulime=adr.op_address,\n\tzad_desc=zad_desc||' за имот ' || UPPER(d1.value) || ' с адрес: ' || (CASE WHEN coalesce(adr.op_address,'')!='' THEN adr.op_address || ', ' ELSE '' END) || adr.op_city || ' община ' || adr.op_muniname\n\tFROM taxobject t\n\tJOIN decode d1 ON upper(d1.columnname) = 'KINDPROPERTY' and d1.code = t.kindproperty\n\tJOIN dgp_msg.getbestaddres(t.address_id, null,null) adr ON true\n\tWHERE t.taxobject_id=z.taxobject_id AND z.msgyear=" + this.msgYear + " AND z.sortcode=1 AND t.kindtaxobject='1'";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 24 */     if (this.showSQL) {
/* 25 */       System.out.println(sql);
/*    */     }
/* 27 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 28 */     stzad.execute();
/* 29 */     stzad.close();
/* 30 */     this.dbconn.commit();
/*    */     
/* 32 */     sql = "UPDATE dgp_msg.msg_zad z SET zad_desc=coalesce(zad_desc,'')||' за ' || LOWER(tmr.name) ||(CASE WHEN coalesce(cmar.mark_name,'')='' OR cmar.mark_name like 'НЕИЗ%' THEN '' ELSE ' '||cmar.mark_name END) || (CASE WHEN coalesce(cmod.model_name,'') in ('','НЕУКАЗАН','НЕИЗВЕСТЕН') THEN '' ELSE ' '||cmod.model_name END) || (CASE WHEN coalesce(t.registerno,'')='' THEN '' ELSE ', рег.№ '||t.registerno\tEND) || (CASE WHEN trim(coalesce(t.registername,''))='' THEN '' ELSE ' ('||trim(t.registername)||' )' END) \n   FROM taxobject t\n   JOIN transpmeansreg tmr ON tmr.transpmeansreg_id=t.transpmeansreg_id\n   LEFT JOIN carreg cr ON cr.carreg_id=t.carreg_id\n   LEFT JOIN carmodel cmod ON cmod.carmodel_id=cr.carmodel_id\n   LEFT JOIN carmark cmar ON cmar.carmark_id=cmod.carmark_id\n   WHERE t.taxobject_id=z.taxobject_id AND t.kindtaxobject='2' AND z.msgyear=" + this.msgYear + " AND (z.sortcode=2 OR (z.sortcode=3 AND z.paycode='3400'))";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 39 */     if (this.showSQL) {
/* 40 */       System.out.println(sql);
/*    */     }
/* 42 */     stzad = this.dbconn.prepareCall(sql);
/* 43 */     stzad.execute();
/* 44 */     stzad.close();
/* 45 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\FillObjectDataDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */