/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class RadioKanaliMalkoTarnovoZadalDAO
/*    */   extends OldZadDAO
/*    */ {
/*    */   public RadioKanaliMalkoTarnovoZadalDAO(Connection conn, int msgYear, boolean byYear) {
/* 10 */     super(conn, msgYear, byYear);
/*    */   }
/*    */ 
/*    */   
/*    */   public void setFilterByExistingZad(int filterByExistingZad) throws Exception {
/* 15 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 21 */     String sql = resourceToString("fill_radiokanal_mtarnovo.sql");
/* 22 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 23 */     sql = sql.replace("{:DAVNOST}", getDavnostSQL());
/* 24 */     sql = sql.replace("{:P_BY_YEAR}", this.byYear ? "1" : "0");
/* 25 */     if (this.showSQL) {
/* 26 */       System.out.println(sql);
/*    */     }
/* 28 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 29 */     stzad.execute();
/* 30 */     stzad.close();
/* 31 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\RadioKanaliMalkoTarnovoZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */