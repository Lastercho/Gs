/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class MsgNumberingDAO
/*    */   extends BaseYearDAO {
/*    */   public MsgNumberingDAO(Connection conn, int msgYear) {
/*  9 */     super(conn, msgYear);
/*    */   }
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 14 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ 
/*    */   
/*    */   public void numbering() throws Exception {
/* 19 */     String sql = resourceToString("msg_numbering.sql");
/* 20 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 21 */     if (this.showSQL) {
/* 22 */       System.out.println(sql);
/*    */     }
/* 24 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 25 */     stzad.execute();
/* 26 */     stzad.close();
/* 27 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void numberingByYear() throws Exception {
/* 32 */     String sql = resourceToString("msg_numbering_by_year.sql");
/* 33 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 34 */     if (this.showSQL) {
/* 35 */       System.out.println(sql);
/*    */     }
/* 37 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 38 */     stzad.execute();
/* 39 */     stzad.close();
/* 40 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\MsgNumberingDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */