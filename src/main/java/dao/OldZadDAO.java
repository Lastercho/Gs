/*    */ package dao;
/*    */ 
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public abstract class OldZadDAO
/*    */   extends BaseYearDAO {
/*    */   private boolean filterDavnostByYear = false;
/*    */   protected boolean byYear = false;
/*  9 */   protected int filterByExistingZad = 0;
/* 10 */   protected int davnostFalg = 0;
/*    */   
/*    */   public static final int DABNOST_NONE = 0;
/*    */   
/*    */   public static final int DAVNOST_10YEAR = 1;
/*    */   public static final int DAVNOST_OTPISANI = 2;
/*    */   public static final int DAVNOST_OTPISANI_AND_10YEAR = 3;
/*    */   public static final int DAVNOST_ALL = 4;
/*    */   
/*    */   public OldZadDAO(Connection conn, int msgYear, boolean byYear) {
/* 20 */     super(conn, msgYear);
/* 21 */     this.byYear = byYear;
/*    */   }
/*    */ 
/*    */   
/*    */   protected String getDavnostSQL() {
/* 26 */     switch (this.davnostFalg)
/*    */     { case 0:
/* 28 */
        String sql = "";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */         
/* 56 */         return sql;case 1: sql = " AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.kind=2 AND f.debtsubject_id=ds.debtsubject_id) "; if (this.filterDavnostByYear) sql = sql + " AND extract(year from ds.tax_begidate)>=" + (this.msgYear - 10);  return sql;case 2: sql = " AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.kind=4 AND f.debtsubject_id=ds.debtsubject_id) "; return sql;case 3: sql = " AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.kind in (2,4) AND f.debtsubject_id=ds.debtsubject_id) "; if (this.filterDavnostByYear) sql = sql + " AND extract(year from ds.tax_begidate)>=" + (this.msgYear - 10);  return sql; }  String sql = " AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.debtsubject_id=ds.debtsubject_id) "; if (this.filterDavnostByYear) sql = sql + " AND extract(year from ds.tax_begidate)>=" + (this.msgYear - 5);  return sql;
/*    */   }
/*    */   
/*    */   public void setDanvostFlag(int davnost) throws Exception {
/* 60 */     if (davnost > 4 || davnost < 0) {
/* 61 */       throw new Exception("Флага за давност е извън разрешение диапазон");
/*    */     }
/* 63 */     this.davnostFalg = davnost;
/*    */   }
/*    */   public int getDavnostFlag() {
/* 66 */     return this.davnostFalg;
/*    */   }
/*    */   
/*    */   public int getFilterByExistingZad() {
/* 70 */     return this.filterByExistingZad;
/*    */   }
/*    */   
/*    */   public boolean isByYear() {
/* 74 */     return this.byYear;
/*    */   }
/*    */   
/*    */   public abstract void setFilterByExistingZad(int paramInt) throws Exception;
/*    */   
/*    */   public boolean isFilterDavnostByYear() {
/* 80 */     return this.filterDavnostByYear;
/*    */   }
/*    */   
/*    */   public void setFilterDavnostByYear(boolean filterDavnostByYear) {
/* 84 */     this.filterDavnostByYear = filterDavnostByYear;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\OldZadDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */