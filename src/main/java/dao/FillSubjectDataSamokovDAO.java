/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FillSubjectDataSamokovDAO
/*    */   extends FillSubjectDataDAO
/*    */ {
/*    */   public FillSubjectDataSamokovDAO(Connection conn, int msgYear, long muniID, boolean useAlternativeSendMsgMethod, int addressType) throws Exception {
/* 20 */     super(conn, msgYear, muniID, useAlternativeSendMsgMethod, addressType);
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 27 */     String sql = "INSERT INTO dgp_msg.msg_lica (taxsubject_id,msgyear,ein,isperson,isdead ,\n                        ime,cityname,muniname,city_lice_id,ulimel,dscode,myname,\n                        pin,badaddress,email,other_msg_code) \n\tSELECT ts.taxsubject_id,{:P_YEAR},ts.idn,(CASE WHEN coalesce(ts.isperson,0)='1' THEN 1 ELSE 0 END),coalesce((select 1 from person p where p.taxsubject_id=ts.taxsubject_id and (trim(coalesce(p.dead_actno,''))!='' or p.dead_date is not null) order by user_date desc limit 1),0) ,\n\t\ttrim(coalesce(ts.name,'')),coalesce(adr.op_city,'*'),coalesce(adr.op_muniname,'*'),coalesce(adr.op_ekkate,-1),adr.op_address,coalesce((CASE WHEN adr.op_muni_id={:P_MUNI_ID} THEN adr.op_muni_old_code ELSE adr.op_province_ekatte||'00' END ),'*'),'***',\n\t\tcoalesce(ts.pin,''),(CASE WHEN adr.op_isbadaddr=1 OR coalesce(ts.name,'')='' OR coalesce(adr.op_city,'')='' THEN 1 ELSE 0 END),(CASE WHEN {:P_ALTERNATIVE_METHODS}>0 AND coalesce(ts.e_mail,'')!='' THEN ts.e_mail ELSE null END),(CASE WHEN {:P_ALTERNATIVE_METHODS}>0 THEN coalesce(ts.year_message,0) ELSE 0 END)\n FROM taxsubject ts\n JOIN dgp_msg.getbestaddres({:GETADDRESS_FUNCPARAM}) adr ON true\n WHERE ts.taxsubject_id IN (SELECT z.taxsubject_id FROM dgp_msg.msg_zad z WHERE z.msgyear={:P_YEAR} GROUP BY z.taxsubject_id) ;";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 36 */     sql = sql.replace("{:P_YEAR}", Integer.toString(this.msgYear));
/* 37 */     sql = sql.replace("{:P_MUNI_ID}", Long.toString(this.muniId));
/* 38 */     sql = sql.replace("{:P_ALTERNATIVE_METHODS}", this.useAlternativeSendMsgMethod ? "1" : "0");
/* 39 */     if (this.addressType == 0)
/*    */     {
/* 41 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", "ts.present_clientaddr_id,(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.post_clientaddr_id"); } 
/* 42 */     if (this.addressType == 1) {
/*    */       
/* 44 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", "(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.present_clientaddr_id,ts.post_clientaddr_id");
/*    */     } else {
/*    */       
/* 47 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", " ts.post_clientaddr_id,(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.present_clientaddr_id  ");
/*    */     } 
/* 49 */     if (this.showSQL) {
/* 50 */       System.out.println(sql);
/*    */     }
/* 52 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 53 */     stzad.execute();
/* 54 */     stzad.close();
/* 55 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\FillSubjectDataSamokovDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */