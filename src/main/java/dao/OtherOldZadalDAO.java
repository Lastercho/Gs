/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class OtherOldZadalDAO
/*    */   extends OldZadDAO {
/*    */   private String payCodes;
/*    */   public static final int FILTER_NONE = 0;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT = 1;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT_MPS = 2;
/*    */   
/*    */   public OtherOldZadalDAO(Connection conn, int msgYear, boolean byYear, String payCodes) {
/* 14 */     super(conn, msgYear, byYear);
/* 15 */     this.payCodes = payCodes;
/*    */   }
/*    */ 
/*    */   
/*    */   public void setFilterByExistingZad(int filterByExistingZad) throws Exception {
/* 20 */     if (filterByExistingZad > 2 || filterByExistingZad < 0) {
/* 21 */       throw new Exception("Флага за филтриране на стари задължения е извън разрешение диапазон");
/*    */     }
/* 23 */     this.filterByExistingZad = filterByExistingZad;
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 29 */     String sql = resourceToString("fill_other_old.sql");
/* 30 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 31 */     sql = sql.replace("{:DAVNOST}", getDavnostSQL());
/* 32 */     sql = sql.replace("{:PAYCODES}", this.payCodes);
/* 33 */     switch (this.filterByExistingZad) {
/*    */       
/*    */       case 1:
/* 36 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode=1 AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       
/*    */       case 2:
/* 40 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode in (1,2) AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       default:
/* 43 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "");
/*    */         break;
/*    */     } 
/* 46 */     sql = sql.replace("{:P_BY_YEAR}", this.byYear ? "1" : "0");
/* 47 */     if (this.showSQL) {
/* 48 */       System.out.println(sql);
/*    */     }
/* 50 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 51 */     stzad.execute();
/* 52 */     stzad.close();
/* 53 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\OtherOldZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */