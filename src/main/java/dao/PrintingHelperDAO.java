/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class PrintingHelperDAO
/*    */   extends BaseYearDAO
/*    */ {
/*    */   public PrintingHelperDAO(Connection conn, int msgYear) {
/* 10 */     super(conn, msgYear);
/*    */   }
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 15 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void markSinglesPages(int maxZadal) throws Exception {
/* 21 */     String sql = "UPDATE dgp_msg.msg_lica SET pages=1 WHERE msgyear=? AND taxsubject_id IN (SELECT l.taxsubject_id FROM dgp_msg.msg_lica l WHERE l.msgyear=? and l.badaddress=0 GROUP BY l.taxsubject_id,l.msgyear HAVING (    SELECT sum(CASE WHEN z.sortcode=1 THEN 1.5 ELSE 1 END) \tFROM dgp_msg.msg_zad z \tWHERE z.msgyear=l.msgyear AND z.taxsubject_id=l.taxsubject_id AND z.msgnom>0 )<?  )";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 33 */     if (this.showSQL) {
/* 34 */       System.out.println(sql);
/*    */     }
/* 36 */     CallableStatement st = this.dbconn.prepareCall(sql);
/* 37 */     st.setInt(1, this.msgYear);
/* 38 */     st.setInt(2, this.msgYear);
/* 39 */     st.setInt(3, maxZadal);
/* 40 */     st.execute();
/* 41 */     st.close();
/* 42 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\PrintingHelperDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */