/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class MPSOldZadDAO extends OldZadDAO {
/*    */   public static final int FILTER_NONE = 0;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT_MPS = 1;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_MPS = 2;
/*    */   
/*    */   public MPSOldZadDAO(Connection conn, int msgYear, boolean byYear) {
/* 12 */     super(conn, msgYear, byYear);
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 18 */     String sql = resourceToString("fill_mps_old.sql");
/* 19 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 20 */     sql = sql.replace("{:DAVNOST}", getDavnostSQL());
/* 21 */     switch (this.filterByExistingZad) {
/*    */       
/*    */       case 1:
/* 24 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode in (1,2) AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       
/*    */       case 2:
/* 28 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode=2 AND zi.foryear=zi.msgyear AND coalesce(zi.parnom,0)=coalesce(ds.partidano,0))");
/*    */         break;
/*    */       default:
/* 31 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "");
/*    */         break;
/*    */     } 
/* 34 */     sql = sql.replace("{:P_BY_YEAR}", this.byYear ? "1" : "0");
/* 35 */     if (this.showSQL) {
/* 36 */       System.out.println(sql);
/*    */     }
/* 38 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 39 */     stzad.execute();
/* 40 */     stzad.close();
/* 41 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void setFilterByExistingZad(int filterByExistingZad) throws Exception {
/* 46 */     if (filterByExistingZad > 2 || filterByExistingZad < 0) {
/* 47 */       throw new Exception("Флага за филтриране на стари задължения при съществуващи за тек.година е извън разрешение диапазон");
/*    */     }
/* 49 */     this.filterByExistingZad = filterByExistingZad;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\MPSOldZadDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */