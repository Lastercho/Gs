package dao;

import java.sql.Connection;


public class CleanDAO
  extends BaseYearDAO
{
  public CleanDAO(Connection conn, int msgYear) {
    super(conn, msgYear);
  }


  public void execute() throws Exception {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void clean() throws Exception {
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_lica WHERE msgyear=" + this.msgYear).executeUpdate();
    this.dbconn.commit();
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear).executeUpdate();
    this.dbconn.commit();
  }

  public void deleteUnderValue(String minValue) throws Exception {
    this.dbconn.createStatement().executeUpdate("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (SELECT taxsubject_id FROM dgp_msg.msg_zad z WHERE z.msgyear=" + this.msgYear + " GROUP BY taxsubject_id HAVING sum(z.dan+z.smet)<0.001 AND sum(z.dan_nedd+z.smet_nedd)<" + minValue + " )");
    this.dbconn.commit();
  }


  public void deleteUnderValueOtherCurYear(String minValue) throws Exception {
    this.dbconn.createStatement().executeUpdate("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (SELECT taxsubject_id FROM dgp_msg.msg_zad z WHERE z.msgyear=" + this.msgYear + " GROUP BY taxsubject_id HAVING sum(z.dan+z.smet+z.dan_nedd+z.smet_nedd)<" + minValue + " AND min(z.sortcode)=3 AND max(z.sortcode)=3)");
    this.dbconn.commit();
  }

  public void deleteMunicipalityZadal() throws Exception {
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (SELECT ts.taxsubject_id FROM taxsubject ts WHERE left(idn,9)=(SELECT LEFT(idn,9)  FROM taxsubject WHERE taxsubject_id=(SELECT configvalue FROM config WHERE UPPER(name)='MUN_TAXSUBJECT_ID')::numeric))").executeUpdate();
    this.dbconn.commit();
  }
  public void deleteOtherSobst() throws Exception {
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (SELECT ts.taxsubject_id FROM taxsubject ts WHERE idn='9999990000001')").executeUpdate();
    this.dbconn.commit();
  }

  public void deleteDead() throws Exception {
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (SELECT p.taxsubject_id FROM person p WHERE trim(coalesce(p.dead_actno,''))!='')").executeUpdate();
    this.dbconn.commit();
  }

  public void cleanPages() throws Exception {
    this.dbconn.prepareStatement("UPDATE dgp_msg.msg_lica SET pages=0 WHERE msgyear=" + this.msgYear).executeUpdate();
    this.dbconn.commit();
  }



  public void cleanMPSCurrentYearOnly() throws Exception {
    String sql = "DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " and taxsubject_id in (\n" + "\tSELECT z.taxsubject_id\n" + "\tFROM  dgp_msg.msg_zad z\n" + "\tJOIN  dgp_msg.msg_lica l ON l.taxsubject_id=z.taxsubject_id and l.msgyear=z.msgyear\n" + "\tWHERE z.msgyear=" + this.msgYear + " AND l.other_msg_code<2 \n" + "\tGROUP by z.taxsubject_id\n" + "\tHAVING sum(CASE WHEN sortcode=2 THEN 1 ELSE 0 END)=count(*) AND sum(coalesce(z.dan_nedd,0)+coalesce(z.dan_lih,0)+coalesce(z.smet_nedd,0)+coalesce(z.smet_lih,0))=0\n" + ")";







    this.dbconn.prepareStatement(sql).executeUpdate();
    this.dbconn.commit();
    cleanSubjectWithoutZad();
  }


  public void cleanWithoutImot() throws Exception {
    String sql = "DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " and taxsubject_id in (\n" + "\tSELECT taxsubject_id FROM dgp_msg.msg_zad z WHERE z.msgyear=" + this.msgYear + " GROUP BY taxsubject_id HAVING sum(CASE WHEN sortcode=1 THEN 1 ELSE 0 END)=0 " + ")";


    this.dbconn.prepareStatement(sql).executeUpdate();
    this.dbconn.commit();
    cleanSubjectWithoutZad();
  }


  public void cleanSubjectWithoutZad() throws Exception {
    this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_lica l  WHERE l.msgyear=" + this.msgYear + " AND NOT EXISTS (SELECT 1 FROM dgp_msg.msg_zad z WHERE z.taxsubject_id=l.taxsubject_id AND z.msgyear=l.msgyear)").executeUpdate();
    this.dbconn.commit();
  }


  public void cleanALlUnderValue10() throws Exception {
    String sql = "DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " and taxsubject_id in (\n" + "\tselect taxsubject_id FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " group by taxsubject_id having sum(coalesce(dan,0)+coalesce(smet,0)+coalesce(dan_nedd,0)+coalesce(smet_nedd,0)+coalesce(dan_lih,0)+coalesce(smet_lih,0))<10 \n" + ")";


    this.dbconn.prepareStatement(sql).executeUpdate();
    this.dbconn.commit();
  }


  public void groupNedobor() throws Exception {
    String sql = resourceToString("group_nedobor.sql");
    sql = sql.replace("{:P_YEAR}", this.msgYear + "");
    if (this.showSQL) {
      System.out.println(sql);
    }
    this.dbconn.prepareStatement(sql).execute();
    this.dbconn.commit();
  }



  public void markChujdenciSamokov() throws Exception {
    String sql = "update dgp_msg.msg_lica set badaddress=1,chujdenec=1 where msgyear=" + this.msgYear + " and taxsubject_id in (7229,7230,7231,7249,7408,7409,7424,7427,7507,7581,7582,7583,7594,7600,7603,7649,7650,7654,7655,7661,7664,7666,7667,7676,7679,7726,7729,7771,7792,7793,108947,7799,7801,7803,7842,7846,7893,7894,7913,7914,7915,7921,7930,7951,7964,7965,7967,7969,7973,7979,7984,7985,7986,7988,7990,7991,7992,7994,7995,7996,7997,7998,7999,8001,8006,8007,8008,8009,8010,8011,8014,8017,8020,8021,8023,8027,8028,8030,8032,8034,8036,8037,8042,8044,8045,8046,8047,8048,8049,8050,8051,8055,8059,8063,8064,8065,8066,8070,8071,8074,8105,8111,8114,8132,8148,8155,8157,8176,8189,8196,8270,8374,8544,8669,8677,8679,8823,8834,8845,8922,8925,8931,8959,8989,8992,8994,9010,9030,9041,9044,9058,9060,9080,9082,9083,9085,9102,9110,9124,9128,9129,9138,9159,9160,9162,9166,9168,9170,9172,9173,9174,9175,9176,9181,9183,9202,9205,9209,9214,9217,9219,9220,9222,9223,9246,9248,9253,9254,9259,9267,9268,9273,9274,9275,9279,9287,9294,9295,9298,9304,9310,9315,9316,9324,9326,9336,9340,9342,9343,9345,9347,9349,9382,9384,9390,9392,9403,9404,9406,9422,9424,9425,9426,9427,9429,9447,9449,9450,9458,9472,9473,9476,9477,9479,9488,9495,9496,9498,9500,9501,9503,9509,9513,9515,9516,9536,9537,9539,9540,9552,9556,9560,9561,9566,9574,9575,9579,9580,9581,9582,9584,9585,9596,9597,9600,9604,9605,9606,9608,9612,9614,9616,9622,9623,9625,9627,9630,9633,9635,9637,9640,9641,9643,9644,9647,9656,9661,9663,9669,9675,9679,9681,9682,9683,9684,9690,9696,9697,9701,9705,9706,9708,9710,9711,9712,9715,9716,9718,9720,9721,9722,9723,9724,9725,9731,9733,9739,9741,9743,9771,9774,9783,9793,9798,9807,9808,9813,9814,9822,9825,9827,9828,9829,9830,9832,9834,9836,9838,9842,9843,9844,9846,9848,9850,9854,9859,9860,9866,9867,9870,9872,9883,9884,9896,9900,9901,9902,9907,9908,9909,9911,9913,9914,9915,9916,9917,9919,9920,9923,9925,9926,9927,9928,9933,9934,9935,9936,9937,9941,9942,9943,9944,9947,9949,9951,9952,9953,9957,9959,9960,9961,9962,9963,9970,9974,9975,9978,9999,10148,10322,10324,10325,10326,10332,10333,10343,10448,10536,10601,10669,10689,10693,10709,10712,10715,10716,10718,10720,10721,10722,10920,10926,11421,12552,13442,16601,20101,21199,17777,24838,30764,29466,34872,29972,28826,24406,24363,34159,31440,29949,30743,33432,32381,27770,48133,36847,45164,48146,42980,43247,37730,37008,37990,47372,42894,39683,40434,47200,43446,53884,55997,59767,49437,49959,58245,53009,59529,59341,54591,56942,56476,52730,55982,52663,53617,53169,52434,56809,57529,50327,59564,57770,59839,60663,55758,51914,56925,51970,51901,64800,61923,71837,68517,108896,71686,61917,64900,65621,68713,68441,67873,66558,63598,66266,65299,64715,61427,62482,60798,72871,74691,73176,72509,74713,73123,16229,14715,10709,10343,10718,10448,10669,13442,10722,10693,12552) ";
    if (this.showSQL) {
      System.out.println(sql);
    }
    this.dbconn.prepareStatement(sql).execute();
    this.dbconn.commit();
  }
}


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\CleanDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */