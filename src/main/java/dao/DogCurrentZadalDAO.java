/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class DogCurrentZadalDAO
/*    */   extends CurrentZadalDAO
/*    */ {
/*    */   private int filterByExistingZad;
/*    */   public static final int FILTER_NONE = 0;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT = 1;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT_MPS = 2;
/*    */   
/*    */   public DogCurrentZadalDAO(Connection conn, int msgYear, int filterByzad) throws Exception {
/* 15 */     super(conn, msgYear);
/* 16 */     if (filterByzad < 0 || filterByzad > 2) {
/* 17 */       throw new Exception("Фълтърът за задължения е извън обхват - кучета текуща година");
/*    */     }
/* 19 */     this.filterByExistingZad = filterByzad;
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 25 */     String sql = resourceToString("fill_dog_current.sql");
/* 26 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 27 */     sql = sql.replace("{:P_MODE}", this.byOstatak ? "1" : "0");
/* 28 */     if (this.forDeklar) {
/* 29 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND ds.kinddoc=1");
/*    */     } else {
/* 31 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND coalesce(ds.kinddoc,0)!=1");
/*    */     } 
/* 33 */     switch (this.filterByExistingZad) {
/*    */       
/*    */       case 1:
/* 36 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode=1 AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       
/*    */       case 2:
/* 40 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode in (1,2) AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       default:
/* 43 */         sql = sql.replace("{:ONLY_WITH_IMOT_MPS}", "");
/*    */         break;
/*    */     } 
/* 46 */     if (this.showSQL) {
/* 47 */       System.out.println(sql);
/*    */     }
/* 49 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 50 */     stzad.execute();
/* 51 */     stzad.close();
/* 52 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void deleteOnlyDogNovaZagora() throws Exception {
/* 57 */     this.dbconn.prepareStatement("DELETE FROM dgp_msg.msg_zad WHERE msgyear=" + this.msgYear + " AND taxsubject_id in (select taxsubject_id FROM (SELECT taxsubject_id,sum(dan+smet+dan_nedd+smet_nedd) totalzad,sum(CASE WHEN paycode='8013' THEN dan ELSE 0 END) dog_cur_zad FROM dgp_msg.msg_zad z WHERE z.msgyear=" + this.msgYear + " GROUP BY taxsubject_id) q1 WHERE totalzad=dog_cur_zad)").executeUpdate();
/* 58 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\DogCurrentZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */