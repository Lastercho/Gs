/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class GarageSredecZadalDAO
/*    */   extends OldZadDAO {
/*    */   public GarageSredecZadalDAO(Connection conn, int msgYear, boolean byYear) {
/*  9 */     super(conn, msgYear, byYear);
/*    */   }
/*    */ 
/*    */   
/*    */   public void setFilterByExistingZad(int filterByExistingZad) throws Exception {
/* 14 */     throw new UnsupportedOperationException("Not supported yet.");
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 20 */     String sql = resourceToString("fill_other_sredec.sql");
/* 21 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 22 */     sql = sql.replace("{:DAVNOST}", getDavnostSQL());
/* 23 */     sql = sql.replace("{:P_BY_YEAR}", this.byYear ? "1" : "0");
/* 24 */     if (this.showSQL) {
/* 25 */       System.out.println(sql);
/*    */     }
/* 27 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 28 */     stzad.execute();
/* 29 */     stzad.close();
/* 30 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\GarageSredecZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */