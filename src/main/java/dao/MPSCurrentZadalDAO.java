/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class MPSCurrentZadalDAO
/*    */   extends CurrentZadalDAO {
/*    */   private boolean addOnlyWithImot = false;
/*    */   
/*    */   public MPSCurrentZadalDAO(Connection conn, int msgYear, boolean addOnlyWithImot) {
/* 11 */     super(conn, msgYear);
/* 12 */     this.addOnlyWithImot = addOnlyWithImot;
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 18 */     String sql = resourceToString("fill_mps_current.sql");
/* 19 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 20 */     sql = sql.replace("{:P_MODE}", this.byOstatak ? "1" : "0");
/* 21 */     if (this.addOnlyWithImot) {
/*    */       
/* 23 */       sql = sql.replace("{:ONLY_WITH_IMOT}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode=1 AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */     } else {
/* 25 */       sql = sql.replace("{:ONLY_WITH_IMOT}", "");
/*    */     } 
/* 27 */     if (this.forDeklar) {
/* 28 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND ds.kinddoc=1");
/*    */     } else {
/* 30 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND coalesce(ds.kinddoc,0)!=1");
/*    */     } 
/* 32 */     if (this.showSQL) {
/* 33 */       System.out.println(sql);
/*    */     }
/* 35 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 36 */     stzad.execute();
/* 37 */     stzad.close();
/* 38 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void moveSecondPayPeriodToFirst() throws Exception {
/* 43 */     this.dbconn.prepareStatement(" UPDATE dgp_msg.msg_zad z SET dan_1=z.dan_2,dan_2=0,srok1_1=z.srok1_2,srok1_2=null WHERE z.msgyear=" + this.msgYear + " AND z.sortcode=2 AND z.foryear=z.msgyear AND z.srok1_1 is null AND z.dan5>0 AND z.dan>0 AND z.dan_1=0 AND z.dan=z.dan_2 AND z.srok1_2<=to_date('30.04.'||z.msgyear,'dd.mm.yyyy')").executeUpdate();
/* 44 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\MPSCurrentZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */