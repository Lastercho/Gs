/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class FillSubjectDataDAO
/*    */   extends BaseYearDAO
/*    */ {
/*    */   protected final long muniId;
/*    */   protected final boolean useAlternativeSendMsgMethod;
/*    */   protected final int addressType;
/*    */   public static final int USE_PRESENT_ADDRESS = 0;
/*    */   public static final int USE_PERMANENT_ADDRESS = 1;
/*    */   public static final int USE_POST_ADDRESS = 2;
/*    */   
/*    */   public FillSubjectDataDAO(Connection conn, int msgYear, long muniID, boolean useAlternativeSendMsgMethod, int addressType) throws Exception {
/* 17 */     super(conn, msgYear);
/* 18 */     this.muniId = muniID;
/* 19 */     this.useAlternativeSendMsgMethod = useAlternativeSendMsgMethod;
/* 20 */     this.addressType = addressType;
/* 21 */     if (addressType < 0 || addressType > 2) {
/* 22 */       throw new Exception("Грешен тип адрес");
/*    */     }
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 42 */     String sql = "INSERT INTO dgp_msg.msg_lica (taxsubject_id,msgyear,ein,isperson,isdead ,\n                        ime,cityname,muniname,city_lice_id,ulimel,dscode,myname,\n                        pin,badaddress,email,other_msg_code) \n\tSELECT ts.taxsubject_id,{:P_YEAR},ts.idn,(CASE WHEN coalesce(ts.isperson,0)='1' THEN 1 ELSE 0 END),coalesce((select 1 from person p where p.taxsubject_id=ts.taxsubject_id and trim(coalesce(p.dead_actno,''))!='' order by user_date desc limit 1),0) ,\n\t\ttrim(coalesce(ts.name,'')),coalesce(adr.op_city,'*'),coalesce(adr.op_muniname,'*'),coalesce(adr.op_ekkate,-1),adr.op_address,coalesce((CASE WHEN adr.op_muni_id={:P_MUNI_ID} THEN adr.op_muni_old_code ELSE adr.op_province_ekatte||'00' END ),'*'),'***',\n\t\tcoalesce(ts.pin,''),(CASE WHEN adr.op_isbadaddr=1 OR coalesce(ts.name,'')='' OR coalesce(adr.op_city,'')='' THEN 1 ELSE 0 END),(CASE WHEN {:P_ALTERNATIVE_METHODS}>0 AND coalesce(ts.e_mail,'')!='' THEN ts.e_mail ELSE null END),(CASE WHEN {:P_ALTERNATIVE_METHODS}>0 THEN coalesce(ts.year_message,0) ELSE 0 END)\n FROM taxsubject ts\n JOIN dgp_msg.getbestaddres({:GETADDRESS_FUNCPARAM}) adr ON true\n WHERE ts.taxsubject_id IN (SELECT z.taxsubject_id FROM dgp_msg.msg_zad z WHERE z.msgyear={:P_YEAR} GROUP BY z.taxsubject_id) ;";
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */     
/* 51 */     sql = sql.replace("{:P_YEAR}", Integer.toString(this.msgYear));
/* 52 */     sql = sql.replace("{:P_MUNI_ID}", Long.toString(this.muniId));
/* 53 */     sql = sql.replace("{:P_ALTERNATIVE_METHODS}", this.useAlternativeSendMsgMethod ? "1" : "0");
/* 54 */     if (this.addressType == 0)
/*    */     {
/* 56 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", "ts.present_clientaddr_id,(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.post_clientaddr_id"); } 
/* 57 */     if (this.addressType == 1) {
/*    */       
/* 59 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", "(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.present_clientaddr_id,ts.post_clientaddr_id");
/*    */     } else {
/*    */       
/* 62 */       sql = sql.replace("{:GETADDRESS_FUNCPARAM}", " ts.post_clientaddr_id,(CASE WHEN coalesce(ts.permanent_addres_exist,0)=1 THEN ts.permanent_clientaddr_id ELSE null END),ts.present_clientaddr_id  ");
/*    */     } 
/* 64 */     if (this.showSQL) {
/* 65 */       System.out.println(sql);
/*    */     }
/* 67 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 68 */     stzad.execute();
/* 69 */     stzad.close();
/* 70 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void setOtherMsgCodeBurgas() throws Exception {
/* 75 */     if (this.useAlternativeSendMsgMethod) {
/* 76 */       String sql = "UPDATE dgp_msg .msg_lica SET other_msg_code=2 WHERE msgyear={:P_YEAR} AND email IS NOT NULL AND coalesce(other_msg_code,0)!=2";
/* 77 */       sql = sql.replace("{:P_YEAR}", Integer.toString(this.msgYear));
/* 78 */       if (this.showSQL) {
/* 79 */         System.out.println(sql);
/*    */       }
/* 81 */       CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 82 */       stzad.execute();
/* 83 */       stzad.close();
/* 84 */       this.dbconn.commit();
/*    */     } 
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\FillSubjectDataDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */