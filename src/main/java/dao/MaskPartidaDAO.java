/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class MaskPartidaDAO
/*    */   extends BaseYearDAO {
/*    */   private final String ebkCOde;
/*    */   
/*    */   public MaskPartidaDAO(Connection conn, int msgYear, String ebkCode) {
/* 11 */     super(conn, msgYear);
/* 12 */     this.ebkCOde = ebkCode;
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 18 */     String sql = resourceToString("mask_partida.sql");
/* 19 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 20 */     sql = sql.replace("{:P_EBK_CODE}", this.ebkCOde);
/* 21 */     if (this.showSQL) {
/* 22 */       System.out.println(sql);
/*    */     }
/* 24 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 25 */     stzad.execute();
/* 26 */     stzad.close();
/* 27 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\MaskPartidaDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */