/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public class ImotOldZadalDAO
/*    */   extends OldZadDAO
/*    */ {
/*    */   public static final int FILTER_NONE = 0;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT_MPS = 1;
/*    */   public static final int FILTER_HAS_CURRENT_YEAR_IMOT = 2;
/*    */   
/*    */   public ImotOldZadalDAO(Connection conn, int msgYear, boolean byYear) {
/* 14 */     super(conn, msgYear, byYear);
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 20 */     String sql = resourceToString("fill_imoti_old.sql");
/* 21 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 22 */     sql = sql.replace("{:DAVNOST}", getDavnostSQL());
/* 23 */     switch (this.filterByExistingZad) {
/*    */       
/*    */       case 1:
/* 26 */         sql = sql.replace("{:ONLY_WITH_IMOT}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode in (1,2) AND zi.foryear=zi.msgyear AND (zi.dan+zi.smet)>0)");
/*    */         break;
/*    */       
/*    */       case 2:
/* 30 */         sql = sql.replace("{:ONLY_WITH_IMOT}", "AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND  zi.sortcode=1 AND zi.foryear=zi.msgyear AND coalesce(zi.parnom,'')=coalesce(ds.partidano,''))");
/*    */         break;
/*    */       default:
/* 33 */         sql = sql.replace("{:ONLY_WITH_IMOT}", "");
/*    */         break;
/*    */     } 
/* 36 */     sql = sql.replace("{:P_BY_YEAR}", this.byYear ? "1" : "0");
/* 37 */     if (this.showSQL) {
/* 38 */       System.out.println(sql);
/*    */     }
/* 40 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 41 */     stzad.execute();
/* 42 */     stzad.close();
/* 43 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void setFilterByExistingZad(int filterByExistingZad) throws Exception {
/* 49 */     if (filterByExistingZad > 2 || filterByExistingZad < 0) {
/* 50 */       throw new Exception("Флага за филтриране на стари задължения при съществуващи за тек.година е извън разрешение диапазон");
/*    */     }
/* 52 */     this.filterByExistingZad = filterByExistingZad;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\ImotOldZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */