/*    */ package dao;
/*    */ 
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public abstract class CurrentZadalDAO
/*    */   extends BaseYearDAO {
/*    */   protected boolean byOstatak = true;
/*    */   protected boolean forDeklar = true;
/*    */   
/*    */   public CurrentZadalDAO(Connection conn, int msgYear) {
/* 11 */     super(conn, msgYear);
/*    */   }
/*    */   
/*    */   public boolean isByOstatak() {
/* 15 */     return this.byOstatak;
/*    */   }
/*    */   public void setByOstatak(boolean byOstatak) {
/* 18 */     this.byOstatak = byOstatak;
/*    */   }
/*    */   public boolean isForDeklar() {
/* 21 */     return this.forDeklar;
/*    */   }
/*    */   public void setForDeklar(boolean forDeklar) {
/* 24 */     this.forDeklar = forDeklar;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\CurrentZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */