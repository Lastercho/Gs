/*    */ package dao;
/*    */ 
/*    */ import java.sql.Connection;
/*    */ import java.sql.Statement;
/*    */ 
/*    */ public class AnalyzerDAO
/*    */   extends BaseDAO
/*    */ {
/*    */   public AnalyzerDAO(Connection conn) {
/* 10 */     super(conn);
/*    */   }
/*    */ 
/*    */   
/*    */   public void analyzeTable(String tableName) throws Exception {
/* 15 */     Statement st = this.dbconn.createStatement();
/* 16 */     st.execute("ANALYZE " + tableName);
/* 17 */     st.close();
/* 18 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void vacumTable(String tableName) throws Exception {
/* 23 */     Statement st = this.dbconn.createStatement();
/* 24 */     st.execute("COMMIT; VACUUM FULL ANALYZE " + tableName + "; COMMIT;");
/* 25 */     st.close();
/* 26 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\AnalyzerDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */