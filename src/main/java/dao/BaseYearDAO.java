/*    */ package dao;
/*    */ 
/*    */ import java.io.BufferedReader;
/*    */ import java.io.InputStream;
/*    */ import java.io.InputStreamReader;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ public abstract class BaseYearDAO
/*    */   extends BaseDAO {
/*    */   protected int msgYear;
/*    */   protected boolean showSQL = true;
/*    */   
/*    */   public BaseYearDAO(Connection conn, int msgYear) {
/* 14 */     super(conn);
/* 15 */     this.msgYear = msgYear;
/*    */   }
/*    */ 
/*    */   
/*    */   public abstract void execute() throws Exception;
/*    */   
/*    */   protected String resourceToString(String resName) throws Exception {
/* 22 */     InputStream istr = getClass().getResourceAsStream("/sql/" + resName);
/* 23 */     StringBuilder sb = new StringBuilder();
/* 24 */     BufferedReader br = new BufferedReader(new InputStreamReader(istr, "UTF-8")); int c;
/* 25 */     for (c = br.read(); c != -1; ) { sb.append((char)c); c = br.read(); }
/*    */     
/* 27 */     return sb.toString();
/*    */   }
/*    */   
/*    */   public boolean isShowSQL() {
/* 31 */     return this.showSQL;
/*    */   }
/*    */   public void setShowSQL(boolean showSQL) {
/* 34 */     this.showSQL = showSQL;
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\BaseYearDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */