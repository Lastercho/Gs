/*    */ package dao;
/*    */ 
/*    */ import java.sql.CallableStatement;
/*    */ import java.sql.Connection;
/*    */ 
/*    */ 
/*    */ public class ImotCurrentZadalDAO
/*    */   extends CurrentZadalDAO
/*    */ {
/*    */   public ImotCurrentZadalDAO(Connection conn, int msgYear) {
/* 11 */     super(conn, msgYear);
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void execute() throws Exception {
/* 17 */     String sql = resourceToString("fill_imoti_current.sql");
/* 18 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 19 */     sql = sql.replace("{:P_MODE}", this.byOstatak ? "1" : "0");
/* 20 */     if (this.forDeklar) {
/* 21 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND ds.kinddoc=1");
/*    */     } else {
/* 23 */       sql = sql.replace("{:P_FILTER_KINDOC}", "AND coalesce(ds.kinddoc,0)!=1");
/*    */     } 
/* 25 */     if (this.showSQL) {
/* 26 */       System.out.println(sql);
/*    */     }
/* 28 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 29 */     stzad.execute();
/* 30 */     stzad.close();
/* 31 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void fillTBOPromilText() throws Exception {
/* 38 */     String sql = resourceToString("fill_promiltbo.sql");
/* 39 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 40 */     if (this.showSQL) {
/* 41 */       System.out.println(sql);
/*    */     }
/* 43 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 44 */     stzad.execute();
/* 45 */     stzad.close();
/* 46 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void fillTBOPromilTextPomorie() throws Exception {
/* 51 */     String sql = resourceToString("fill_promiltbo_pomorie.sql");
/* 52 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 53 */     if (this.showSQL) {
/* 54 */       System.out.println(sql);
/*    */     }
/* 56 */     this.dbconn.prepareStatement(sql).execute();
/* 57 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void fillMissingDO() throws Exception {
/* 63 */     String sql = resourceToString("add_missing_doc.sql");
/* 64 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 65 */     if (this.showSQL) {
/* 66 */       System.out.println(sql);
/*    */     }
/* 68 */     CallableStatement stzad = this.dbconn.prepareCall(sql);
/* 69 */     stzad.execute();
/* 70 */     stzad.close();
/* 71 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public void fillDNIPromilTextPomorie() throws Exception {
/* 77 */     String sql = resourceToString("fill_promildni_pomorie.sql");
/* 78 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 79 */     if (this.showSQL) {
/* 80 */       System.out.println(sql);
/*    */     }
/* 82 */     this.dbconn.prepareStatement(sql).execute();
/* 83 */     this.dbconn.commit();
/*    */   }
/*    */ 
/*    */   
/*    */   public void fillDNIPromil22Text() throws Exception {
/* 88 */     String sql = resourceToString("fill_promildni_22.sql");
/* 89 */     sql = sql.replace("{:P_YEAR}", this.msgYear + "");
/* 90 */     if (this.showSQL) {
/* 91 */       System.out.println(sql);
/*    */     }
/* 93 */     this.dbconn.prepareStatement(sql).execute();
/* 94 */     this.dbconn.commit();
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\dao\ImotCurrentZadalDAO.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */