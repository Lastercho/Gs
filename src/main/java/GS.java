import dao.*;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;
import net.sf.jasperreports.engine.fill.AsynchronousFilllListener;
import net.sf.jasperreports.engine.fill.FillListener;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.util.JRLoader;
import org.jdesktop.application.Application;
import org.jdesktop.application.Task;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;




public class GS {
    private JPanel MainPanel;
    private JPanel DataBase;
    private JPanel DataPanel;
    private JPanel PaymentPanel;
    private JLabel jLabel1;
    private JLabel jLabel15;
    private JTextField einText;
    private JLabel jLabel2;
    private JTextField dbip_edit;
    private JLabel jLabel3;
    private JTextField dbport_edit;
    private JLabel jLabel4;
    private JTextField dbname_edit;
    private JButton singleMsgPrintBtn;
    private JLabel jLabel5;
    private JTextField dbuser_edit;
    private JLabel jLabel6;
//    private JButton dataBaseDisconnect;
    private JPasswordField dbpass_edit;
    private JButton copydata_dtn;
    private JCheckBox analyzeCheck;
    private JCheckBox without_dead_check;
    private JCheckBox with_tbotext_check;
    private JLabel jLabel13;
    private JComboBox mps_old_combo;
    private JTextField paycode_edit;
    private JLabel jLabel12;
    private JCheckBox davnostByYearCheck;
    private JCheckBox nedd_by_year_check;
    private JComboBox davnost_combo;
    private JTextField minsuma_edit;
    private JTextField wpay_edit;
    private JCheckBox spr_check;
    private JCheckBox showPINCheck;
    private JTextField kasaadr_edit;
    private JButton dataprocessingBtn;
    private JButton print_btn;
    private JPanel BottomPanel;
    private JLabel version;
    private JLabel jLabel11;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JLabel jLabel10;
    private JLabel jLabel16;
    private JButton dbconnect_btn;
    private JLabel st_label;
    private JLabel jLabel14;
    private Connection oracon = null; private Map<Integer, Integer> pagesHash;
    private long muniId = 0L;
    private String muniCode;
    private String muniName = "";
    private int msgYear = 0;
    private String bankIban = "";
    private String bankCode = "";
    private String bankName = "";
    private String kasaAddres = "";
    private String otherPay = "";
    private String webSprAddress = "";
    private String bottomNote = null;
    private String bigDNIText = null;
    ReporStatustHelper reportStatus;
    private JComboBox<String> addr_type_combo;

    private JCheckBox alternativeSendMsgCheck;
    private JComboBox imot_old_combo;
    private JComboBox mps_cur_combo;
    private JComboBox other_combo;


    public GS() {
        singleMsgPrintBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
            singleMsgPrintAction();
            }
        });

        dbconnect_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dbconnAction();
            }
        });
        dataprocessingBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                data_processing_action();

            }
        });
        print_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                print_action();
            }
        });
        copydata_dtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                copydata_action();
            }
        });

    }



    public static void main(String[] args) {
        JFrame frame = new JFrame("GS");
        frame.setContentPane(new GS().MainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public void dbconnAction() {
        if (this.oracon == null) {
            try {
                Class.forName("org.postgresql.Driver");
                this.oracon = DriverManager.getConnection("jdbc:postgresql://" + dbip_edit.getText().trim() + ":" + this.dbport_edit.getText().trim() + "/" + this.dbname_edit.getText().trim(), this.dbuser_edit.getText().trim(), this.dbpass_edit.getText().trim());
                this.oracon.setAutoCommit(false);
                readMuniData();
                this.dbconnect_btn.setText("Разкачане от БД");
            } catch (Exception ex) {
                this.oracon = null;
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Неуспешна връзка към базата данни.\n" + ex.getMessage());
            }
        } else {
            try {
                this.oracon.rollback();
                this.oracon.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
            } finally {
                this.oracon = null;
                this.dbconnect_btn.setText("Връзка с БД");
            }
        }
        Component frame = null;

        JOptionPane.showMessageDialog(null,
                "Съобщения за " + this.msgYear,
                "Предупреждение",
                JOptionPane.WARNING_MESSAGE);

    }

    public Task copydata_action() {
        return new Copydata_actionTask(Application.getInstance());
    }

    private class Copydata_actionTask extends Task<Object, Void> {
        Copydata_actionTask(Application app) {
            super(app);
        }

        protected Object doInBackground() {
            return GS.this.copydata();
        }

        protected void succeeded(Object result) {}
    }


    public Task print_action() {
        return new Print_actionTask(Application.getInstance());
    }

    private class Print_actionTask extends Task<Object, Void> {
        Print_actionTask(Application app) {
            super(app);
        }

        protected Object doInBackground() {
            GS.this.print();
            return null;
        }

        protected void succeeded(Object result) {}
    }


    public Task data_processing_action() {
        return new Data_processing_actionTask(Application.getInstance());
    }

    private class Data_processing_actionTask extends Task<Object, Void> {
        Data_processing_actionTask(Application app) {
            super(app);
        }

        protected Object doInBackground() {
            return GS.this.dataPreProcessing();
        }

        protected void succeeded(Object result) {}
    }


    public void singleMsgPrintAction() {
        String printPath = getJarFolder();
        Map<String, Object> params = new HashMap<>();
        params.put("msgyear", "" + this.msgYear);
        params.put("w3pay", this.wpay_edit.getText().trim());
        params.put("wwwspr", Boolean.valueOf(this.spr_check.isSelected()));
        params.put("show_pin", Boolean.valueOf(this.showPINCheck.isSelected()));
        params.put("wwwspr_address", this.webSprAddress);
        params.put("kasa_addr", this.kasaadr_edit.getText());
        params.put("IBAN", this.bankIban);
        params.put("banka", this.bankName);
        params.put("BIC", this.bankCode);
        params.put("ds_name", this.muniName);
        params.put("nomasking", Boolean.valueOf(false));
        try {
            PrintingHelperDAO printHelperDAO = new PrintingHelperDAO(this.oracon, this.msgYear);
            ReportParamsEntity reportParamsEntity = new ReportParamsEntity();
            reportParamsEntity.setVirtualizerPages(5000);
            reportParamsEntity.setPageSides(0);
            this.pagesHash = new HashMap<>();
            if (this.muniId != 1227L)
                if (this.muniId != 1057L)
                    if (this.muniId != 1054L && this.muniId != 1060L) {
                        String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.ein='" + this.einText.getText().trim() + "' ";
                        reportParamsEntity.setGroupSQL(sql);
                        reportParamsEntity.setReportQuerySQLParamCount(2);
                        reportParamsEntity.setReportTemplateName("saob_2");
                        reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                        printSinglePersonGroup("Печат съобщение за едно лице ", reportParamsEntity, printPath, false, params);
                    }
            this.st_label.setText("Съобщенията са отпечатани");
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
    }


    private String resourceToString(String resName) throws Exception {
        InputStream istr = getClass().getResourceAsStream("/sql/" + resName);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(istr, "UTF-8"));
        int c;
        for (c = br.read(); c != -1; ) {
            sb.append((char)c);
            c = br.read();
        }
        return sb.toString();
    }

    public void print() {
        String printPath = getJarFolder() + "\\print-" + this.muniId;
        Map<String, Object> params = new HashMap<>();
        params.put("msgyear", "" + this.msgYear);
        params.put("w3pay", this.wpay_edit.getText().trim());
        params.put("wwwspr", Boolean.valueOf(this.spr_check.isSelected()));
        params.put("show_pin", Boolean.valueOf(this.showPINCheck.isSelected()));
        params.put("wwwspr_address", this.webSprAddress);
        params.put("bottomNote", this.bottomNote);
        params.put("bigDNIText", this.bigDNIText);
        params.put("kasa_addr", this.kasaadr_edit.getText());
        params.put("IBAN", this.bankIban);
        params.put("banka", this.bankName);
        params.put("BIC", this.bankCode);
        params.put("ds_name", this.muniName);
        params.put("nomasking", Boolean.valueOf(false));
        try {
            this.st_label.setText("1.Подготовка за печат");
            (new CleanDAO(this.oracon, this.msgYear)).cleanPages();
            this.st_label.setText("2.Маркиране на едностранните");
            PrintingHelperDAO printHelperDAO = new PrintingHelperDAO(this.oracon, this.msgYear);
            ReportParamsEntity reportParamsEntity = new ReportParamsEntity();
            reportParamsEntity.setVirtualizerPages(5000);
            reportParamsEntity.setPageSides(1);
            this.pagesHash = new HashMap<>();
            if (this.muniId == 1227L && this.msgYear < 2019) {
                printHelperDAO.markSinglesPages(9);
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.isperson=1";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setReportQuerySQLParamCount(2);
                reportParamsEntity.setReportTemplateName("saob_2");
                reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                printSinglePersonGroup("3.Печат многостранни ФЛ ", reportParamsEntity, printPath + "\\person", true, params);
                sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.isperson!=1";
                reportParamsEntity.setGroupSQL(sql);
                printSinglePersonGroup("4.Печат многостранни ЮЛ ", reportParamsEntity, printPath + "\\organization", true, params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.isperson=1 {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("5.Едностранни ФЛ ", reportParamsEntity, printPath + "\\person", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("6.Двустранни ФЛ ", reportParamsEntity, printPath + "\\person", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(600);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("7.4-ри странни ФЛ ", reportParamsEntity, printPath + "\\person", params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.isperson!=1 {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("8.Едностранни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("9.Двустранни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("10.4-ри странни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
            } else if (this.muniId == 1257L) {
                printHelperDAO.markSinglesPages(8);
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND (l.city_lice_id in (65231,100205) or l.dscode!='2313')";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setReportQuerySQLParamCount(2);
                reportParamsEntity.setReportTemplateName("saob_samokov");
                reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                printSinglePersonGroup("3.Печат многостранни - без селата ", reportParamsEntity, printPath + "\\withot_villages", true, params);
                String[][] aVillafes = {
                        { "391", "alino" }, { "3441", "beli_isakr" }, { "3767", "belchin" }, { "3770", "belchinski_bani" }, { "15285", "govedarci" }, { "16599", "gorni_okol" }, { "18201", "gucal" }, { "22469", "dolni_okol" }, { "23039", "dospei" }, { "23491", "dragushinovo" },
                        { "31228", "zlokuchene" }, { "37294", "klisura" }, { "37527", "kovachevci" }, { "43832", "lisec" }, { "46067", "madzhare" }, { "46276", "mala_carkva" }, { "47264", "marica" }, { "52249", "novo_selo" }, { "57697", "popovyane" }, { "58548", "prodanovci" },
                        { "61604", "raduil" }, { "61922", "rayovo" }, { "62486", "relyovo" }, { "100206", "chervenata_zemya_dragushinovo" }, { "83243", "shipochane" }, { "83291", "shiroki_dol" }, { "87535", "yarebkovica" }, { "87552", "yarlovo" } };
                int i;
                for (i = 0; i < aVillafes.length; i++) {
                    sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id=" + aVillafes[i][0];
                    reportParamsEntity.setGroupSQL(sql);
                    printSinglePersonGroup("4." + (i + 1) + " Печат многостранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], true, params);
                }
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND (s.city_lice_id in (65231,100205) or s.dscode!='2313') {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("5.Едностранни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("6.Двустранни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("7.4-ри странни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
                for (i = 0; i < aVillafes.length; i++) {
                    sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id=" + aVillafes[i][0] + " {:P_FILTER2} ");
                    reportParamsEntity.setGroupSQL(sql);
                    reportParamsEntity.setMaxPagesInFile(5000);
                    reportParamsEntity.setPageSides(1);
                    printMultipleTaxsubjectsMsg("8." + (i + 1) + "a Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
                    reportParamsEntity.setGroupSQL(sql);
                    reportParamsEntity.setMaxPagesInFile(2500);
                    reportParamsEntity.setPageSides(2);
                    printMultipleTaxsubjectsMsg("8." + (i + 1) + "б Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
                    reportParamsEntity.setGroupSQL(sql);
                    reportParamsEntity.setMaxPagesInFile(500);
                    reportParamsEntity.setPageSides(4);
                    printMultipleTaxsubjectsMsg("8." + (i + 1) + "в Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
                }
            } else if (this.muniId == 1057L) {
                printHelperDAO.markSinglesPages(9);
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id=36525";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setReportQuerySQLParamCount(2);
                reportParamsEntity.setReportTemplateName("saob_2");
                reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                printSinglePersonGroup("3.Печат многостранни - гр.Карнобат ", reportParamsEntity, printPath + "\\karnobat", true, params);
                sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id!=36525 AND l.dscode='0205'";
                reportParamsEntity.setGroupSQL(sql);
                printSinglePersonGroup("4.Печат многостранни - села ", reportParamsEntity, printPath + "\\villages", true, params);
                sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.dscode!='0205'";
                reportParamsEntity.setGroupSQL(sql);
                printSinglePersonGroup("5.Печат многостранни - БП ", reportParamsEntity, printPath + "\\bulgaria", true, params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id=36525 {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("6.Едностранни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("7.Двустранни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("8.4-ри странни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id!=36525 AND s.dscode='0205' {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("9.Едностранни - села ", reportParamsEntity, printPath + "\\villages", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("10.Двустранни - села ", reportParamsEntity, printPath + "\\villages", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("11.4-ри странни - села ", reportParamsEntity, printPath + "\\villages", params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.dscode!='0205' {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("12.Едностранни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("13.Двустранни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("14.4-ри странни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
            } else if (this.muniId == 1054L) {
                printHelperDAO.markSinglesPages(8);
                reportParamsEntity.setVirtualizerPages(5000);
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND z.msgnom>0 AND l.other_msg_code=2";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setReportQuerySQLParamCount(2);
                reportParamsEntity.setReportTemplateName("saob_burgas_1");
                reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                reportParamsEntity.setPageSides(0);
                printSinglePersonGroup("3.Печат за email ", reportParamsEntity, printPath + "\\by_email", false, params);
                sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND z.msgnom>0 AND l.other_msg_code=4";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setPageSides(0);
                printSinglePersonGroup("5.Печат за БЕЗ СЪОБЩЕНИЕ ", reportParamsEntity, printPath + "\\by_noprint", false, params);
                sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND (l.other_msg_code<2 or l.other_msg_code=3)";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setPageSides(1);
                printSinglePersonGroup("6.Печат многостранни ", reportParamsEntity, printPath, true, params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND (s.other_msg_code<2 or s.other_msg_code=3) {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("4.Едностранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("5.Двустранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("6.4-ри странни ", reportParamsEntity, printPath, params);
            } else if (this.muniId == 1060L) {
                printHelperDAO.markSinglesPages(7);
                reportParamsEntity.setVirtualizerPages(5000);
                reportParamsEntity.setReportQuerySQLParamCount(4);
                reportParamsEntity.setReportTemplateName("saob_by_year_pomorie");
                reportParamsEntity.setReportSQLFileName("print_saob_by_year.sql");
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 ";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setPageSides(1);
                printSinglePersonGroup("1.Печат многостранни ", reportParamsEntity, printPath, true, params);
                sql = resourceToString("print_saob_by_year.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("2.Едностранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("3.Двустранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("4.4-ри странни ", reportParamsEntity, printPath, params);
            } else {
                printHelperDAO.markSinglesPages(8);
                String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0";
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setReportQuerySQLParamCount(2);
                reportParamsEntity.setReportTemplateName("saob_2");
                reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
                printSinglePersonGroup("3.Печат многостранни ", reportParamsEntity, printPath, true, params);
                sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 {:P_FILTER2} ");
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(5000);
                reportParamsEntity.setPageSides(1);
                printMultipleTaxsubjectsMsg("4.Едностранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(2500);
                reportParamsEntity.setPageSides(2);
                printMultipleTaxsubjectsMsg("5.Двустранни ", reportParamsEntity, printPath, params);
                reportParamsEntity.setGroupSQL(sql);
                reportParamsEntity.setMaxPagesInFile(500);
                reportParamsEntity.setPageSides(4);
                printMultipleTaxsubjectsMsg("6.4-ри странни ", reportParamsEntity, printPath, params);
            }
            AnalyzerDAO analyzeDAO = new AnalyzerDAO(this.oracon);
            analyzeDAO.vacumTable("dgp_msg.msg_lica");
            analyzeDAO.vacumTable("dgp_msg.msg_zad");
            this.st_label.setText("Съобщенията са отпечатани");
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
    }

    public void printSinglePersonGroup(String caption, ReportParamsEntity reportParamsEntity, String basePath, boolean separateByPageCount, Map<String, Object> params) throws Exception {
        String countSQL = reportParamsEntity.getGroupSQL().replaceAll("SELECT([^<]*)FROM", "SELECT count(distinct l.taxsubject_id) FROM ");
        long totalRecord = 0L;
        long curRecord = 0L;
        this.pagesHash = new HashMap<>();
        params.put("multipleFile", Boolean.valueOf(false));
        CallableStatement st = this.oracon.prepareCall(countSQL);
        st.setInt(1, this.msgYear);
        ResultSet rs = st.executeQuery();
        if (rs.next())
            totalRecord = rs.getLong(1);
        rs.close();
        st.close();
        PreparedStatement pageSt = this.oracon.prepareStatement("UPDATE dgp_msg.msg_lica SET PAGES=? WHERE taxsubject_id=? AND msgyear=?");
        String reportSQL = resourceToString(reportParamsEntity.getReportSQLFileName());
        reportParamsEntity.setReportQuerySQL(reportSQL.replace("{:P_FILTER}", " AND s.taxsubject_id=? "));
        st = this.oracon.prepareCall(reportParamsEntity.getGroupSQL() + " ORDER BY z.msgnom");
        st.setInt(1, this.msgYear);
        rs = st.executeQuery();
        while (rs.next()) {
            curRecord++;
            this.st_label.setText(caption + curRecord + "/" + totalRecord + " : " + rs.getString("ein"));
            int pageCount = printSinglePersonMsg(rs.getLong("taxsubject_id"), basePath, separateByPageCount, reportParamsEntity, params);
            pageSt.setInt(1, pageCount);
            pageSt.setLong(2, rs.getLong("taxsubject_id"));
            pageSt.setInt(3, this.msgYear);
            pageSt.execute();
        }
        this.oracon.commit();
        rs.close();
        st.close();
    }

    public int printSinglePersonMsg(long taxsubjectId, String basePath, boolean separateByPageCount, ReportParamsEntity reportParamsEntity, Map<String, Object> params) throws Exception {
        CallableStatement st = this.oracon.prepareCall(reportParamsEntity.getReportQuerySQL());
        st.setInt(1, this.msgYear);
        st.setLong(2, taxsubjectId);
        if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
            st.setInt(3, this.msgYear);
            st.setLong(4, taxsubjectId);
        }
        ResultSet rs = st.executeQuery();
        JRResultSetDataSource rsDataSource = new JRResultSetDataSource(rs);
        InputStream jasperIS = getClass().getResourceAsStream("/reports/" + reportParamsEntity.getReportTemplateName() + ".jasper");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperIS, params, (JRDataSource)rsDataSource);
        int pageCount = jasperPrint.getPages().size();
        if ((pageCount > 2 && pageCount != 4) || reportParamsEntity.getPageSides() == 0) {
            String pdfPath;
            int hKey;
            if (separateByPageCount) {
                hKey = pageCount;
                pdfPath = basePath + "\\" + pageCount + "-pages";
            } else {
                hKey = 0;
                pdfPath = basePath;
            }
            Integer i = this.pagesHash.get(Integer.valueOf(hKey));
            if (i == null) {
                i = Integer.valueOf(0);
                File dir = new File(pdfPath);
                if (!dir.exists())
                    dir.mkdirs();
            }
            Integer integer1 = i, integer2 = i = Integer.valueOf(i.intValue() + 1);
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "CP1251");
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfPath + "\\" + i + ".pdf");
            exporter.exportReport();
            this.pagesHash.put(Integer.valueOf(hKey), i);
        }
        rs.close();
        st.close();
        return pageCount;
    }

    private void printMultipleTaxsubjectsMsg(String caption, ReportParamsEntity reportParamsEntity, String basePath, Map<String, Object> params) throws Exception {
        params.put("multipleFile", Boolean.valueOf(true));
        this.reportStatus = new ReporStatustHelper();
        this.reportStatus.setPdfPath(basePath + "\\" + reportParamsEntity.getPageSides() + "-pages");
        CallableStatement st = this.oracon.prepareCall("SELECT count(distinct msgnom) FROM (" + reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", "") + ") q0");
        st.setInt(1, this.msgYear);
        st.setInt(2, reportParamsEntity.getPageSides());
        if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
            st.setInt(3, this.msgYear);
            st.setInt(4, reportParamsEntity.getPageSides());
        }
        ResultSet rs = st.executeQuery();
        if (!rs.next())
            throw new Exception("Грешна заявка за общ брой в printMultipleTaxsubjectsMsg");
        this.reportStatus.setTotalQueuedMessages(rs.getInt(1));
        rs.close();
        st.close();
        File dir = new File(this.reportStatus.getPdfPath());
        if (!dir.exists())
            dir.mkdirs();
        int i = this.reportStatus.getTotalQueuedMessages();
        int sqlOffset = 0;
        int part = 1;
        while (i > 0) {
            this.reportStatus.setPart(part);
            this.st_label.setText(caption + " - подготовка част " + this.reportStatus.getPart());
            if (i >= reportParamsEntity.getMaxPagesInFile()) {
                this.reportStatus.setPartTotalQueuedMessages(reportParamsEntity.getMaxPagesInFile());
            } else {
                this.reportStatus.setPartTotalQueuedMessages(i);
            }
            st = this.oracon.prepareCall("SELECT min(msgnom) minnom,max(msgnom) maxnom FROM (SELECT distinct msgnom FROM (" + reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", "") + ") q0 ORDER BY msgnom OFFSET " + sqlOffset + " LIMIT " + reportParamsEntity.getMaxPagesInFile() + ") q1");
            st.setInt(1, this.msgYear);
            st.setInt(2, reportParamsEntity.getPageSides());
            if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
                st.setInt(3, this.msgYear);
                st.setInt(4, reportParamsEntity.getPageSides());
            }
            rs = st.executeQuery();
            if (!rs.next())
                throw new Exception("Грешна заявка за частичен брой в printMultipleTaxsubjectsMsg");
            String partSQL = reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", " AND z.msgnom>=" + rs.getInt(1) + " AND z.msgnom<=" + rs.getInt(2) + " ");
            printMultiple(caption, reportParamsEntity, partSQL, params);
            sqlOffset += reportParamsEntity.getMaxPagesInFile();
            part++;
            i -= reportParamsEntity.getMaxPagesInFile();
        }
    }

    private void printMultiple(String caption, ReportParamsEntity reportParamsEntity, String sql, Map<String, Object> params) throws Exception {
        JRFileVirtualizer virtualizer = new JRFileVirtualizer(reportParamsEntity.getVirtualizerPages());
        params.put("REPORT_VIRTUALIZER", virtualizer);
        CallableStatement st = this.oracon.prepareCall(sql);
        st.setInt(1, this.msgYear);
        st.setInt(2, reportParamsEntity.getPageSides());
        if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
            st.setInt(3, this.msgYear);
            st.setInt(4, reportParamsEntity.getPageSides());
        }
        ResultSet rs = st.executeQuery();
        JRResultSetDataSource rsDataSource = new JRResultSetDataSource(rs);
        InputStream jasperIS = getClass().getResourceAsStream("/reports/" + reportParamsEntity.getReportTemplateName() + ".jasper");
        JasperReport report = (JasperReport) JRLoader.loadObject(jasperIS);
        AsynchronousFillHandle handle = AsynchronousFillHandle.createHandle(report, params, (JRDataSource)rsDataSource);
        this.reportStatus.startReder();
        handle.startFill();
        handle.addFillListener(new FillListener() {
            public void pageGenerated(JasperPrint jasperPrint, int i) {
                GS.this.reportStatus.nextPage();
            }

            public void pageUpdated(JasperPrint jasperPrint, int i) {}
        });
        handle.addListener(new AsynchronousFilllListener() {
            public void reportFinished(JasperPrint jasperPrint) {
                try {
                    GS.this.reportStatus.setPartTotalRenderedPages(jasperPrint.getPages().size());
                    GS.this.reportStatus.startExport();
                    JRPdfExporter exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "CP1251");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, GS.this.reportStatus.getFullPDFName());
                    exporter.exportReport();
                } catch (JRException e) {
                    e.printStackTrace();
                }
                GS.this.reportStatus.finish();
            }

            public void reportCancelled() {
                GS.this.reportStatus.finish();
            }

            public void reportFillError(Throwable throwable) {
                throwable.fillInStackTrace();
                throwable.printStackTrace();
                GS.this.reportStatus.finish();
            }
        });
        while (!this.reportStatus.isFinished()) {
            Thread.sleep(600L);
            if (this.reportStatus.inRenderState()) {
                this.st_label.setText(caption + this.reportStatus.getPageCount() + " / " + (this.reportStatus.getTotalQueuedMessages() * reportParamsEntity.getPageSides()) + " страници, " + this.reportStatus.getTotalQueuedMessages() + " съобщения");
                continue;
            }
            this.st_label.setText(caption + " - експорт част " + this.reportStatus.getPart() + ", " + this.reportStatus.getPartTotalRenderedPages() + " страници, " + this.reportStatus.getPartTotalQueuedMessages() + " съобщения");
        }
        if (this.reportStatus.getPartTotalRenderedPages() != reportParamsEntity.getPageSides() * this.reportStatus.getPartTotalQueuedMessages())
            throw new Exception("Генерирания брой страници не е верен !!!!!");
        Thread.sleep(1000L);
        virtualizer.cleanup();
    }

    public Boolean copydata() {
        String mYear = Integer.toString(this.msgYear);
        String filterByOblog = "1";
        SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm:ss");
        AnalyzerDAO analyzeDAO = new AnalyzerDAO(this.oracon);
        try {
            createtables();
            this.st_label.setText("1. Подготовка " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            CleanDAO cleanDAO = new CleanDAO(this.oracon, this.msgYear);
            cleanDAO.clean();
            if (this.analyzeCheck.isSelected()) {
                analyzeDAO.vacumTable("dgp_msg.msg_lica");
                analyzeDAO.vacumTable("dgp_msg.msg_zad");
                analyzeDAO.analyzeTable("debtinstalment");
                analyzeDAO.analyzeTable("baldebtinst");
                analyzeDAO.analyzeTable("debtsubject");
                analyzeDAO.analyzeTable("debtpartproperty");
                analyzeDAO.analyzeTable("taxdoc");
                analyzeDAO.analyzeTable("address");
                analyzeDAO.analyzeTable("city");
                analyzeDAO.analyzeTable("street");
                analyzeDAO.analyzeTable("falloff");
            }
            this.st_label.setText("2. Попълване на задължения за имоти за " + mYear + " " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            ImotCurrentZadalDAO imotCurZadDAO = new ImotCurrentZadalDAO(this.oracon, this.msgYear);
            imotCurZadDAO.setByOstatak(true);
            imotCurZadDAO.setForDeklar(true);
            imotCurZadDAO.execute();
            imotCurZadDAO.setForDeklar(false);
            imotCurZadDAO.execute();
            this.st_label.setText("3. Попълване на липсващи данъчни оценки " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            imotCurZadDAO.fillMissingDO();
            if (this.mps_cur_combo.getSelectedIndex() < 2) {
                this.st_label.setText("4. Попълване на задължения за ПС за " + mYear + " " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                MPSCurrentZadalDAO mpsCurrZadDAO = new MPSCurrentZadalDAO(this.oracon, this.msgYear, (this.mps_cur_combo.getSelectedIndex() == 1));
                mpsCurrZadDAO.setByOstatak(true);
                mpsCurrZadDAO.setForDeklar(true);
                mpsCurrZadDAO.execute();
                mpsCurrZadDAO.setForDeklar(false);
                mpsCurrZadDAO.execute();
                mpsCurrZadDAO.moveSecondPayPeriodToFirst();
                if (this.mps_old_combo.getSelectedIndex() < 3) {
                    this.st_label.setText("5. Попълване на недобори за ПС " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                    MPSOldZadDAO mpsOldZadDao = new MPSOldZadDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
                    mpsOldZadDao.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
                    mpsOldZadDao.setDanvostFlag(this.davnost_combo.getSelectedIndex());
                    mpsOldZadDao.setFilterByExistingZad(this.mps_old_combo.getSelectedIndex());
                    mpsOldZadDao.execute();
                    mpsOldZadDao = null;
                }
            }
            if (this.muniId == 1058L) {
                this.st_label.setText("5a. Канали и радиоточки за текуща и мин.години " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                RadioKanaliMalkoTarnovoZadalDAO radioKanalMTarnovoZadalDAO = new RadioKanaliMalkoTarnovoZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
                radioKanalMTarnovoZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
                radioKanalMTarnovoZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
                radioKanalMTarnovoZadalDAO.execute();
            } else if (this.muniId == 1064L) {
                this.st_label.setText("5a. Гаражи и радиоточки за текуща и мин.години " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                GarageSredecZadalDAO garageSredecZadalDAO = new GarageSredecZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
                garageSredecZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
                garageSredecZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
                garageSredecZadalDAO.execute();
            }
            if (this.other_combo.getSelectedIndex() < 3) {
                if (this.paycode_edit.getText().contains("8013")) {
                    DogCurrentZadalDAO dogCurrZadDAO = new DogCurrentZadalDAO(this.oracon, this.msgYear, this.other_combo.getSelectedIndex());
                    dogCurrZadDAO.setByOstatak(true);
                    dogCurrZadDAO.setForDeklar(true);
                    dogCurrZadDAO.execute();
                    dogCurrZadDAO.setForDeklar(false);
                    dogCurrZadDAO.execute();
                }
                this.st_label.setText("6. Попълване на недобори за други данъци " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                OtherOldZadalDAO otherOldZadalDAO = new OtherOldZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected(), this.paycode_edit.getText());
                otherOldZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
                otherOldZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
                otherOldZadalDAO.setFilterByExistingZad(this.other_combo.getSelectedIndex());
                otherOldZadalDAO.execute();
            }
            if (this.imot_old_combo.getSelectedIndex() < 3) {
                this.st_label.setText("7. Попълване на недобори за имоти " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                ImotOldZadalDAO imotOldZadDAO = new ImotOldZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
                imotOldZadDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
                imotOldZadDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
                imotOldZadDAO.setFilterByExistingZad(this.imot_old_combo.getSelectedIndex());
                imotOldZadDAO.execute();
                imotOldZadDAO = null;
            }
            if (this.muniId == 1060L) {
                this.st_label.setText("7a. Групиране на недоборите " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                cleanDAO.groupNedobor();
            }
            this.st_label.setText("8. Попълване на данните за имоти/ПС " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            (new FillObjectDataDAO(this.oracon, this.msgYear)).execute();
            analyzeDAO.analyzeTable("dgp_msg.msg_zad");
            if (this.muniId == 1228L)
                cleanDAO.cleanWithoutImot();
            this.oracon.commit();
            this.st_label.setText("9. Изчистване на малките недобори " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            cleanDAO.deleteUnderValue(this.minsuma_edit.getText());
            cleanDAO.deleteUnderValueOtherCurYear(this.minsuma_edit.getText());
            if (this.muniId == 1056L && this.msgYear == 2019)
                cleanDAO.cleanALlUnderValue10();
            if (this.muniId != 1257L) {
                this.st_label.setText("10. Изтриване задълженията на ОБЩИНАТА " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                cleanDAO.deleteMunicipalityZadal();
            }
            cleanDAO.deleteOtherSobst();
            analyzeDAO.analyzeTable("taxsubject");
            analyzeDAO.analyzeTable("person");
            if (this.without_dead_check.isSelected()) {
                this.st_label.setText("11. Изтриване на починалите " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                cleanDAO.deleteDead();
            }
            if (this.muniId == 1227L || this.muniId == 1300L)
                (new DogCurrentZadalDAO(this.oracon, this.msgYear, this.other_combo.getSelectedIndex())).deleteOnlyDogNovaZagora();
            this.st_label.setText("12. Попълване данните за лицата " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            if (this.muniId == 1257L) {
                FillSubjectDataSamokovDAO fillSubjectDataDAO = new FillSubjectDataSamokovDAO(this.oracon, this.msgYear, this.muniId, this.alternativeSendMsgCheck.isSelected(), this.addr_type_combo.getSelectedIndex());
                fillSubjectDataDAO.execute();
            } else {
                FillSubjectDataDAO fillSubjectDataDAO = new FillSubjectDataDAO(this.oracon, this.msgYear, this.muniId, this.alternativeSendMsgCheck.isSelected(), this.addr_type_combo.getSelectedIndex());
                fillSubjectDataDAO.execute();
                if (this.muniId == 1054L) {
                    fillSubjectDataDAO.setOtherMsgCodeBurgas();
                    if (this.msgYear == 2018)
                        cleanDAO.cleanMPSCurrentYearOnly();
                }
            }
            if (this.muniId == 1257L)
                cleanDAO.markChujdenciSamokov();
            this.st_label.setText("13. Анонимизиране на партиди " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            (new MaskPartidaDAO(this.oracon, this.msgYear, this.muniCode)).execute();
            if (this.with_tbotext_check.isSelected()) {
                this.st_label.setText("14. Попълване на текста на промил ТБО " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                if (this.muniId == 1060L) {
                    imotCurZadDAO.fillTBOPromilTextPomorie();
                } else {
                    imotCurZadDAO.fillTBOPromilText();
                }
            }
            if (this.muniId == 1060L || this.muniId == 1061L || this.muniId == 1226L) {
                this.st_label.setText("15. Попълване на текста на промил ДНИ " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                imotCurZadDAO.fillDNIPromilTextPomorie();
            } else if (this.muniId == 1058L || this.muniId == 1300L) {
                this.st_label.setText("15. Попълване на текста на промил ДНИ " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
                imotCurZadDAO.fillDNIPromil22Text();
            }
            this.st_label.setText("Финализиране " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
            analyzeDAO.vacumTable("dgp_msg.msg_lica");
            analyzeDAO.vacumTable("dgp_msg.msg_zad");
            this.st_label.setText("Данните са извлечени " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
        return Boolean.valueOf(true);
    }

    public Boolean dataPreProcessing() {
        try {
            this.st_label.setText("1. Инициализация...");
            (new CleanDAO(this.oracon, this.msgYear)).cleanPages();
            this.st_label.setText("2. Номериране");
            MsgNumberingDAO msgNumberingDAO = new MsgNumberingDAO(this.oracon, this.msgYear);
            if (this.nedd_by_year_check.isSelected()) {
                msgNumberingDAO.numberingByYear();
            } else {
                msgNumberingDAO.numbering();
            }
            this.st_label.setText("Данните са готови за печат");
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
        return Boolean.valueOf(true);
    }

    public void readMuniData() {
        try {
            this.oracon.prepareStatement("CREATE SCHEMA IF NOT EXISTS dgp_msg").executeUpdate();
            ResultSet rs = this.oracon.createStatement().executeQuery("SELECT c.municipality_id,c.configvalue,m.name,ebk_code FROM config c JOIN municipality m ON m.municipality_id=c.municipality_id WHERE c.name='TAXYEAR14'");
            if (rs.next()) {
                this.muniId = rs.getLong("municipality_id");
                this.msgYear = rs.getInt("configvalue");
                this.muniName = rs.getString("name");
                this.muniCode = rs.getString("ebk_code");
            }
            rs.close();
            rs = this.oracon.createStatement().executeQuery("SELECT bac.iban,b.bic,b.fullname FROM baccount bac JOIN bank b ON b.bank_id=bac.bank_id WHERE bac.isbase=1 AND isactive=1");
            if (rs.next()) {
                this.bankIban = rs.getString("iban");
                this.bankCode = rs.getString("bic");
                this.bankName = rs.getString("fullname");
            }
            rs.close();
            rs = this.oracon.createStatement().executeQuery("select coalesce(o.note,'') || ', ' || coalesce(o.address, '') offaddress from Office o where o.isactive = 1 and o.office_id=1");
            if (rs.next())
                this.kasaAddres = rs.getString("offaddress");
            rs.close();
            this.bottomNote = "След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено. Ако междувременно сте платили задълженията си, моля да ни извините за безпокойството !";
            this.bigDNIText = null;
            if (this.muniId == 1227L) {
                this.kasaAddres = "на касите на отдел \"Местни данъци и такси\"        ул.\"24-Май\" №1, гр.Нова Загора";
                this.bankName = "Алианц Банк България";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\",                             касите на EasyPay или офисите на Банка ДСК.";
                this.webSprAddress = " или на адрес https://mdt.nova-zagora.org";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1062L) {
                this.kasaAddres = "на каса с адрес с.Руен, ул.\"Първи Май\" №18           и в кметствата на селата от община Руен";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\".";
                this.webSprAddress = " на адрес https://mpdt.obstinaruen.com";
                this.bankName = "Алианц Банк България АД";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1061L) {
                this.kasaAddres = "на каса с адрес гр.Приморско,                     ул.\"Трети Март\" №56";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
                this.webSprAddress = " на адрес https://mdt.primorsko.bg:8543 ";
                this.bottomNote = "След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено. Ако междувременно сте платили задълженията си, моля да ни извините за безпокойството!<br>Телефони за контакти: 0550 33695; 0550 33626; 0550 33603 и 0550 32002.";
                this.bigDNIText = "Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в гр.Приморско и гр.Китен, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 5.0 /пет/ промила.<br>";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1058L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
                this.bankName = "БАНКА ДСК";
                this.webSprAddress = " на адрес https://62.176.74.163 ";
                this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153/24.02.2012г., ДНИ за жилищни имоти в гр.Малко Търново, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 4.5 промила.<br>";
                this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
            } else if (this.muniId == 1064L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
                this.bankName = "Юробанк България АД";
                this.webSprAddress = " на адрес https://mdt.sredets.bg ";
                this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
            } else if (this.muniId == 1226L) {
                this.webSprAddress = " на адрес https://mdt.kotel.bg ";
                this.kasaAddres = "на каса с адрес гр.Котел,ул.\"Алеко Богориди\" №1";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и офисите на Банка ДСК.";
                this.bankName = "ОБЩИНСКА БАНКА АД";
                this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в гр.Котел, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 5.0 /пет/ промила.<br>";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1228L) {
                this.webSprAddress = " на адрес https://mdt.tvarditsa.org ";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
                this.bankName = "ОБЩИНСКА БАНКА АД";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1057L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
                this.bankName = "Централна Кооперативна Банка АД";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1300L) {
                this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в с.Стефан Караджово, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 4.5 промила.<br>";
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
                this.kasaAddres = "на каса с адрес гр.Болярово,                          ул.\"Димитър Благоев\" №7";
                this.bankName = "ОБЕДИНЕНА БЪЛГАРСКА БАНКА";
                this.bankCode = "UBBSBGSF";
                this.kasaadr_edit.setText(this.kasaAddres);
                this.webSprAddress = " на адрес https://213.91.194.72 ";
            } else if (this.muniId == 1283L) {
                this.otherPay = "или във всеки пощенски клон с пощенски запис за данъчно плащане.";
                this.kasaAddres = "на каса с адрес с.Минерални Бани,                             бул.\"Васил Левски\" №3";
                this.bankName = "ТЪРГОВСКА БАНКА \"Д\" ";
                this.webSprAddress = " mineralnibani.bg ";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1055L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
                this.kasaAddres = "на каса с адрес гр.Айтос,                                           ул.\"Цар Освободител\" №3";
                this.bankName = "ТБ \"АЛИАНЦ БАНК БЪЛГАРИЯ\" АД";
                this.webSprAddress = " на адрес https://mdt.aytos.bg ";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1056L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                              и касите на EasyPay.";
                this.kasaAddres = "на каса с адрес гр.Камено,                                           ул.\"Освобождение\" №101";
                this.webSprAddress = " на адрес https://mdt.kameno.bg ";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1065L) {
                this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
                this.kasaAddres = "на каса с адрес гр.Сунгурларе,                                           ул.\"Г.Димитров\" №2, ет.1";
                this.bankName = "УНИКРЕДИТ БУЛБАНК";
                this.webSprAddress = " на адрес https://mdt.sungurlare.org ";
                this.bottomNote += "<br>За справка позвънете на тел. 05571 5703";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else if (this.muniId == 1257L) {
                this.otherPay = "<b>или</b> чрез \"Български пощи\", Изипей, Фастпай<br>      и през интернет страницата на Общината.";
                this.kasaAddres = "на касите на Община Самоков                                             и в кметствата в селата на община Самоков";
                this.bankName = "ЦКБ АД КЛОН САМОКОВ";
                this.bottomNote = "Дължими са единствено сумите, които не са платени към датата на получаване на съобщението. След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено.";
                this.kasaadr_edit.setText(this.kasaAddres);
            } else {
                this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
                this.otherPay = "или във всеки пощенски клон с пощенски запис за данъчно плащане, интернет или чрез EasyPay";
            }
            this.wpay_edit.setText(this.otherPay);
            this.oracon.createStatement().execute(resourceToString("create_functions.sql"));
            this.oracon.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
    }

    public boolean isTableExists(String tableName, String shema) {
        boolean result = false;
        try {
            ResultSet rs = this.oracon.createStatement().executeQuery("SELECT count(*) FROM information_schema.tables WHERE table_name = '" + tableName + "' AND table_schema='" + shema + "'");
            if (rs.next() && rs.getInt(1) > 0)
                result = true;
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public boolean isColumnExists(String column, String table, String shema) {
        boolean result = false;
        try {
            ResultSet rs = this.oracon.createStatement().executeQuery("SELECT column_name FROM information_schema.columns WHERE table_name= '" + table + "' AND table_schema='" + shema + "' AND UPPER(column_name)='" + column.toUpperCase() + "'");
            result = rs.next();
            rs.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public void createtables() throws Exception {
        try {
            if (isTableExists("msg_lica", "dgp_msg")) {
                if (!isColumnExists("email", "msg_lica", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ADD email character varying(255)");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("other_msg_code", "msg_lica", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ADD other_msg_code numeric(2,0) NOT NULL DEFAULT 0");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                PreparedStatement ps = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ALTER COLUMN ein TYPE character varying(30)");
                ps.executeUpdate();
                ps.close();
                if (!isColumnExists("city_lice_id", "msg_lica", "dgp_msg")) {
                    ps = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_lica RENAME COLUMN kn_l TO city_lice_id;");
                    ps.executeUpdate();
                    ps = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_lica alter COLUMN city_lice_id type  numeric using (case when city_lice_id='*' then -1 when coalesce(city_lice_id,'')='' then 0 else city_lice_id::numeric end);");
                    ps.executeUpdate();
                    ps.close();
                }
            } else {
                PreparedStatement ps = this.oracon.prepareStatement("CREATE TABLE dgp_msg.msg_lica ( taxsubject_id numeric NOT NULL, msgyear numeric(4,0) NOT NULL, ein character varying(20) NOT NULL, isperson numeric(1,0) NOT NULL, isdead numeric(1,0) NOT NULL, ime character varying(200) NOT NULL, dscode character varying(5) NOT NULL, myname character varying(50) NOT NULL, cityname character varying(100) NOT NULL, muniname character varying(50) NOT NULL, city_lice_id numeric NOT NULL, ulimel character varying(200), pin character varying(20), email character varying(255), other_msg_code numeric(2,0) NOT NULL DEFAULT 0, badaddress numeric(1,0) NOT NULL DEFAULT 0, pages numeric(5,0) NOT NULL DEFAULT 0,  CONSTRAINT msg_lica_pkey PRIMARY KEY (taxsubject_id, msgyear))");
                ps.executeUpdate();
                ps.close();
            }
            if (isTableExists("msg_zad", "dgp_msg")) {
                if (!isColumnExists("taxobject_id", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD taxobject_id  numeric");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("promsm", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ALTER COLUMN promsm TYPE character varying(300)");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("city_i_id", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_zad RENAME COLUMN kn_i TO city_i_id;");
                    preparedStatement.executeUpdate();
                    preparedStatement = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_zad alter COLUMN city_i_id type  numeric using city_i_id::numeric;");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("promdni", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN promdni character varying(200)");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("dan_nedd_min_year", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN dan_nedd_min_year numeric(4,0) default 0;");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("dan_nedd_max_year", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN dan_nedd_max_year numeric(4,0) default 0;");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("smet_nedd_min_year", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN smet_nedd_min_year numeric(4,0) default 0;");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
                if (!isColumnExists("smet_nedd_max_year", "msg_zad", "dgp_msg")) {
                    PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN smet_nedd_max_year numeric(4,0) default 0;");
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
            } else {
                PreparedStatement preparedStatement = this.oracon.prepareStatement("CREATE TABLE dgp_msg.msg_zad (  id numeric NOT NULL,  taxsubject_id numeric NOT NULL,  msgyear numeric(4,0) NOT NULL,  paycode character varying(4) NOT NULL,  sortcode numeric NOT NULL,  foryear numeric(4,0) NOT NULL,  parnom character varying(30),  nasmi character varying(100),  city_i_id numeric,  ulime character varying(200),  doc numeric(13,2) NOT NULL,  promsm character varying(200),  mypromil numeric(22,5),  dan numeric(10,2) DEFAULT 0,  smet numeric(10,2) DEFAULT 0,  dan5 numeric(10,2) DEFAULT 0,  dan_1 numeric(10,2) DEFAULT 0,  dan_2 numeric(10,2) DEFAULT 0,  smet5 numeric(10,2) DEFAULT 0,  smet_1 numeric(10,2) DEFAULT 0,  smet_2 numeric(10,2) DEFAULT 0,  dan_nedd numeric(11,2) DEFAULT 0,  dan_lih numeric(11,2) DEFAULT 0,  smet_nedd numeric(11,2) DEFAULT 0,  smet_lih numeric(11,2) DEFAULT 0,  date_lix date,  srok1_1 date,  srok1_2 date,  taxdoc_id numeric,  taxobject_id  numeric,   zad_desc character varying(500),  msgnom numeric(7,0) NOT NULL DEFAULT 0,  zadnop numeric(4,0)NOT NULL DEFAULT 0,  maskpartida character varying(30),  promdni character varying(200),  dan_nedd_min_year numeric(4,0) default 0,  dan_nedd_max_year numeric(4,0) default 0,  smet_nedd_min_year numeric(4,0) default 0,  smet_nedd_max_year numeric(4,0) default 0,  CONSTRAINT msg_zad_pkey PRIMARY KEY (id))");
                preparedStatement.executeUpdate();
                preparedStatement.close();
                preparedStatement = this.oracon.prepareStatement("CREATE INDEX msg_zad_ind1 ON dgp_msg.msg_zad USING btree (msgyear,taxsubject_id,sortcode)");
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }
            this.oracon.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
        }
    }

    private String getJarFolder() {
        String p = (new File(".")).getAbsolutePath();
        return p.substring(0, p.length() - 1);
    }


}
