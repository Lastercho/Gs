/*    */
/*    */ 
/*    */ import java.math.BigDecimal;
/*    */ import java.text.DecimalFormat;
/*    */ import java.text.DecimalFormatSymbols;
/*    */ import java.util.Locale;
/*    */ 
/*    */ public class msgDecimalFormatter
/*    */ {
/*    */   public static String format(BigDecimal value) {
/* 11 */     DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
/* 12 */     otherSymbols.setDecimalSeparator('.');
/* 13 */     otherSymbols.setGroupingSeparator(' ');
/* 14 */     DecimalFormat df = new DecimalFormat("#,##0.00", otherSymbols);
/* 15 */     return df.format(value);
/*    */   }
/*    */ }


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\msgDecimalFormatter.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */