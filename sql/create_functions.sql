drop function if exists dgp_msg.getbestaddres(numeric, numeric, numeric);

CREATE OR REPLACE FUNCTION dgp_msg.getbestaddres(IN ip_adr1_id numeric, IN ip_adr2_id numeric, IN ip_adr3_id numeric, OUT op_address character varying, OUT op_city character varying, OUT op_admregion character varying, OUT op_muniname character varying, OUT op_areaname character varying, OUT op_postcode character varying, OUT op_ekkate numeric, OUT op_muni_id numeric, OUT op_muni_old_code character varying, OUT op_province_ekatte character varying, OUT op_isbadaddr numeric)
  RETURNS record AS
$$
DECLARE
	crs CURSOR (ipadr_id numeric) IS SELECT 
		(case adr.street_id
               when null then ''
               else
                 case Str.Kind_Street
                      when '0' then ''
                      when '1' then 'ул.'
                      when '2' then 'бул.'
                      when '3' then 'пл.'
                      when '4' then 'ж.р.'
                      when '5' then 'ж.к.'
                      when '6' then 'ж.кв.'
                      when '7' then 'кв.'
                      when '8' then 'M.'
                      else '' end || coalesce(Str.Name,'')
              end) StrName,
      adr.nom,
      adr.block,
      adr.entry,
      adr.floor,
      adr.apartment,
		Ci.postcode,
		m.name muniname,
		p.name areaname,
		Ci.kind_city,
		Ci.ekatte,
      case Ci.Kind_City
            when '0' then ''
            when '1' then 'гр.'
            when '2' then 'с.'
            when '3' then 'м.'
            when '4' then 'кв.'
            else  '' end || coalesce(Ci.Name,'') X2,
      ar.name arname,
      m.municipality_id muni_id,
      m.old_code muni_old_code,
      p.ekkate prov_ekatte,
      ci.city_id
	FROM Address adr
	--LEFT OUTER JOIN City Ci on adr.city_id = Ci.City_Id
	JOIN City Ci on adr.city_id = Ci.City_Id
	JOIN municipality m ON m.municipality_id=Ci.municipality_id
	JOIN province p ON p.province_id=m.province_id
	--LEFT OUTER JOIN municipality m ON m.municipality_id=Ci.municipality_id
	LEFT OUTER JOIN  Street Str on adr.street_id = Str.Street_Id
	LEFT OUTER JOIN  admregion ar on adr.admregion_id = ar.admregion_id
	WHERE adr.address_id = ipadr_id;
	radr				record;

	v_addr_ids		numeric ARRAY[3];
	v_cadr_id		numeric;	
	
	vStreetName    varchar;
begin
	op_isbadaddr=1;
	v_addr_ids[0]=ip_adr1_id;
	v_addr_ids[1]=ip_adr2_id;
	v_addr_ids[2]=ip_adr3_id;

	op_city=null;
	op_address=null;
	op_admregion=null;
	op_muniname=null;
	op_areaname=null;
	op_postcode=null;
	op_ekkate=null;
	op_muni_id=null;
	op_muni_old_code=null;
	op_province_ekatte=null;
	
	vStreetName='';
	FOREACH v_cadr_id IN ARRAY v_addr_ids
	LOOP
		--raise notice '%',v_cadr_id;
		if (v_cadr_id IS NOT NULL) then
			open crs(v_cadr_id);
			fetch crs INTO radr;
			if FOUND then
				if (UPPER(left(radr.StrName,8))='НЕИЗВЕСТ') then
					vStreetName='';
				else
					vStreetName=radr.StrName;
				end if;
				if coalesce(radr.nom,'')!='' then
					vStreetName := vStreetName || ' N: ' || trim(leading '0' FROM radr.nom);
				end if;
				if coalesce(radr.block,'')!='' then
					vStreetName := vStreetName || ', бл.' || trim(leading '0' FROM radr.block);
				end if;
				if coalesce(radr.entry,'')!='' then
					vStreetName := vStreetName || ', вх.' || trim(leading '0' FROM radr.entry);
				end if;
				if coalesce(radr.floor,'')!='' then
					vStreetName := vStreetName || ', ет.' || trim(leading '0' FROM radr.floor);
				end if;
				if coalesce(radr.apartment,'')!='' then
					vStreetName := vStreetName || ', ап.' || trim(leading '0' FROM radr.apartment);
				end if;
				op_city=radr.X2;
				op_address=vStreetName;
				op_admregion=radr.arname;
				op_muniname=radr.muniname;
				op_areaname=radr.areaname;
				op_postcode=radr.postcode;
				op_ekkate=radr.city_id; --radr.ekatte;
				op_muni_id=radr.muni_id;
				op_muni_old_code=radr.muni_old_code;
				op_province_ekatte=radr.prov_ekatte;
				if (UPPER(left(radr.X2,8))='НЕИЗВЕСТ') then
					op_isbadaddr=1;
				else
					if ((UPPER(left(radr.StrName,8))='НЕИЗВЕСТ' OR  radr.StrName='') AND radr.Kind_City='1') then
						op_isbadaddr=1;
					else
                                                if (radr.Kind_City='1' AND coalesce(radr.nom,'')='' AND coalesce(radr.block,'')='') then
                                                    op_isbadaddr=1;
                                                else
                                                    op_isbadaddr=0;
                                                end if;
						
					end if;
				end if;
			end if;
			close crs;
			if (op_isbadaddr=0) then
				EXIT;
			end if;
		end if;		
	END LOOP;
  
--exception
--  when others then
    --begin
--		raise notice 'error ';
      --return('');
    --end;
end;
$$ LANGUAGE plpgsql;

