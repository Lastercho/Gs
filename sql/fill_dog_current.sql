-- Задължения за кучета - тек.година
DO
$$
declare
r					record;
p_year			numeric; -- вх.параметър за тек.година
p_by_ost			integer; -- вх.параметър: 0-извеждат се облозите, 1-извеждат се остатъците
v_id				numeric;
v_zad				numeric(12,2);
v_seek_id		numeric;
v_desc			varchar;
vTaxdocId5		numeric;
vTaxobjectId5	numeric;
begin
	p_year:={:P_YEAR};
	p_by_ost={:P_MODE};
	SELECT coalesce(max(id),0)+1 FROM dgp_msg.msg_zad INTO v_id;
	FOR r IN SELECT ds.document_id taxdoc_id, ds.kinddebtreg_id, ds.taxsubject_id,
	(CASE WHEN td.taxdoc_id IS NULL THEN ds.docno ELSE td.docno END) docno,(CASE WHEN td.doc_date IS NULL THEN ds.doc_date ELSE td.doc_date END) doc_date,
	ds.partidano,ds.kinddoc,coalesce(ds.taxobject_id,td.taxobject_id) taxobject_id,
	coalesce(di.instno,0) instno,di.termpay_date,coalesce(di.instsum,0) obl,coalesce(bdi.instsum,0) ost,
	coalesce(sanction_pkg.tempdiscount(di.debtinstalment_id,trunc(current_date)),0) otbiv,
        (
		SELECT (CASE WHEN trim(coalesce(t2.registername,''))='' THEN '' ELSE ' с име '||trim(UPPER(t2.registername)) END) || (CASE WHEN length(trim(coalesce(d2.breed,'')))>1 THEN ' порода '||trim(d2.breed) ELSE '' END)
		FROM taxobject t2
		JOIN taxdoc td2 ON td2.taxobject_id=t2.taxobject_id
		JOIN dog d2 ON d2.taxdoc_id=td2.taxdoc_id
		WHERE t2.taxobject_id=coalesce(ds.taxobject_id,td.taxobject_id) AND t2.kindtaxobject='4'
	) descript
	FROM debtsubject ds
	JOIN debtinstalment di on ds.debtsubject_id = di.debtsubject_id
	LEFT JOIN baldebtinst bdi on di.debtinstalment_id = bdi.debtinstalment_id
	LEFT JOIN taxdoc td on ds.document_id = td.taxdoc_id AND ds.kinddoc=1
	WHERE ds.kinddebtreg_id=37
	AND extract(YEAR FROM ds.tax_begidate)=p_year
	AND (CASE WHEN p_by_ost=0 THEN coalesce(di.instsum,0) ELSE (coalesce(bdi.instsum,0) + coalesce(bdi.interestsum,0)) END)>0
	{:P_FILTER_KINDOC}
        {:ONLY_WITH_IMOT_MPS}
	LOOP
		SELECT id FROM dgp_msg.msg_zad mz WHERE mz.taxsubject_id=r.taxsubject_id AND mz.msgyear=p_year AND coalesce(mz.parnom,'')=coalesce(r.partidano,'') AND mz.sortcode=3 AND mz.paycode='8013' INTO v_seek_id;
		if (p_by_ost=0)
		then
			v_zad=r.obl;
		else
			v_zad=r.ost;
		end if;		
		if (r.kinddoc=5) then
			v_desc=' - Прехвърлени задължения от партида на починало лице';
		else
			if (coalesce(r.docno,'')='')
			then
				v_desc='';
			else
				v_desc=(CASE WHEN r.kinddoc=1 THEN ' по декларация № ' ELSE ' по документ № ' END)|| trim(r.docno) ||(CASE WHEN coalesce(r.doc_date,'1800-01-01 00:00:00')>'1800-01-01 00:00:00' THEN '/'|| to_char(r.doc_date, 'dd.mm.yyyy') ELSE '' END);
			end if;
		end if;
                v_desc=v_desc||' за куче '||coalesce(r.descript,'');
		if (v_seek_id IS NULL) 
		THEN
                        INSERT INTO dgp_msg.msg_zad (
				id,taxsubject_id,msgyear,paycode,sortcode,foryear,taxdoc_id,parnom,doc,
				dan,dan5,dan_1,dan_2,
				date_lix,srok1_1,srok1_2,zad_desc,taxobject_id
			)
			VALUES (
				v_id,r.taxsubject_id,p_year,'8013',3,p_year,r.taxdoc_id,r.partidano,0,
				v_zad,r.otbiv,(CASE WHEN r.instno=1 THEN v_zad ELSE 0 END),(CASE WHEN r.instno!=1 THEN v_zad ELSE 0 END),
				CURRENT_DATE,(CASE WHEN r.instno=1 THEN r.termpay_date ELSE NULL END),(CASE WHEN r.instno!=1 THEN r.termpay_date ELSE NULL END),v_desc,r.taxobject_id
			);
			v_id=v_id+1;
		ELSE
			-- update
			UPDATE dgp_msg.msg_zad SET
				dan=dan+v_zad,
				dan5=dan5+r.otbiv,
				dan_1=dan_1+(CASE WHEN r.instno=1 THEN v_zad ELSE 0 END),
				dan_2=dan_2+(CASE WHEN r.instno!=1 THEN v_zad ELSE 0 END),
				srok1_1=(CASE WHEN srok1_1 IS NULL AND r.instno=1 THEN r.termpay_date ELSE srok1_1 END), 
				srok1_2=(CASE WHEN srok1_2 IS NULL AND r.instno!=2 THEN r.termpay_date ELSE srok1_2 END),
				taxobject_id=(CASE WHEN taxobject_id IS NULL THEN r.taxobject_id ELSE taxobject_id END),
				taxdoc_id=(CASE WHEN taxdoc_id IS NULL THEN r.taxdoc_id ELSE taxdoc_id END),
				zad_desc=(CASE WHEN trim(coalesce(zad_desc,''))='' THEN v_desc ELSE zad_desc END)
			WHERE id=v_seek_id;
			
		END IF;
	END LOOP;
end;
$$
 
