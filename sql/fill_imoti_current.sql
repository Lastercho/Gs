/*
-- година
-- по остатъци/по облог
-- да се пусне два пъти - един път с kinddoc=1 и втори с kinddoc!=1


-- Задължения за тещата година за имоти само
*/
DO
$$
declare
r					record;
p_year			numeric; -- вх.параметър за тек.година
p_by_ost			integer; -- вх.параметър: 0-извеждат се облозите, 1-извеждат се остатъците
v_id				numeric;
v_zad				numeric(12,2);
v_seek_id		numeric;
v_desc			varchar;
vTaxdocId5		numeric;
vTaxobjectId5	numeric;
begin
	p_year:={:P_YEAR};
	p_by_ost={:P_MODE};
	SELECT coalesce(max(id),0)+1 FROM dgp_msg.msg_zad INTO v_id;
	FOR r IN SELECT ds.document_id taxdoc_id, ds.kinddebtreg_id, ds.taxsubject_id,
	(CASE WHEN td.taxdoc_id IS NULL THEN ds.docno ELSE td.docno END) docno,(CASE WHEN td.doc_date IS NULL THEN ds.doc_date ELSE td.doc_date END) doc_date,
	ds.partidano,ds.kinddoc,coalesce(ds.taxobject_id,td.taxobject_id) taxobject_id,
	coalesce((
		SELECT sum(coalesce(dpp.sumval,0)) 
		FROM debtpartproperty dpp 
		WHERE dpp.debtsubject_id=(SELECT max(ds2.debtsubject_id) FROM debtsubject ds2 WHERE coalesce(ds2.parent_debtsubject_id, ds2.debtsubject_id)=coalesce(ds.parent_debtsubject_id,ds.debtsubject_id) AND ds2.kinddebtreg_id=2 AND coalesce(ds2.typecorr,'0') != '2' AND ds2.kinddoc!=5)
	),0) doo,
	coalesce(di.instno,0) instno,di.termpay_date,coalesce(di.instsum,0) obl,coalesce(bdi.instsum,0) ost,
	coalesce(sanction_pkg.tempdiscount(di.debtinstalment_id,trunc(current_date)),0) otbiv
	FROM debtsubject ds
	JOIN debtinstalment di on ds.debtsubject_id = di.debtsubject_id
	LEFT JOIN baldebtinst bdi on di.debtinstalment_id = bdi.debtinstalment_id
	LEFT JOIN taxdoc td on ds.document_id = td.taxdoc_id AND ds.kinddoc=1
	WHERE ds.kinddebtreg_id in (2,5)
	AND extract(YEAR FROM ds.tax_begidate)=p_year
	AND (CASE WHEN p_by_ost=0 THEN coalesce(di.instsum,0) ELSE (coalesce(bdi.instsum,0) + coalesce(bdi.interestsum,0)) END)>0
	{:P_FILTER_KINDOC} 
--	AND ds.kinddoc=1 ---1
--	AND ds.kinddoc!=1 ---2
	LOOP
		SELECT id FROM dgp_msg.msg_zad mz WHERE mz.taxsubject_id=r.taxsubject_id AND mz.msgyear=p_year AND coalesce(mz.parnom,'')=coalesce(r.partidano,'') AND mz.sortcode=1 INTO v_seek_id;
		if (p_by_ost=0)
		then
			v_zad=r.obl;
		else
			v_zad=r.ost;
		end if;		
		if (r.kinddoc=5) then
--			SELECT td.taxdoc_id,td.taxobject_id FROM taxdoc td WHERE td.partidano=r.partidano AND td.documenttype_id in (21,22) AND coalesce(td.partidano,'')!='' INTO vTaxdocId5,vTaxobjectId5;
--			if (r.taxdoc_Id IS NULL) then
--				r.taxdoc_id=vTaxdocId5;
--			end if;
--			if (r.taxobject_id IS NULL) then
--				r.taxobject_id=vTaxobjectId5;
--			end if;
			v_desc=' - Прехвърлени задължения от партида на починало лице';
		else
			if (coalesce(r.docno,'')='')
			then
				v_desc='';
			else
				v_desc=(CASE WHEN r.kinddoc=1 THEN ' по декларация № ' ELSE ' по документ № ' END)|| trim(r.docno) ||(CASE WHEN coalesce(r.doc_date,'1800-01-01 00:00:00')>'1800-01-01 00:00:00' THEN '/'|| to_char(r.doc_date, 'dd.mm.yyyy') ELSE '' END);
			end if;
		end if;
		if (v_seek_id IS NULL) 
		THEN
			-- insert
			INSERT INTO dgp_msg.msg_zad (
				id,taxsubject_id,msgyear,paycode,sortcode,foryear,taxdoc_id,parnom,doc,
				dan,dan5,dan_1,dan_2,
				smet,smet5,smet_1,smet_2,
				date_lix,srok1_1,srok1_2,zad_desc,taxobject_id
			)
			VALUES (
				v_id,r.taxsubject_id,p_year,'2100',1,p_year,r.taxdoc_id,r.partidano,r.doo,
				(CASE WHEN r.kinddebtreg_id=2 THEN v_zad ELSE 0 END),(CASE WHEN r.kinddebtreg_id=2 THEN r.otbiv ELSE 0 END),(CASE WHEN r.kinddebtreg_id=2 AND r.instno=1 THEN v_zad ELSE 0 END),(CASE WHEN r.kinddebtreg_id=2 AND r.instno!=1 THEN v_zad ELSE 0 END),
				(CASE WHEN r.kinddebtreg_id=5 THEN v_zad ELSE 0 END),(CASE WHEN r.kinddebtreg_id=5 THEN r.otbiv ELSE 0 END),(CASE WHEN r.kinddebtreg_id=5 AND r.instno=1 THEN v_zad ELSE 0 END),(CASE WHEN r.kinddebtreg_id=5 AND r.instno!=1 THEN v_zad ELSE 0 END),
				CURRENT_DATE,(CASE WHEN r.instno=1 THEN r.termpay_date ELSE NULL END),(CASE WHEN r.instno!=1 THEN r.termpay_date ELSE NULL END),v_desc,r.taxobject_id
			);
			v_id=v_id+1;
		ELSE
			-- update
			UPDATE dgp_msg.msg_zad SET
				-- doc
				dan=dan+(CASE WHEN r.kinddebtreg_id=2 THEN v_zad ELSE 0 END),
				dan5=dan5+(CASE WHEN r.kinddebtreg_id=2 THEN r.otbiv ELSE 0 END),
				dan_1=dan_1+(CASE WHEN r.kinddebtreg_id=2 AND r.instno=1 THEN v_zad ELSE 0 END),
				dan_2=dan_2+(CASE WHEN r.kinddebtreg_id=2 AND r.instno!=1 THEN v_zad ELSE 0 END),
				smet=smet+(CASE WHEN r.kinddebtreg_id=5 THEN v_zad ELSE 0 END),
				smet5=smet5+(CASE WHEN r.kinddebtreg_id=5 THEN r.otbiv ELSE 0 END),
				smet_1=smet_1+(CASE WHEN r.kinddebtreg_id=5 AND r.instno=1 THEN v_zad ELSE 0 END),
				smet_2=smet_2+(CASE WHEN r.kinddebtreg_id=5 AND r.instno!=1 THEN v_zad ELSE 0 END),
				srok1_1=(CASE WHEN srok1_1 IS NULL AND r.instno=1 THEN r.termpay_date ELSE srok1_1 END), 
				srok1_2=(CASE WHEN srok1_2 IS NULL AND r.instno!=1 THEN r.termpay_date ELSE srok1_2 END),
				doc=(CASE WHEN r.kinddebtreg_id=2 AND doc=0 THEN r.doo ELSE doc END),
				taxobject_id=(CASE WHEN taxobject_id IS NULL THEN r.taxobject_id ELSE taxobject_id END),
				taxdoc_id=(CASE WHEN taxdoc_id IS NULL THEN r.taxdoc_id ELSE taxdoc_id END),
				zad_desc=(CASE WHEN trim(coalesce(zad_desc,''))='' THEN v_desc ELSE zad_desc END)
			WHERE id=v_seek_id;
			
		END IF;
	END LOOP;
end;
$$
 
