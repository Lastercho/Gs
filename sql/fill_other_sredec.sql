-- Радиоточки и гаражи текуща година и недобори за община Средец
DO
$$
declare
c CURSOR (cp_by_year numeric,cp_year numeric) IS 
SELECT ds.taxsubject_id,kd.kinddebtreg_id,kd.code paycode,ds.partidano,coalesce(d.dekltype,0) dekltype,
	(CASE WHEN cp_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END) foryear,
	sum(CASE WHEN extract(YEAR FROM ds.tax_begidate)=cp_year THEN coalesce(bdi.instsum,0) ELSE 0 END) glav,
	sum(CASE WHEN extract(YEAR FROM ds.tax_begidate)<cp_year THEN coalesce(bdi.instsum,0) ELSE 0 END) nedobor
FROM debtsubject ds
INNER JOIN debtinstalment di ON ds.debtsubject_id = di.debtsubject_id
INNER JOIN baldebtinst bdi on di.debtinstalment_id = bdi.debtinstalment_id 
JOIN kinddebtreg kd ON kd.kinddebtreg_id=ds.kinddebtreg_id
LEFT JOIN matsat.msat_deklar d ON d.taxsubject_id=ds.taxsubject_id AND d.partida=ds.partidano
WHERE kd.code='8090'
AND coalesce(bdi.instsum,0)>0
{:DAVNOST}
-- {:ONLY_WITH_IMOT_MPS}
GROUP BY ds.taxsubject_id,kd.kinddebtreg_id,ds.partidano,coalesce(d.dekltype,0),(CASE WHEN cp_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END)
ORDER BY foryear desc;



r						record;
p_year				numeric; -- вх.параметър за тек.година
p_by_year			integer; -- вх.параметър: 0-извежда се обща сума на недобори, 1-недобор по години
v_id				numeric;
v_seek_id			numeric;
v_desc				varchar;
begin
	p_year:={:P_YEAR};
	p_by_year={:P_BY_YEAR};
	SELECT coalesce(max(id),0)+1 FROM dgp_msg.msg_zad INTO v_id;
	open c(p_by_year,p_year);
	LOOP
		fetch c into r;
		EXIT WHEN NOT FOUND;
		r.paycode=left(r.paycode,4);
		if (p_by_year>0) then
			v_seek_id=-1;
		else
			SELECT id FROM dgp_msg.msg_zad mz WHERE mz.taxsubject_id=r.taxsubject_id AND mz.msgyear=p_year AND mz.parnom=r.partidano AND mz.sortcode=3 AND mz.foryear=mz.msgyear AND mz.paycode=r.paycode INTO v_seek_id;
		end if;

                if (r.dekltype=0) then
                    v_desc=' - такса за гараж/радиоточка';
                elsif (r.dekltype=1) then
                    v_desc=' - такса за гараж';
                else
                    v_desc=' - такса за радиоточка';
                end if;

		if (coalesce(v_seek_id,0)<1) 
		THEN
			-- insert
			INSERT INTO dgp_msg.msg_zad (
				id,taxsubject_id,msgyear,paycode,sortcode,foryear,parnom,doc,
                                dan,dan_nedd,dan_lih,srok1_1,dan_1,
				date_lix,zad_desc
			)
			VALUES (
				v_id,r.taxsubject_id,p_year,r.paycode,3,(CASE WHEN r.foryear=0 AND r.glav>0 THEN p_year ELSE r.foryear END),r.partidano,0,
				r.glav,r.nedobor,0,(CASE WHEN r.glav>0 THEN to_date('30.06.'||p_year, 'DD.MM.YYYY') ELSE null END),r.glav,
				CURRENT_DATE,v_desc
			);
			v_id=v_id+1;
		ELSE
			-- update
--			RAISE NOTICE 'UPDATING ...............';
			UPDATE dgp_msg.msg_zad SET dan_nedd=dan_nedd+r.nedobor WHERE id=v_seek_id;
		END IF;
	END LOOP;
	close c;
end;
$$
 
