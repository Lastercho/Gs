SELECT s.*,z.*,(CASE WHEN s.isperson=1 AND char_length(s.ein)=10 THEN overlay(s.ein placing '****' from 7 for 4) ELSE s.ein END) maskegn
FROM dgp_msg.msg_zad z
JOIN dgp_msg.msg_lica s ON s.taxsubject_id=z.taxsubject_id 
WHERE s.msgyear=? AND s.taxsubject_id=?
ORDER BY z.sortcode,z.parnom,foryear
LIMIT 100
/*
SELECT s.*,z2.id,z2.paycode,z2.sortcode,z2.iscuryear,z2.vhn_d,z2.parnom,z2.vim,z2.nasmi,z2.obsime,z2.kn_i,z2.ulime,z2.doc,z2.promsm,
(CASE WHEN s.fegn=1 AND char_length(s.egn)=10 THEN overlay(s.egn placing '****' from 7 for 4) ELSE s.egn END) maskegn,
z2.dan1,z2.smet,z2.ddan5,z2.ddan_1,z2.ddan_2,z2.ssmet5,z2.ssmet_1,z2.ssmet_2,z2.ddan_past,z2.ssmet_past,z2.ddan_lih,z2.smet_lih,z2.date_lix, 
z2.srok1_1,z2.srok1_2,z2.srok2_1,z2.srok2_2,z2.otbdate1,z2.otbdate2,(z2.dan1-z2.ddan5) otb1,(z2.smet-z2.ssmet5) otb2,z2.ddan_3,z2.ddan_4,COALESCE(z2.zad_desc,'') zad_desc
z2.nop,z2.sside,z2.prnzad,z2.ts_nop,z2.maskpartida,s.pin,(SELECT count(*) FROM dgp_msg.dgp_msg_zad2 z WHERE z.taxsubject_id=z2.taxsubject_id AND z.sside>0 ) msgcount ,z2.lastonpage 
FROM dgp_msg.dgp_msg_zad2 z2 
JOIN dgp_msg.dgp_msg_lica s ON s.taxsubject_id=z2.taxsubject_id 
WHERE s.mydscode=? AND z2.sside=? AND z2.nop>=? AND z2.nop<=? ORDER BY z2.nop
*/          