do
$$
declare

r					record;
v_msgyear		numeric;
vMaxDSID			numeric;
promDniTxt		varchar;
vObjCount		integer;
vBigDNICount	numeric;
taxperId            numeric;
begin
	v_msgyear={:P_YEAR};

	UPDATE dgp_msg.msg_zad SET promdni=null where msgyear=v_msgyear and promdni is not null;
        SELECT taxperiod_id FROM taxperiod WHERE extract(year from begin_date)=v_msgyear AND taxperkind='0' INTO taxperId;

	for r in select z.id,z.taxsubject_id,z.parnom
		from dgp_msg.msg_zad z
		where z.msgyear=v_msgyear and z.sortcode=1 and z.dan>0
	LOOP
		promDniTxt=null;
		
		SELECT max(xds.debtsubject_id) INTO vMaxDSID
		FROM debtsubject xds
		WHERE xds.partidano=r.parnom
		AND xds.taxsubject_id = r.taxsubject_id
		AND xds.kinddebtreg_id = 2
		AND coalesce(xds.typecorr,'0') != '2'
		AND extract(YEAR FROM xds.tax_begidate) = v_msgyear
		--AND coalesce(xds.kinddoc,0)!=5;
                AND coalesce(xds.kinddoc,0)=1;
		

		select count(*) total_cnt,max(is_big_dni) bigdni_cnt INTO vObjCount,vBigDNICount
		from (
			select (CASE WHEN h.kindhomeobjreg_id=1 and coalesce(ph.isbasehome, 0)=0 and coalesce(ph.isrent, 0)=0 and coalesce(ph.istourist ,0)=0 and c.resort_id is not null and abs(dp.totaltax-(dp.totalval*qr.homeprom/1000))<0.02 then qr.homeprom else 0 end) is_big_dni
			from debtsubject ds
			join debtpartproperty dp ON dp.debtsubject_id=ds.debtsubject_id
			join homeobj h on h.homeobj_id=dp.homeobj_id
			join building b on b.building_id=h.building_id
			join property p ON p.property_id=b.property_id
			join address a ON a.address_id=p.property_address_id
			join city c ON c.city_id=a.city_id
			join parthomeobj ph ON ph.homeobj_id=h.homeobj_id and ph.taxsubject_id=ds.taxsubject_id
                        join lateral (
                            select nr.homeprom from normresorts nr join resorts re on re.code=nr.code  where re.resort_id=c.resort_id and nr.taxperiod_id=taxperId
                        ) qr ON TRUE
			where dp.kindproperty=2
			and ds.kinddebtreg_id=2 and extract(year from ds.tax_begidate)=v_msgyear
                        and c.resort_id is not null
			--and ds.taxsubject_id=356793 and ds.partidano='6506086675003'
			and ds.debtsubject_id=vMaxDSID
			and coalesce(dp.totaltax,0)>0
			union all
			select 0 is_big_dni
			from debtsubject ds
			join debtpartproperty dp ON dp.debtsubject_id=ds.debtsubject_id
			join land l on l.land_id=dp.homeobj_id
			join property p ON p.property_id=l.property_id
			join address a ON a.address_id=p.property_address_id
			join city c ON c.city_id=a.city_id
			where dp.kindproperty=1
			and ds.kinddebtreg_id=2 and extract(year from ds.tax_begidate)=v_msgyear
                        and c.resort_id is not null
			--and ds.taxsubject_id=356793 and ds.partidano='6506086675003'
			and ds.debtsubject_id=vMaxDSID
			and coalesce(dp.totaltax,0)>0
		) q0;

--		if (vObjCount,vBigDNICount
		if (coalesce(vBigDNICount,0)>0) then
			promDniTxt=round(vBigDNICount,1);
		end if;
		UPDATE dgp_msg.msg_zad SET promdni=promDniTxt  WHERE id=r.id;
	END LOOP;
	
end
$$;
