DO
$$
declare
p_year      numeric; -- вх.параметър за тек.година
p_municode  varchar;
v_id        numeric;
v_part      varchar;
v_new       varchar;
v_egn       varchar;
v_isperson  integer;
i           integer;
d           integer;
begin
    p_year={:P_YEAR};
    p_municode={:P_EBK_CODE};
    FOR v_id,v_part,v_egn,v_isperson IN SELECT z.id,z.parnom,l.ein,l.isperson 
        FROM dgp_msg.msg_zad z 
        JOIN dgp_msg.msg_lica l ON l.taxsubject_id=z.taxsubject_id AND z.msgyear=l.msgyear
        WHERE z.msgyear=p_year AND sortcode=1 AND left(z.parnom,5) NOT IN (p_municode||'H',p_municode||'F')
    LOOP
        v_new=null;
        i=char_length(v_egn);
        d=char_length(v_part);
        if (v_isperson=1 AND left(v_part,i)=left(v_egn,i)) then
            v_new=overlay(v_part placing lpad('',i-6,'*') from 7 for i-7+1);
        elseif (v_isperson=1 and d>9 and (left(v_part,1) between '0' and '9') AND position('-' in v_part)=0) then
            v_new=overlay(v_part placing lpad('',4,'*') from 7 for 4);
        end if;
        UPDATE dgp_msg.msg_zad SET maskpartida=v_new WHERE id=v_id;
    end loop;
end;
$$
 
