do
$$
declare
c CURSOR (cp_msgyear numeric,cp_taxperiod_id numeric) IS SELECT z.id,z.taxsubject_id,z.parnom,z.doc,z.smet,--z.kn_i,
	(CASE WHEN EXISTS 
		(
		SELECT 1 
                FROM garbtax g
		JOIN taxdoc td ON td.taxdoc_id=g.taxdoc_id AND td.docstatus NOT IN ('10','70','90')
		JOIN taxdoc tdp ON tdp.taxdoc_id=g.proptaxdoc_id
		WHERE g.taxperiod_id=cp_taxperiod_id
		AND coalesce(g.containernorm_id,0)>0
		AND coalesce(g.frequency,0)>0 AND coalesce(g.container_number,0)>0
		AND tdp.partidano=z.parnom AND tdp.documenttype_id in (21,22)
-- да се допълни за промените 2017-2018
		) 
		THEN 1 ELSE 0 END ) iscontainer
	FROM dgp_msg.msg_zad z
	WHERE z.msgyear=cp_msgyear 
	AND z.foryear=z.msgyear
	AND z.sortcode=1 AND (dan>0 or smet>0) AND z.foryear=z.msgyear;

r					record;
v_minprom		numeric;
v_maxprom		numeric;
v_msgyear		numeric;
v_promil			numeric;
v_promtxt		varchar;
v_taxper_id		numeric;
v_r3				numeric(10,0);
v_r4				numeric(10,0);
v_r5				numeric(10,0);
v_round			integer;
begin
	v_msgyear={:P_YEAR};

	SELECT taxperiod_id FROM taxperiod WHERE extract(year from begin_date)=v_msgyear AND taxperkind='0' INTO v_taxper_id;

	SELECT sum(CASE WHEN right(q0.promiltbo::varchar,1)='0' THEN 0 ELSE 1 END) r5,sum(CASE WHEN right(q0.promiltbo::varchar,2)='00' THEN 0 ELSE 1 END) r4,sum(CASE WHEN right(q0.promiltbo::varchar,3)='000' THEN 0 ELSE 1 END) r3
	FROM (
		SELECT round(coalesce(promiltbo,0),5) promiltbo
		FROM debtsubject ds
		JOIN debtpartproperty p ON p.debtsubject_id=ds.debtsubject_id
		WHERE extract(year from ds.tax_begidate)=v_msgyear AND ds.kinddebtreg_id=5
	) q0 
	INTO v_r5,v_r4,v_r3;
	if (v_r5>0) then
		v_round=5;
	elsif (v_r4>0) then
		v_round=4;
	elsif (v_r3>0) then
		v_round=3;		
	else
		v_round=2;
	end if;
	open c(v_msgyear,v_taxper_id);
	LOOP
		FETCH c INTO r;
		EXIT WHEN NOT FOUND;
--		SELECT max(p.promiltbo) maxprom,min(p.promiltbo) minpriom
--		FROM debtsubject ds
--		JOIN debtpartproperty p ON p.debtsubject_id=ds.debtsubject_id
--		WHERE extract(year from ds.tax_begidate)=v_msgyear
--			AND ds.kinddebtreg_id=5
--			AND ds.partidano=r.parnom
--			AND ds.taxsubject_id=r.taxsubject_id	
--		INTO v_minprom,v_maxprom;
		SELECT max(p.promiltbo) maxprom,min(p.promiltbo) minpriom
		FROM debtpartproperty p
                WHERE p.debtsubject_id=(
                    SELECT max(xds.debtsubject_id)
                    FROM debtsubject xds
                    WHERE xds.partidano=r.parnom
                    AND xds.taxsubject_id = r.taxsubject_id
                    AND xds.kinddebtreg_id = 5
                    AND coalesce(xds.typecorr,'0') != '2'
                    AND extract(YEAR FROM xds.tax_begidate) = v_msgyear
                    AND coalesce(xds.kinddoc,0)!=5
                )
		INTO v_minprom,v_maxprom;

		if (r.iscontainer=1) then
			v_promil=null;
			v_promtxt='според количеството';
		else
			if (abs(v_minprom-v_maxprom)>0.0001) then
				v_promil=null;
				v_promtxt=null;
			else
				if (abs(r.doc*v_minprom*0.001-r.smet)<0.03) then
					v_promil=v_minprom;
					v_promtxt=round(v_minprom,v_round)||'%o';
				elsif (abs(r.doc*v_minprom*0.001-r.smet)<0.05) then
					v_promil=v_minprom;
					v_promtxt='* '||round(v_minprom,v_round)||'%o';
				else
					v_promil=null;
					v_promtxt=null;
				end if;
			end if;

			if (v_promtxt IS NULL) then
				SELECT '* '||string_agg(round(promiltbo,v_round)||'%o',' / ' ORDER BY promiltbo)
					FROM (
						SELECT p.promiltbo
						FROM debtpartproperty p
                                                WHERE p.debtsubject_id=(
                                                    SELECT max(xds.debtsubject_id)
                                                    FROM debtsubject xds
                                                    WHERE xds.partidano=r.parnom
                                                    AND xds.taxsubject_id = r.taxsubject_id
                                                    AND xds.kinddebtreg_id = 5
                                                    AND coalesce(xds.typecorr,'0') != '2'
                                                    AND extract(YEAR FROM xds.tax_begidate) = v_msgyear
                                                    AND coalesce(xds.kinddoc,0)!=5
                                                )
						GROUP BY p.promiltbo
						ORDER BY p.promiltbo
					) q0
					INTO v_promtxt;
			end if;
		end if;
		UPDATE dgp_msg.msg_zad SET promsm=v_promtxt,mypromil=v_promil  WHERE id=r.id;
	END LOOP;
	close c;
end
$$;