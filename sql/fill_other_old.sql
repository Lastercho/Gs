-- Задължения за недобори - други
-- Задължения за недобори - други
DO
$$
declare

r						record;
p_year				numeric; -- вх.параметър за тек.година
p_by_year			integer; -- вх.параметър: 0-извежда се обща сума на недобори, 1-недобор по години
v_id					numeric;
v_seek_id			numeric;
v_desc				varchar;
begin
	p_year:={:P_YEAR};
	p_by_year={:P_BY_YEAR};
	SELECT coalesce(max(id),0)+1 FROM dgp_msg.msg_zad INTO v_id;

	FOR r IN SELECT q0.taxsubject_id,q0.foryear,q0.partidano,glav,lihva,ds2.docno,ds2.doc_date,ds2.document_id,ds2.taxobject_id,ds2.kinddoc,
		q0.kinddebtreg_id,q0.payname,q0.fullpayname,q0.code paycode,q0.decalr_ds_id,q0.other_ds_id,q0.dead_ds_id,q0.globa_ds_id
		FROM (
			SELECT ds.taxsubject_id,kd.kinddebtreg_id,kd.name payname,kd.fullname fullpayname,kd.code,ds.partidano,(CASE WHEN p_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END) foryear,
				max(CASE WHEN kinddoc=1 AND kd.code!='65001' THEN ds.debtsubject_id ELSE null END) decalr_ds_id,max(CASE WHEN coalesce(ds.kinddoc,0)=0 THEN ds.debtsubject_id ELSE null END) other_ds_id,
				max(CASE WHEN ds.kinddoc=5 THEN ds.debtsubject_id ELSE null END) dead_ds_id,max(CASE WHEN kinddoc=2 AND kd.code='65001' THEN ds.debtsubject_id ELSE null END) globa_ds_id,
				sum(coalesce(bdi.instsum,0)) glav,
				sum(Round(coalesce(bdi.interestsum, 0) + coalesce(sanction_pkg.TempInt(coalesce(di.debtinstalment_id,0.0),CURRENT_DATE),0),2)) lihva
			FROM debtsubject ds
			JOIN debtinstalment di ON ds.debtsubject_id = di.debtsubject_id
			JOIN baldebtinst bdi ON di.debtinstalment_id = bdi.debtinstalment_id 
			JOIN kinddebtreg kd ON kd.kinddebtreg_id=ds.kinddebtreg_id
			WHERE kd.code in ( {:PAYCODES} )
			--WHERE kd.code in ( '14001','2800','3400','65001','8013') 
			AND extract(YEAR FROM ds.tax_begidate)<p_year
			AND (coalesce(bdi.instsum,0) + coalesce(bdi.interestsum,0))>0
		        {:DAVNOST}
--			AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.debtsubject_id=ds.debtsubject_id AND f.kind in (2,4))
			{:ONLY_WITH_IMOT_MPS}
			GROUP BY ds.taxsubject_id,kd.kinddebtreg_id,ds.partidano,(CASE WHEN p_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END)
		) q0
		LEFT JOIN debtsubject ds2 ON ds2.debtsubject_id=(CASE WHEN q0.code='65001' THEN coalesce(q0.globa_ds_id,q0.other_ds_id) ELSE coalesce(q0.decalr_ds_id,q0.other_ds_id,q0.dead_ds_id) END)
	LOOP
		
		r.paycode=left(r.paycode,4);
		if (p_by_year>0) then
			v_seek_id=-1;
		else
			SELECT id FROM dgp_msg.msg_zad mz WHERE mz.taxsubject_id=r.taxsubject_id AND mz.msgyear=p_year AND coalesce(mz.parnom,'')=coalesce(r.partidano,'') AND mz.sortcode=3 AND mz.foryear=mz.msgyear AND mz.paycode=r.paycode INTO v_seek_id;
		end if;

		if (r.paycode='6500') then
				v_desc=coalesce((SELECT (CASE WHEN a.kindact='11' THEN ' по наказателно постановление № ' ELSE ' по фиш № ' END)||a.actno||'/'||to_char(a.act_date,'dd.mm.yyyy') FROM act a WHERE a.act_id=r.document_id AND a.kindact in ('11','12')),' по наказателно постановление/фиш');			
		else
			if (r.kinddoc=5) then
				v_desc=' - Прехвърлени задължения от партида на починало лице';
			else
				if (trim(coalesce(r.docno,''))='') then
					v_desc='';
				else
					v_desc=(CASE WHEN r.kinddoc=1 THEN ' по декларация № ' ELSE ' по документ ' END) || trim(r.docno) ||(CASE WHEN coalesce(r.doc_date,'1800-01-01 00:00:00')>'1800-01-01 00:00:00' THEN '/'|| to_char(r.doc_date, 'dd.mm.yyyy') ELSE '' END);
				end if;
			end if;
			if (r.paycode='8013') then
				v_desc=v_desc || ' за куче' || coalesce(
					(
						SELECT (CASE WHEN trim(coalesce(t.registername,''))='' THEN '' ELSE ' с име '||trim(UPPER(t.registername)) END) || (CASE WHEN trim(coalesce(d.breed,''))='' THEN '' ELSE ' порода '||trim(d.breed) END)
						FROM taxobject t
						JOIN taxdoc td ON td.taxobject_id=t.taxobject_id
						JOIN dog d ON d.taxdoc_id=td.taxdoc_id
						WHERE t.taxobject_id=r.taxobject_id AND t.kindtaxobject='4'
					),'');

			elseif (r.paycode='1400') then
                            v_desc=v_desc || ' за извършвана патентна дейност' || coalesce(
                            (
                                SELECT ' през ' || extract(year FROM tp.begin_date)||'г.'
                                FROM taxperiod tp
                                JOIN taxobject t ON t.taxperiod_id=tp.taxperiod_id 
                                WHERE t.taxobject_id=r.taxobject_id AND tp.taxperkind='0'
                            ),'');
                        elseif (r.paycode='2800') then
                            v_desc=v_desc||' за туристически обект за настаняване '||coalesce((
                                select coalesce(t.registername,'')||' '||adr.op_city||coalesce(' '||adr.op_address,'')||coalesce(' за '||extract(year from tp.begin_date)||'г.','') odesc
                                from taxobject t 
                                join taxdoc td ON td.taxobject_id=t.taxobject_id
                                join dgp_msg.getbestaddres(t.address_id,null,null) adr ON true
                                left join taxperiod tp ON tp.taxperiod_id=t.taxperiod_id and tp.taxperkind='0'
                                where td.partidano=r.partidano and td.taxsubject_id=r.taxsubject_id  and td.documenttype_id=40
                                ),'');
			end if;
		
		end if;

		
		if (coalesce(v_seek_id,0)<1) 
		THEN
			-- insert
			INSERT INTO dgp_msg.msg_zad (
				id,taxsubject_id,msgyear,paycode,sortcode,foryear,taxdoc_id,parnom,doc,
				dan_nedd,dan_lih,
				date_lix,zad_desc,taxobject_id
--                                dan_nedd_min_year,dan_nedd_max_year
			)
			VALUES (
				v_id,r.taxsubject_id,p_year,r.paycode,3,r.foryear,r.document_id,r.partidano,0,
				r.glav,r.lihva,
				CURRENT_DATE,v_desc,r.taxobject_id
			);
			v_id=v_id+1;
		ELSE
			-- update
			UPDATE dgp_msg.msg_zad SET
				dan_nedd=dan_nedd+r.glav,
				dan_lih=dan_lih+r.lihva
			WHERE id=v_seek_id;
		END IF;
	END LOOP;
	
-- RAISE EXCEPTION '-----TEST';	
end;
$$
 

