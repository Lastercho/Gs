DO
$$
declare

cc CURSOR(cp_msgyear numeric) IS SELECT z.id,z.taxsubject_id,l.badaddress,z.parnom,z.taxdoc_id
	FROM dgp_msg.msg_zad z
	JOIN dgp_msg.msg_lica l ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear
	WHERE z.msgyear=cp_msgyear
	ORDER BY l.badaddress,l.dscode,l.cityname,l.city_lice_id,l.ulimel,l.ein,l.taxsubject_id,z.sortcode,z.parnom,z.foryear desc,z.paycode desc
	;
	
p_year      	numeric; -- вх.параметър за тек.година
r					record;

v_id        	numeric;
v_badaddr		numeric;
v_last_tsid		numeric;
v_znop			numeric(7);
v_tsnop			numeric(7);

v_part      	varchar;
v_new       	varchar;
v_egn       	varchar;
v_isperson  	integer;
i           	integer;
d           	integer;
v_key           varchar;
v_oldkey        varchar;
begin
	p_year:={:P_YEAR};
	open cc(p_year);
	v_last_tsid=-1;
	v_znop=1;
	v_tsnop=0;
        v_oldkey='';
	loop
		fetch cc into r;
		exit when not found;
                v_key=coalesce(r.parnom,'_')||'__'||coalesce(r.taxdoc_id,-1);
		if (v_last_tsid!=r.taxsubject_id) then
			v_znop=1;
                        v_oldkey=v_key;
			if (r.badaddress=0) then
				v_tsnop=v_tsnop+1;
			end if;
			v_last_tsid=r.taxsubject_id;
		else
                    if (v_key!=v_oldkey) then
                        v_znop=v_znop+1;
                        v_oldkey=v_key;
                    end if;			
		end if;
		UPDATE dgp_msg.msg_zad SET msgnom=(CASE WHEN r.badaddress=0 THEN v_tsnop ELSE 0 END),zadnop=v_znop WHERE id=r.id;
	end loop;
	close cc;
end;
$$
 
