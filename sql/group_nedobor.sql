-- Групира недоборите на общ ред Поморие 2019
DO
$$
declare
r						record;
minId					numeric;
p_year				numeric; -- вх.параметър за тек.година
begin	
	p_year:={:P_YEAR};
	for r in select z.taxsubject_id,sortcode,paycode,coalesce(z.parnom,'') g_partidano, 
		min(case when (z.dan_nedd>0 or z.dan_lih>0) then z.foryear else 9999 end) min_dan_year,
		min(case when (z.smet_nedd>0 or z.smet_lih>0) then z.foryear else 9999 end) min_smet_year,
		max(case when (z.dan_nedd>0 or z.dan_lih>0) then z.foryear else 0 end) max_dan_year,
		max(case when (z.smet_nedd>0 or z.smet_lih>0) then z.foryear else 0 end) max_smet_year,
		sum(z.dan_nedd) dan_nedd_sum,sum(z.dan_lih) dan_lih_sum,
		sum(z.smet_nedd) smet_nedd_sum,sum(z.smet_lih) smet_lih_sum,
		max(z.foryear) year_save
		from dgp_msg.msg_zad z
		where z.msgyear=p_year
		and z.foryear<p_year
		group by z.taxsubject_id,sortcode,paycode,coalesce(z.parnom,'')
	loop
		update dgp_msg.msg_zad z set dan_nedd=r.dan_nedd_sum,dan_lih=r.dan_lih_sum,smet_nedd=r.smet_nedd_sum,smet_lih=r.smet_lih_sum,
			dan_nedd_min_year=(case when r.min_dan_year=9999 then 0 else r.min_dan_year end),dan_nedd_max_year=r.max_dan_year,
			smet_nedd_min_year=(case when r.min_smet_year=9999 then 0 else r.min_smet_year end),smet_nedd_max_year=r.max_smet_year
			where z.taxsubject_id=r.taxsubject_id and z.msgyear=p_year and z.sortcode=r.sortcode and z.foryear=r.year_save and z.paycode=r.paycode and coalesce(z.parnom,'')=r.g_partidano;
		delete from dgp_msg.msg_zad z where z.taxsubject_id=r.taxsubject_id and z.msgyear=p_year and z.sortcode=r.sortcode and z.foryear<r.year_save and z.paycode=r.paycode and coalesce(z.parnom,'')=r.g_partidano;
	end loop;	
end;
$$
 