-- Задължения за недобори за имоти
DO
$$
declare
r						record;
p_year				numeric; -- вх.параметър за тек.година
p_by_year			integer; -- вх.параметър: 0-извежда се обща сума на недобори, 1-недобор по години

v_id					numeric;
v_seek_id			numeric;
v_desc				varchar;
begin	
	p_year:={:P_YEAR};
	p_by_year={:P_BY_YEAR};

	SELECT coalesce(max(id),0)+1 FROM dgp_msg.msg_zad INTO v_id;

	FOR r IN SELECT q0.taxsubject_id,q0.foryear,q0.partidano,glav_d,glav_s,lihva_d,lihva_s,ds2.docno,ds2.doc_date,ds2.document_id,ds2.taxobject_Id,ds2.kinddoc,q0.min_year_d,q0.max_year_d,q0.min_year_s,q0.max_year_s
		FROM (
			SELECT ds.taxsubject_id,ds.partidano,(CASE WHEN p_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END) foryear,
				max(CASE WHEN kinddoc=1 THEN ds.debtsubject_id ELSE null END) decalr_ds_id,max(CASE WHEN coalesce(ds.kinddoc,0)=0 THEN ds.debtsubject_id ELSE null END) other_ds_id,max(CASE WHEN ds.kinddoc=5 THEN ds.debtsubject_id ELSE null END) dead_ds_id,
				sum(CASE WHEN ds.kinddebtreg_id=2 THEN coalesce(bdi.instsum,0) ELSE 0 END) glav_d,
				sum(CASE WHEN ds.kinddebtreg_id=5 THEN coalesce(bdi.instsum,0) ELSE 0 END) glav_s,
				sum(CASE WHEN ds.kinddebtreg_id=2 THEN Round(coalesce(bdi.interestsum, 0) + coalesce(sanction_pkg.TempInt(coalesce(di.debtinstalment_id,0.0),CURRENT_DATE),0),2) ELSE 0 END) lihva_d,
				sum(CASE WHEN ds.kinddebtreg_id=5 THEN Round(coalesce(bdi.interestsum, 0) + coalesce(sanction_pkg.TempInt(coalesce(di.debtinstalment_id,0.0),CURRENT_DATE),0),2) ELSE 0 END) lihva_s,
                        min(CASE WHEN ds.kinddebtreg_id=2 then extract(YEAR FROM ds.tax_begidate) else 9999 end) min_year_d,
                        max(CASE WHEN ds.kinddebtreg_id=2 then extract(YEAR FROM ds.tax_begidate) else 0 end) max_year_d,
			min(CASE WHEN ds.kinddebtreg_id=5 then extract(YEAR FROM ds.tax_begidate) else 9999 end) min_year_s,
                        max(CASE WHEN ds.kinddebtreg_id=5 then extract(YEAR FROM ds.tax_begidate) else 0 end) max_year_s
			FROM debtsubject ds
			JOIN debtinstalment di ON ds.debtsubject_id = di.debtsubject_id
			JOIN baldebtinst bdi ON di.debtinstalment_id = bdi.debtinstalment_id 
			WHERE ds.kinddebtreg_id in (2,5)
			AND extract(YEAR FROM ds.tax_begidate)<p_year
			AND (coalesce(bdi.instsum,0) + coalesce(bdi.interestsum,0))>0
		        {:DAVNOST}
--			AND NOT EXISTS (SELECT 1 FROM falloff f WHERE f.debtsubject_id=ds.debtsubject_id AND f.kind in (2,4))
			{:ONLY_WITH_IMOT}
--			AND EXISTS (SELECT 1 FROM dgp_msg.msg_zad zi WHERE zi.msgyear=p_year AND zi.taxsubject_id=ds.taxsubject_id AND zi.sortcode=1 AND zi.foryear=zi.msgyear AND coalesce(zi.parnom,'')=coalesce(ds.partidano,''))
			GROUP BY ds.taxsubject_id,ds.partidano,(CASE WHEN p_by_year=1 THEN extract(YEAR FROM ds.tax_begidate) ELSE 0 END)
		) q0
		LEFT JOIN debtsubject ds2 ON ds2.debtsubject_id=coalesce(q0.decalr_ds_id,q0.other_ds_id,q0.dead_ds_id)
	LOOP
		if (p_by_year>0) then
			v_seek_id=-1;
		else
			SELECT id FROM dgp_msg.msg_zad mz WHERE mz.taxsubject_id=r.taxsubject_id AND mz.msgyear=p_year AND coalesce(mz.parnom,'')=coalesce(r.partidano,'') AND mz.sortcode=1 AND mz.foryear=mz.msgyear INTO v_seek_id;
		end if;
                if (r.min_year_d=9999) then
                    r.min_year_d=0;
                end if;
                if (r.min_year_s=9999) then
                    r.min_year_s=0;
                end if;
		if (coalesce(v_seek_id,0)<1) 
		THEN
                        if (r.taxobject_id IS NULL) then
                            SELECT td.taxdoc_id,td.taxobject_id FROM taxdoc td WHERE td.partidano=r.partidano AND td.documenttype_id in (21,22) AND coalesce(td.partidano,'')!='' INTO r.taxobject_id,r.document_id;
			end if;
			if (r.kinddoc=5) then
				v_desc=' - Прехвърлени задължения от партида на починало лице';
			else
				if (coalesce(r.docno,'')='') then
					v_desc='';
				else
					v_desc=(CASE WHEN r.kinddoc=1 THEN ' по декларация № ' ELSE ' по документ ' END) || trim(r.docno) ||(CASE WHEN coalesce(r.doc_date,'1800-01-01 00:00:00')>'1800-01-01 00:00:00' THEN '/'|| to_char(r.doc_date, 'dd.mm.yyyy') ELSE '' END);
				end if;
			end if;
			-- insert
			INSERT INTO dgp_msg.msg_zad (
				id,taxsubject_id,msgyear,paycode,sortcode,foryear,taxdoc_id,parnom,doc,
				dan_nedd,dan_lih,smet_nedd,smet_lih,
				date_lix,zad_desc,taxobject_id
--                                ,dan_nedd_min_year,dan_nedd_max_year,smet_nedd_min_year,smet_nedd_max_year
			)
			VALUES (
				v_id,r.taxsubject_id,p_year,'2100',1,r.foryear,r.document_id,r.partidano,0,
				r.glav_d,r.lihva_d,r.glav_s,r.lihva_s,
				CURRENT_DATE,v_desc,r.taxobject_id
--                                ,r.min_year_d,r.max_year_d,r.min_year_s,r.max_year_s
			);
			v_id=v_id+1;
		ELSE
			-- update
			UPDATE dgp_msg.msg_zad SET
				dan_nedd=dan_nedd+r.glav_d,
				dan_lih=dan_lih+r.lihva_d,
				smet_nedd=smet_nedd+r.glav_s,
				smet_lih=smet_lih+r.lihva_s
--                                ,dan_nedd_min_year=(case when r.min_year_d<dan_nedd_min_year then r.min_year_d else dan_nedd_min_year end),
--                                dan_nedd_max_year=(case when r.max_year_d>dan_nedd_max_year then r.max_year_d else dan_nedd_max_year end),
--                                smet_nedd_min_year=(case when r.min_year_s<smet_nedd_min_year then r.min_year_s else smet_nedd_min_year end),
--                                smet_nedd_max_year=(case when r.max_year_s>smet_nedd_max_year then r.max_year_s else smet_nedd_max_year end)
			WHERE id=v_seek_id;
		END IF;
	END LOOP;
end;
$$
 
