DO
$$
declare	
p_year      	numeric; -- вх.параметър за тек.година
r					record;

v_id        	numeric;
v_badaddr		numeric;
v_last_tsid		numeric;
v_znop			numeric(7);
v_tsnop			numeric(7);

v_part      	varchar;
v_new       	varchar;
v_egn       	varchar;
v_isperson  	integer;
i           	integer;
d           	integer;
begin
	p_year:={:P_YEAR};
	v_last_tsid=-1;
	v_znop=1;
	v_tsnop=0;
        FOR r IN SELECT z.id,z.taxsubject_id,l.badaddress,coalesce(l.other_msg_code,0) msg_method
	FROM dgp_msg.msg_zad z
	JOIN dgp_msg.msg_lica l ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear
	WHERE z.msgyear=p_year
	ORDER BY l.badaddress,(coalesce(l.other_msg_code,0)<2),l.dscode,l.cityname,l.city_lice_id,l.ulimel,l.ein,l.taxsubject_id,z.sortcode,z.parnom,z.foryear,z.paycode desc
	loop
		if (v_last_tsid!=r.taxsubject_id) then
			v_znop=1;
			if (r.badaddress=0 OR r.msg_method>1) then
				v_tsnop=v_tsnop+1;
			end if;
			v_last_tsid=r.taxsubject_id;
		else
			v_znop=v_znop+1;
		end if;
		UPDATE dgp_msg.msg_zad SET msgnom=(CASE WHEN r.badaddress=0 OR r.msg_method>1 THEN v_tsnop ELSE 0 END),zadnop=v_znop WHERE id=r.id;
	end loop;
end;
$$
 
