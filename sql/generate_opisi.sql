select sum(z.lastonpage) as list,min(s.kn_l) as kn_l2,max(s.egn) as egn2,min(z.nop) as minnop,
min(s.ime1) as ime1,min(s.ulimel) as ulimel,min(s.obslime) as obslime,min(trim(s.nasml)) as nasml,max(z.nop) as maxnop
FROM dgp_msg.dgp_msg_zad2 z
JOIN dgp_msg.dgp_msg_lica s ON s.taxsubject_id=z.taxsubject_id
where s.mydscode=? and z.sside=? and z.nop>=? AND z.nop<=?
group by s.egn 
order by minnop
--order by 8,2,4

/*
select count(*) as list,min(s.kn_l) as kn_l2,max(s.egn) as egn2,min(z.nop) as minnop,min(s.ime1) as ime1,min(s.ulimel) as ulimel,min(s.obslime) as obslime,min(s.nasml) as nasml,max(z.nop) as maxnop 
FROM dgp_msg.dgp_msg_zad2 z
JOIN dgp_msg.dgp_msg_lica s ON s.taxsubject_id=z.taxsubject_id
where s.mydscode=? and z.sside=? 
group by s.egn 
order by 2,3,4
*/