SELECT s.ein,s.isperson,s.isdead,s.ime,s.dscode,s.myname,s.cityname,s.muniname,s.ulimel,s.pin,s.email,
z.foryear,z.paycode,z.sortcode,z.parnom,z.doc,
z.dan,z.dan5,z.dan_1,z.dan_2,z.dan_nedd,z.dan_lih,
to_char(date_lix,'dd.mm.yyyy') datelix,to_char(z.srok1_1,'dd.mm.yyyy') srok1,to_char(z.srok1_2,'dd.mm.yyyy') srok2,
z.zad_desc,z.maskpartida,
(CASE WHEN s.isperson=1 AND char_length(s.ein)=10 THEN overlay(s.ein placing '****' from 7 for 4) ELSE s.ein END) maskegn,
(z.msgyear=z.foryear)::boolean iscuryear,
(CASE WHEN (dan_nedd+dan_lih+smet_nedd+smet_lih)>0.001 THEN true ELSE false END) hasnedobor,
(CASE WHEN (dan5+smet5)>0.001 THEN true ELSE false END) hasotbiv,
z.msgnom,z.zadnop,
--left(kd.acc,2)||' '||substring(kd.acc FROM 3 FOR 2)||' '||right(kd.acc,2) newacc,
left(kd.acc,2)||' '||substring(kd.acc FROM 3 FOR 2)||' '||substring(kd.acc FROM 5 FOR 2) newacc,
(CASE WHEN kd.code='14001' THEN 'Патентен данък'
WHEN kd.code='2100' THEN 'Данък в/у недвижимите имоти'
WHEN kd.code='2300' THEN 'Данък върху прев.средства'
WHEN kd.code='8013' THEN 'Такса за куче'
WHEN kd.code='65001' THEN 'Глоба, санкция'
WHEN kd.code='8013' THEN 'Туристически данък'
ELSE kd.fullname END) fullpayname,
1 pkey,
z.promsm,
z.dan_nedd_min_year min_year,z.dan_nedd_max_year max_year,(case when z.promdni is null then 0 else 1 end) has_big_dni
FROM dgp_msg.msg_zad z
JOIN dgp_msg.msg_lica s ON s.taxsubject_id=z.taxsubject_id AND s.msgyear=z.msgyear
JOIN kinddebtreg kd ON kd.code=(CASE left(z.paycode,4) WHEN  '1400' THEN '14001' WHEN '6500' THEN '65001' ELSE z.paycode END)
WHERE s.msgyear=? {:P_FILTER}
AND (z.dan+z.dan_nedd+z.dan_lih)>0
UNION ALL
SELECT s.ein,s.isperson,s.isdead,s.ime,s.dscode,s.myname,s.cityname,s.muniname,s.ulimel,s.pin,s.email,
z.foryear,z.paycode,z.sortcode,z.parnom,z.doc,
z.smet dan,z.smet5 dan5,z.smet_1 dan_1,z.smet_2 dan_2,z.smet_nedd dan_nedd,z.smet_lih dan_lih,
to_char(date_lix,'dd.mm.yyyy') datelix,to_char(z.srok1_1,'dd.mm.yyyy') srok1,to_char(z.srok1_2,'dd.mm.yyyy') srok2,
z.zad_desc,z.maskpartida,
(CASE WHEN s.isperson=1 AND char_length(s.ein)=10 THEN overlay(s.ein placing '****' from 7 for 4) ELSE s.ein END) maskegn,
(z.msgyear=z.foryear)::boolean iscuryear,
(CASE WHEN (dan_nedd+dan_lih+smet_nedd+smet_lih)>0.001 THEN true ELSE false END) hasnedobor,
(CASE WHEN (dan5+smet5)>0.001 THEN true ELSE false END) hasotbiv,
z.msgnom,z.zadnop,
'44 24 00','Такса за битови отпадъци' fullpayname,
2 pkey,
z.promsm,
z.smet_nedd_min_year min_year,z.smet_nedd_max_year max_year,(case when z.promdni is null then 0 else 1 end) has_big_dni
FROM dgp_msg.msg_zad z
JOIN dgp_msg.msg_lica s ON s.taxsubject_id=z.taxsubject_id AND s.msgyear=z.msgyear
WHERE s.msgyear=? {:P_FILTER}
AND (z.smet+z.smet_nedd+z.smet_lih)>0
AND z.sortcode=1
ORDER BY msgnom,zadnop,foryear desc,pkey
          