/* Корегира данъчните оценки ,които са <=0 */
DO
$$
declare
tc CURSOR (cp_year numeric) IS SELECT z.id,z.taxsubject_id,z.taxdoc_id,z.parnom,z.dan,z.smet ,z.doc,
	(SELECT sum(coalesce(dpp.sumval,0)) doc
		FROM debtpartproperty dpp 
		WHERE dpp.debtsubject_id=(
			SELECT max(xds.debtsubject_id)
			FROM debtsubject xds
			WHERE xds.document_id = z.taxdoc_id 
                        AND xds.taxsubject_id = z.taxsubject_id
			AND xds.kinddebtreg_id = 2
			AND coalesce(xds.typecorr,'0') != '2'
			AND extract(YEAR FROM xds.tax_begidate) = z.foryear
                        AND coalesce(xds.kinddoc,0)!=5
		)
--		WHERE dpp.debtsubject_id in (
--			SELECT xds.debtsubject_id
--			FROM debtsubject xds
--			WHERE xds.document_id = z.taxdoc_id AND xds.taxsubject_id = z.taxsubject_id
--			AND xds.kinddebtreg_id = 2
--			AND coalesce(xds.typecorr,'0') != '2'
--			AND extract(YEAR FROM xds.tax_begidate) = z.foryear
--		)
	) doc2_d
--	(SELECT sum(coalesce(dpp.sumval,0)) doc
--		FROM debtpartproperty dpp 
--		WHERE dpp.debtsubject_id in (
--			SELECT xds.debtsubject_id
--			FROM debtsubject xds
--			WHERE xds.document_id = z.taxdoc_id AND xds.taxsubject_id = z.taxsubject_id
--			AND xds.kinddebtreg_id = 5
--			AND coalesce(xds.typecorr,'0') != '2'
--			AND extract(YEAR FROM xds.tax_begidate) = z.foryear
--                        AND exists(select 1 from debtinstalment di inner join baldebtinst bdi on bdi.debtinstalment_id=di.debtinstalment_id where di.debtsubject_id=xds.debtsubject_id) --??
--			)
--	) doc2_s
	FROM dgp_msg.msg_zad z 
	WHERE z.doc<=0 AND z.msgyear=cp_year AND z.sortcode=1 AND z.msgyear=z.foryear;
	r	   		record;
	p_year		numeric; -- вх.параметър за тек.година
begin
	p_year={:P_YEAR};
	open tc(p_year) ;
	loop
		fetch tc INTO r;
		EXIT WHEN NOT FOUND;
			if (r.doc2_d IS NOT NULL)
			then
				UPDATE dgp_msg.msg_zad SET doc=r.doc2_d WHERE id=r.id;
			end if;
	end loop;
	close tc;
end;
$$