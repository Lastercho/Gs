SELECT s.ein,s.isperson,s.isdead,s.ime,s.dscode,s.myname,s.cityname,s.muniname,s.ulimel,s.pin,
z.foryear,z.paycode,z.sortcode,z.parnom,z.doc,z.promsm,
z.dan,z.dan5,z.dan_1,z.dan_2,
z.smet,z.smet5,z.smet_1,z.smet_2,
z.dan_nedd,z.dan_lih,z.smet_nedd,z.smet_lih,
to_char(date_lix,'dd.mm.yyyy') datelix,to_char(z.srok1_1,'dd.mm.yyyy') srok1,to_char(z.srok1_2,'dd.mm.yyyy') srok2,
z.zad_desc,z.maskpartida,
--(CASE WHEN s.isperson=1 AND char_length(s.ein)=10 THEN overlay(s.ein placing '****' from 7 for 4) ELSE s.ein END) maskegn,
(CASE WHEN s.isperson=1 AND char_length(s.ein)>=10 THEN overlay(s.ein placing '****' from 7 for 4) ELSE s.ein END) maskegn,
(z.msgyear=z.foryear)::boolean iscuryear,
(CASE WHEN (dan_nedd+dan_lih+smet_nedd+smet_lih)>0.001 THEN true ELSE false END) hasnedobor,
z.msgnom,z.zadnop,
--left(kd.acc,2)||' '||substring(kd.acc FROM 3 FOR 2)||' '||right(kd.acc,2) newacc,
left(kd.acc,2)||' '||substring(kd.acc FROM 3 FOR 2)||' '||substring(kd.acc FROM 5 FOR 2) newacc,
-- malko tarnovo
(CASE WHEN kd.code='14001' THEN 'Патентен данък'
WHEN kd.code='2100' THEN 'Данък в/у недвижимите имоти'
WHEN kd.code='2300' THEN 'Данък в/у превозните средства'
WHEN kd.code='8013' THEN 'Такса за куче'
WHEN kd.code='65001' THEN 'Глоба, санкция'
WHEN kd.code='8013' THEN 'Туристически данък'
WHEN kd.code='8001' THEN 'Такси за технически услуги'
WHEN kd.code='7000' THEN 'Други неданъчни приходи'
WHEN kd.code='8090' THEN 'Други общински такси'
ELSE kd.fullname END) fullpayname,
(case when z.promdni is null then 0 else 1 end) has_big_dni,
coalesce(z.parnom,'_')||'__'||coalesce(z.taxdoc_id,-1) pkey
FROM dgp_msg.msg_zad z
JOIN dgp_msg.msg_lica s ON s.taxsubject_id=z.taxsubject_id AND z.msgyear=s.msgyear
JOIN kinddebtreg kd ON kd.code=(CASE left(z.paycode,4) WHEN  '1400' THEN '14001' WHEN '6500' THEN '65001' ELSE z.paycode END)
WHERE s.msgyear=? {:P_FILTER}

-- WHERE s.msgyear=? AND s.pages=1
--and z.msgnom>20093
-- AND s.kn_l!='07079' AND s.dscode!='0204'
-- AND s.dscode='0205'
-- and z.msgnom<=20093
-- AND s.isperson=1
-- AND NOT(left(ein,2)='ID' AND s.pages=1)
-- AND msgnom>5026
-- and s.ein='7505106362'
ORDER BY msgnom,zadnop

          