do
$$
declare
c CURSOR (cp_msgyear numeric,cp_taxperiod_id numeric) IS SELECT z.id,z.taxsubject_id,l.isperson,z.parnom,z.doc,z.smet,z.city_i_id,(CASE WHEN q0.garbtax_id IS NULL THEN false ELSE true END) iscontainer,q0.cofi,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.sw_home,0) ELSE coalesce(pr.fsw_home,0)  END )  tbo_s,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.clean_home,0) ELSE coalesce(pr.fclean_home,0)  END )  tbo_c,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.depot_home,0) ELSE coalesce(pr.fdepot_home,0)  END )  tbo_d,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.sw,0) ELSE coalesce(pr.fsw,0)  END )  tbo_sn,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.clean,0) ELSE coalesce(pr.fclean,0)  END )  tbo_cn,
		(CASE WHEN l.isperson=1 THEN coalesce(pr.depot,0) ELSE coalesce(pr.fdepot,0)  END )  tbo_dn
		FROM dgp_msg.msg_zad z
		JOIN dgp_msg.msg_lica l ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear
		JOIN promtbo pr ON pr.city_id=city_i_id AND pr.taxperiod_id=cp_taxperiod_id AND pr.code='1'
		LEFT JOIN LATERAL (
			SELECT g.garbtax_id,string_agg((
					CASE WHEN coalesce(cn.container_id,0)>0 AND coalesce(g.container_number,0)>0 THEN
						(CASE coalesce(cn.container_id,0) WHEN 11 THEN 'Кофа' WHEN 12 THEN 'Контейнер 4куб.м.' WHEN 13 THEN 'Контейнер 1.1куб.м.' ELSE null END)||' - '||round(g.container_number,0)||'бр'
					ELSE null
					END
				),' , ') cofi
			FROM garbtax g
			JOIN taxdoc td ON td.taxdoc_id=g.taxdoc_id AND td.docstatus NOT IN ('70','90') AND td.documenttype_id=23
			JOIN taxdoc tdp ON tdp.taxdoc_id=g.proptaxdoc_id
			LEFT JOIN containernorm cn ON cn.containernorm_id=g.containernorm_id
			WHERE g.taxperiod_id=cp_taxperiod_id
			AND tdp.partidano=z.parnom AND tdp.documenttype_id in (21,22)
			GROUP BY g.garbtax_id
		) q0 ON TRUE
		WHERE z.msgyear=cp_msgyear
		AND z.foryear=z.msgyear
		AND z.sortcode=1 AND (dan>0 or smet>0) AND z.foryear=z.msgyear;


r					record;
v_minprom		numeric;
v_maxprom		numeric;
v_msgyear		numeric;
v_promil			numeric;
v_promtxt		varchar;
v_taxper_id		numeric;
v_cofaprice		numeric;
vMaxDSID			numeric;
v_r3				numeric(10,0);
v_r4				numeric(10,0);
v_r5				numeric(10,0);
v_round			integer;
vTempStr			varchar;
i			numeric;
begin
	v_msgyear={:P_YEAR};

	UPDATE dgp_msg.msg_zad SET promsm=null,mypromil=null where msgyear=v_msgyear;

	SELECT taxperiod_id FROM taxperiod WHERE extract(year from begin_date)=v_msgyear AND taxperkind='0' INTO v_taxper_id;

	SELECT sum(CASE WHEN right(q0.promiltbo::varchar,1)='0' THEN 0 ELSE 1 END) r5,sum(CASE WHEN right(q0.promiltbo::varchar,2)='00' THEN 0 ELSE 1 END) r4,sum(CASE WHEN right(q0.promiltbo::varchar,3)='000' THEN 0 ELSE 1 END) r3
	FROM (
		SELECT round(coalesce(promiltbo,0),5) promiltbo
		FROM debtsubject ds
		JOIN debtpartproperty p ON p.debtsubject_id=ds.debtsubject_id
		WHERE extract(year from ds.tax_begidate)=v_msgyear AND ds.kinddebtreg_id=5
	) q0 
	INTO v_r5,v_r4,v_r3;
	if (v_r5>0) then
		v_round=5;
	elsif (v_r4>0) then
		v_round=4;
	elsif (v_r3>0) then
		v_round=3;		
	else
		v_round=2;
	end if;
	open c(v_msgyear,v_taxper_id);
	LOOP
		FETCH c INTO r;
		EXIT WHEN NOT FOUND;

		

		SELECT max(xds.debtsubject_id) INTO vMaxDSID
		FROM debtsubject xds
		WHERE xds.partidano=r.parnom
		AND xds.taxsubject_id = r.taxsubject_id
		AND xds.kinddebtreg_id = 5
		AND coalesce(xds.typecorr,'0') != '2'
		AND extract(YEAR FROM xds.tax_begidate) = v_msgyear
		AND coalesce(xds.kinddoc,0)!=5;
		
		
		SELECT max(p.promiltbo) maxprom,min(p.promiltbo) minpriom 
		FROM debtpartproperty p
		WHERE p.debtsubject_id=vMaxDSID AND coalesce(p.homeobj_id,0)>0
		INTO v_minprom,v_maxprom;

		v_promtxt='';
		v_cofaprice=0;
		if (r.iscontainer) then
			v_promil=null;
			v_promtxt='според количеството: ';
			SELECT sum(coalesce(p.totaltax,0)) INTO v_cofaprice FROM debtpartproperty p WHERE p.debtsubject_id=vMaxDSID AND coalesce(p.homeobj_id,0)=0; --- ?????
			v_cofaprice=coalesce(v_cofaprice,0);
		end if;
		--else
			
			if (abs(v_minprom-v_maxprom)>0.0001) then
				v_promil=null;
			else
				if (abs((r.doc*v_minprom*0.001)+v_cofaprice-r.smet)<0.03) then
					v_promil=v_minprom;
				elsif (abs((r.doc*v_minprom*0.001)+v_cofaprice-r.smet)<0.06) then
					v_promil=v_minprom;
--					v_promtxt='* ';
				else
					v_promil=null;
				end if;
			end if;

--			if (v_promtxt IS NULL) then
				SELECT ''||string_agg(typ||' '||round(promiltbo,v_round)||'%o',' / ' ORDER BY promiltbo),count(*)
					FROM (
						SELECT p.promiltbo,
						( CASE
							WHEN abs(p.promiltbo-r.tbo_s-r.tbo_c-r.tbo_d)<0.0001 THEN 'за жил.обекти пълен размер'
							WHEN abs(p.promiltbo-r.tbo_sn-r.tbo_cn-r.tbo_dn)<0.0001 THEN 'за нежил.обекти пълен размер'
							WHEN abs(p.promiltbo-r.tbo_c-r.tbo_d)<0.0001 AND not(r.iscontainer) THEN 'за жил.обекти само депо и чистота'
							WHEN abs(p.promiltbo-r.tbo_cn-r.tbo_dn)<0.0001 AND not(r.iscontainer) THEN 'за нежил.обекти само депо и чистота'
							WHEN abs(p.promiltbo-r.tbo_d)<0.0001 THEN 'за жил.обекти само депо'
							WHEN abs(p.promiltbo-r.tbo_dn)<0.0001 THEN 'за нежил.обекти само депо'
							WHEN abs(p.promiltbo-r.tbo_c)<0.0001 THEN (CASE WHEN r.iscontainer THEN '' ELSE 'жил.обекти ' END)||'само чистота'
							WHEN abs(p.promiltbo-r.tbo_cn)<0.0001 THEN (CASE WHEN r.iscontainer THEN '' ELSE 'нежил.обекти ' END)||'само чистота'
							WHEN abs(p.promiltbo)<0.0001 THEN 'без такса'
							ELSE ' '
							END
						) typ
						FROM debtpartproperty p
						LEFT JOIN homeobj h ON h.homeobj_id=p.homeobj_id AND p.kindproperty=2
						WHERE p.debtsubject_id=vMaxDSID
						AND coalesce(p.homeobj_id,0)>0                                                
						GROUP BY p.promiltbo
						ORDER BY p.promiltbo
					) q0
					INTO vTempStr,i;
					v_promtxt=v_promtxt||coalesce(vTempStr,'');
					if (i>1) then
						v_promtxt=v_promtxt|| ' според вида на обекта';
					end if;
--			end if;
		--end if;
		if (r.iscontainer AND r.cofi IS NOT NULL) then
			v_promtxt=v_promtxt||' + '||r.cofi;
		end if;
		UPDATE dgp_msg.msg_zad SET promsm=v_promtxt,mypromil=v_promil  WHERE id=r.id;
	END LOOP;
	close c;
end
$$;
