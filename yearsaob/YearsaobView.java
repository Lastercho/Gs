package yearsaob;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.Timer;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.AsynchronousFillHandle;
import net.sf.jasperreports.engine.fill.AsynchronousFilllListener;
import net.sf.jasperreports.engine.fill.FillListener;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import org.jdesktop.application.Action;
import org.jdesktop.application.Application;
import org.jdesktop.application.ApplicationActionMap;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.Task;
import org.jdesktop.application.TaskMonitor;
import yearsaob.dao.AnalyzerDAO;
import yearsaob.dao.CleanDAO;
import yearsaob.dao.DogCurrentZadalDAO;
import yearsaob.dao.FillObjectDataDAO;
import yearsaob.dao.FillSubjectDataDAO;
import yearsaob.dao.FillSubjectDataSamokovDAO;
import yearsaob.dao.GarageSredecZadalDAO;
import yearsaob.dao.ImotCurrentZadalDAO;
import yearsaob.dao.ImotOldZadalDAO;
import yearsaob.dao.MPSCurrentZadalDAO;
import yearsaob.dao.MPSOldZadDAO;
import yearsaob.dao.MsgNumberingDAO;
import yearsaob.dao.OtherOldZadalDAO;
import yearsaob.dao.PrintingHelperDAO;
import yearsaob.dao.RadioKanaliMalkoTarnovoZadalDAO;

public class YearsaobView extends FrameView {
  private Connection oracon = null; private Map<Integer, Integer> pagesHash;
  private long muniId = 0L;
  private String muniCode;
  private String muniName = "";
  private int msgYear = 0;
  private String bankIban = "";
  private String bankCode = "";
  private String bankName = "";
  private String kasaAddres = "";
  private String otherPay = "";
  private String webSprAddress = "";
  private String bottomNote = null;
  private String bigDNIText = null; ReporStatustHelper reportStatus; private JComboBox<String> addr_type_combo;
  private JCheckBox alternativeSendMsgCheck;

  public YearsaobView(SingleFrameApplication app) {
    super((Application)app);




  this.busyIcons = new Icon[15];
  this.busyIconIndex = 0;
  initComponents();
  ResourceMap resourceMap = getResourceMap();
  int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout").intValue();
  this.messageTimer = new Timer(messageTimeout, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          YearsaobView.this.statusMessageLabel.setText("");
        }
      });
  this.messageTimer.setRepeats(false);
  int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate").intValue();
  for (int i = 0; i < this.busyIcons.length; i++)
    this.busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
  this.busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          YearsaobView.this.busyIconIndex = (YearsaobView.this.busyIconIndex + 1) % YearsaobView.this.busyIcons.length;
          YearsaobView.this.statusAnimationLabel.setIcon(YearsaobView.this.busyIcons[YearsaobView.this.busyIconIndex]);
        }
      });
  this.idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
  this.statusAnimationLabel.setIcon(this.idleIcon);
  this.progressBar.setVisible(false);
  TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
  taskMonitor.addPropertyChangeListener(new PropertyChangeListener() {
        public void propertyChange(PropertyChangeEvent evt) {
          String propertyName = evt.getPropertyName();
          if ("started".equals(propertyName)) {
            if (!YearsaobView.this.busyIconTimer.isRunning()) {
              YearsaobView.this.statusAnimationLabel.setIcon(YearsaobView.this.busyIcons[0]);
              YearsaobView.this.busyIconIndex = 0;
              YearsaobView.this.busyIconTimer.start();
            }
            YearsaobView.this.progressBar.setVisible(true);
            YearsaobView.this.progressBar.setIndeterminate(true);
          } else if ("done".equals(propertyName)) {
            YearsaobView.this.busyIconTimer.stop();
            YearsaobView.this.statusAnimationLabel.setIcon(YearsaobView.this.idleIcon);
            YearsaobView.this.progressBar.setVisible(false);
            YearsaobView.this.progressBar.setValue(0);
          } else if ("message".equals(propertyName)) {
            String text = (String)evt.getNewValue();
            YearsaobView.this.statusMessageLabel.setText((text == null) ? "" : text);
            YearsaobView.this.messageTimer.restart();
          } else if ("progress".equals(propertyName)) {
            int value = ((Integer)evt.getNewValue()).intValue();
            YearsaobView.this.progressBar.setVisible(true);
            YearsaobView.this.progressBar.setIndeterminate(false);
            YearsaobView.this.progressBar.setValue(value);
          }
        }
      });
  this.oracon = null;
}

private JCheckBox analyzeCheck;
private JButton copydata_dtn;
private JButton dataprocessingBtn;
private JCheckBox davnostByYearCheck;
private JComboBox davnost_combo;
private JButton dbconnect_btn;
private JTextField dbip_edit;
private JTextField dbname_edit;
private JTextField dbpass_edit;
private JTextField dbport_edit;
private JTextField dbuser_edit;
private JTextField einText;
private JComboBox<String> imot_old_combo;
private JLabel jLabel1;
private JLabel jLabel10;
private JLabel jLabel11;
private JLabel jLabel12;
private JLabel jLabel13;
private JLabel jLabel14;
private JLabel jLabel15;
private JLabel jLabel2;
private JLabel jLabel3;
private JLabel jLabel4;
private JLabel jLabel5;
private JLabel jLabel6;
private JLabel jLabel7;
private JLabel jLabel8;
private JLabel jLabel9;
private JPanel jPanel1;
private JPanel jPanel2;
private JTextField kasaadr_edit;
private JPanel mainPanel;
private JTextField minsuma_edit;
private ButtonGroup mps_btngroup;
private JComboBox<String> mps_cur_combo;
private JComboBox<String> mps_old_combo;
private JCheckBox nedd_by_year_check;
private ButtonGroup oldmsp_btngroup;
private JComboBox<String> other_combo;
private JTextField paycode_edit;
private JButton print_btn;
private JProgressBar progressBar;
private JCheckBox showPINCheck;
private JButton singleMsgPrintBtn;
private JCheckBox spr_check;
private JLabel st_label;
private JLabel statusAnimationLabel;
private JLabel statusMessageLabel;
private JPanel statusPanel;
private JCheckBox with_tbotext_check;
private JCheckBox without_dead_check;
private JTextField wpay_edit;
private final Timer messageTimer;
private final Timer busyIconTimer;
private final Icon idleIcon;
private final Icon[] busyIcons;
private int busyIconIndex;

private String resourceToString(String resName) throws Exception {
  InputStream istr = getClass().getResourceAsStream("/sql/" + resName);
  StringBuilder sb = new StringBuilder();
  BufferedReader br = new BufferedReader(new InputStreamReader(istr, "UTF-8"));
  int c;
  for (c = br.read(); c != -1; ) {
    sb.append((char)c);
    c = br.read();
  }
  return sb.toString();
}

public void print() {
  String printPath = getJarFolder() + "\\print-" + this.muniId;
  Map<String, Object> params = new HashMap<>();
  params.put("msgyear", "" + this.msgYear);
  params.put("w3pay", this.wpay_edit.getText().trim());
  params.put("wwwspr", Boolean.valueOf(this.spr_check.isSelected()));
  params.put("show_pin", Boolean.valueOf(this.showPINCheck.isSelected()));
  params.put("wwwspr_address", this.webSprAddress);
  params.put("bottomNote", this.bottomNote);
  params.put("bigDNIText", this.bigDNIText);
  params.put("kasa_addr", this.kasaadr_edit.getText());
  params.put("IBAN", this.bankIban);
  params.put("banka", this.bankName);
  params.put("BIC", this.bankCode);
  params.put("ds_name", this.muniName);
  params.put("nomasking", Boolean.valueOf(false));
  try {
    this.st_label.setText("1.Подготовка за печат");
    (new CleanDAO(this.oracon, this.msgYear)).cleanPages();
    this.st_label.setText("2.Маркиране на едностранните");
    PrintingHelperDAO printHelperDAO = new PrintingHelperDAO(this.oracon, this.msgYear);
    ReportParamsEntity reportParamsEntity = new ReportParamsEntity();
    reportParamsEntity.setVirtualizerPages(5000);
    reportParamsEntity.setPageSides(1);
    this.pagesHash = new HashMap<>();
    if (this.muniId == 1227L && this.msgYear < 2019) {
      printHelperDAO.markSinglesPages(9);
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.isperson=1";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setReportQuerySQLParamCount(2);
      reportParamsEntity.setReportTemplateName("saob_2");
      reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
      printSinglePersonGroup("3.Печат многостранни ФЛ ", reportParamsEntity, printPath + "\\person", true, params);
      sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.isperson!=1";
      reportParamsEntity.setGroupSQL(sql);
      printSinglePersonGroup("4.Печат многостранни ЮЛ ", reportParamsEntity, printPath + "\\organization", true, params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.isperson=1 {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("5.Едностранни ФЛ ", reportParamsEntity, printPath + "\\person", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("6.Двустранни ФЛ ", reportParamsEntity, printPath + "\\person", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(600);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("7.4-ри странни ФЛ ", reportParamsEntity, printPath + "\\person", params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.isperson!=1 {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("8.Едностранни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("9.Двустранни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("10.4-ри странни ЮЛ ", reportParamsEntity, printPath + "\\organization", params);
    } else if (this.muniId == 1257L) {
      printHelperDAO.markSinglesPages(8);
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND (l.city_lice_id in (65231,100205) or l.dscode!='2313')";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setReportQuerySQLParamCount(2);
      reportParamsEntity.setReportTemplateName("saob_samokov");
      reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
      printSinglePersonGroup("3.Печат многостранни - без селата ", reportParamsEntity, printPath + "\\withot_villages", true, params);
      String[][] aVillafes = {
          { "391", "alino" }, { "3441", "beli_isakr" }, { "3767", "belchin" }, { "3770", "belchinski_bani" }, { "15285", "govedarci" }, { "16599", "gorni_okol" }, { "18201", "gucal" }, { "22469", "dolni_okol" }, { "23039", "dospei" }, { "23491", "dragushinovo" },
          { "31228", "zlokuchene" }, { "37294", "klisura" }, { "37527", "kovachevci" }, { "43832", "lisec" }, { "46067", "madzhare" }, { "46276", "mala_carkva" }, { "47264", "marica" }, { "52249", "novo_selo" }, { "57697", "popovyane" }, { "58548", "prodanovci" },
          { "61604", "raduil" }, { "61922", "rayovo" }, { "62486", "relyovo" }, { "100206", "chervenata_zemya_dragushinovo" }, { "83243", "shipochane" }, { "83291", "shiroki_dol" }, { "87535", "yarebkovica" }, { "87552", "yarlovo" } };
      int i;
      for (i = 0; i < aVillafes.length; i++) {
        sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id=" + aVillafes[i][0];
        reportParamsEntity.setGroupSQL(sql);
        printSinglePersonGroup("4." + (i + 1) + " Печат многостранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], true, params);
      }
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND (s.city_lice_id in (65231,100205) or s.dscode!='2313') {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("5.Едностранни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("6.Двустранни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("7.4-ри странни - без села ", reportParamsEntity, printPath + "\\withot_villages", params);
      for (i = 0; i < aVillafes.length; i++) {
        sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id=" + aVillafes[i][0] + " {:P_FILTER2} ");
        reportParamsEntity.setGroupSQL(sql);
        reportParamsEntity.setMaxPagesInFile(5000);
        reportParamsEntity.setPageSides(1);
        printMultipleTaxsubjectsMsg("8." + (i + 1) + "a Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
        reportParamsEntity.setGroupSQL(sql);
        reportParamsEntity.setMaxPagesInFile(2500);
        reportParamsEntity.setPageSides(2);
        printMultipleTaxsubjectsMsg("8." + (i + 1) + "б Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
        reportParamsEntity.setGroupSQL(sql);
        reportParamsEntity.setMaxPagesInFile(500);
        reportParamsEntity.setPageSides(4);
        printMultipleTaxsubjectsMsg("8." + (i + 1) + "в Едностранни - " + aVillafes[i][1].toUpperCase() + " ", reportParamsEntity, printPath + "\\villages\\" + aVillafes[i][1], params);
      }
    } else if (this.muniId == 1057L) {
      printHelperDAO.markSinglesPages(9);
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id=36525";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setReportQuerySQLParamCount(2);
      reportParamsEntity.setReportTemplateName("saob_2");
      reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
      printSinglePersonGroup("3.Печат многостранни - гр.Карнобат ", reportParamsEntity, printPath + "\\karnobat", true, params);
      sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.city_lice_id!=36525 AND l.dscode='0205'";
      reportParamsEntity.setGroupSQL(sql);
      printSinglePersonGroup("4.Печат многостранни - села ", reportParamsEntity, printPath + "\\villages", true, params);
      sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND l.dscode!='0205'";
      reportParamsEntity.setGroupSQL(sql);
      printSinglePersonGroup("5.Печат многостранни - БП ", reportParamsEntity, printPath + "\\bulgaria", true, params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id=36525 {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("6.Едностранни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("7.Двустранни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("8.4-ри странни - Карнобат ", reportParamsEntity, printPath + "\\karnobat", params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.city_lice_id!=36525 AND s.dscode='0205' {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("9.Едностранни - села ", reportParamsEntity, printPath + "\\villages", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("10.Двустранни - села ", reportParamsEntity, printPath + "\\villages", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("11.4-ри странни - села ", reportParamsEntity, printPath + "\\villages", params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND s.dscode!='0205' {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("12.Едностранни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("13.Двустранни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("14.4-ри странни - БП ", reportParamsEntity, printPath + "\\bulgaria", params);
    } else if (this.muniId == 1054L) {
      printHelperDAO.markSinglesPages(8);
      reportParamsEntity.setVirtualizerPages(5000);
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND z.msgnom>0 AND l.other_msg_code=2";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setReportQuerySQLParamCount(2);
      reportParamsEntity.setReportTemplateName("saob_burgas_1");
      reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
      reportParamsEntity.setPageSides(0);
      printSinglePersonGroup("3.Печат за email ", reportParamsEntity, printPath + "\\by_email", false, params);
      sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND z.msgnom>0 AND l.other_msg_code=4";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setPageSides(0);
      printSinglePersonGroup("5.Печат за БЕЗ СЪОБЩЕНИЕ ", reportParamsEntity, printPath + "\\by_noprint", false, params);
      sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 AND (l.other_msg_code<2 or l.other_msg_code=3)";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setPageSides(1);
      printSinglePersonGroup("6.Печат многостранни ", reportParamsEntity, printPath, true, params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 AND (s.other_msg_code<2 or s.other_msg_code=3) {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("4.Едностранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("5.Двустранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("6.4-ри странни ", reportParamsEntity, printPath, params);
    } else if (this.muniId == 1060L) {
      printHelperDAO.markSinglesPages(7);
      reportParamsEntity.setVirtualizerPages(5000);
      reportParamsEntity.setReportQuerySQLParamCount(4);
      reportParamsEntity.setReportTemplateName("saob_by_year_pomorie");
      reportParamsEntity.setReportSQLFileName("print_saob_by_year.sql");
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0 ";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setPageSides(1);
      printSinglePersonGroup("1.Печат многостранни ", reportParamsEntity, printPath, true, params);
      sql = resourceToString("print_saob_by_year.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("2.Едностранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("3.Двустранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("4.4-ри странни ", reportParamsEntity, printPath, params);
    } else {
      printHelperDAO.markSinglesPages(8);
      String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.badaddress=0 AND l.pages=0 AND z.msgnom>0";
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setReportQuerySQLParamCount(2);
      reportParamsEntity.setReportTemplateName("saob_2");
      reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
      printSinglePersonGroup("3.Печат многостранни ", reportParamsEntity, printPath, true, params);
      sql = resourceToString("print_saob_1.sql").replace("{:P_FILTER}", " AND s.pages=? AND s.badaddress=0 AND z.msgnom>0 {:P_FILTER2} ");
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(5000);
      reportParamsEntity.setPageSides(1);
      printMultipleTaxsubjectsMsg("4.Едностранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(2500);
      reportParamsEntity.setPageSides(2);
      printMultipleTaxsubjectsMsg("5.Двустранни ", reportParamsEntity, printPath, params);
      reportParamsEntity.setGroupSQL(sql);
      reportParamsEntity.setMaxPagesInFile(500);
      reportParamsEntity.setPageSides(4);
      printMultipleTaxsubjectsMsg("6.4-ри странни ", reportParamsEntity, printPath, params);
    }
    AnalyzerDAO analyzeDAO = new AnalyzerDAO(this.oracon);
    analyzeDAO.vacumTable("dgp_msg.msg_lica");
    analyzeDAO.vacumTable("dgp_msg.msg_zad");
    this.st_label.setText("Съобщенията са отпечатани");
  } catch (Exception ex) {
    ex.printStackTrace();
    JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
  }
}

public void printSinglePersonGroup(String caption, ReportParamsEntity reportParamsEntity, String basePath, boolean separateByPageCount, Map<String, Object> params) throws Exception {
  String countSQL = reportParamsEntity.getGroupSQL().replaceAll("SELECT([^<]*)FROM", "SELECT count(distinct l.taxsubject_id) FROM ");
  long totalRecord = 0L;
  long curRecord = 0L;
  this.pagesHash = new HashMap<>();
  params.put("multipleFile", Boolean.valueOf(false));
  CallableStatement st = this.oracon.prepareCall(countSQL);
  st.setInt(1, this.msgYear);
  ResultSet rs = st.executeQuery();
  if (rs.next())
    totalRecord = rs.getLong(1);
  rs.close();
  st.close();
  PreparedStatement pageSt = this.oracon.prepareStatement("UPDATE dgp_msg.msg_lica SET PAGES=? WHERE taxsubject_id=? AND msgyear=?");
  String reportSQL = resourceToString(reportParamsEntity.getReportSQLFileName());
  reportParamsEntity.setReportQuerySQL(reportSQL.replace("{:P_FILTER}", " AND s.taxsubject_id=? "));
  st = this.oracon.prepareCall(reportParamsEntity.getGroupSQL() + " ORDER BY z.msgnom");
  st.setInt(1, this.msgYear);
  rs = st.executeQuery();
  while (rs.next()) {
    curRecord++;
    this.st_label.setText(caption + curRecord + "/" + totalRecord + " : " + rs.getString("ein"));
    int pageCount = printSinglePersonMsg(rs.getLong("taxsubject_id"), basePath, separateByPageCount, reportParamsEntity, params);
    pageSt.setInt(1, pageCount);
    pageSt.setLong(2, rs.getLong("taxsubject_id"));
    pageSt.setInt(3, this.msgYear);
    pageSt.execute();
  }
  this.oracon.commit();
  rs.close();
  st.close();
}

public int printSinglePersonMsg(long taxsubjectId, String basePath, boolean separateByPageCount, ReportParamsEntity reportParamsEntity, Map<String, Object> params) throws Exception {
  CallableStatement st = this.oracon.prepareCall(reportParamsEntity.getReportQuerySQL());
  st.setInt(1, this.msgYear);
  st.setLong(2, taxsubjectId);
  if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
    st.setInt(3, this.msgYear);
    st.setLong(4, taxsubjectId);
  }
  ResultSet rs = st.executeQuery();
  JRResultSetDataSource rsDataSource = new JRResultSetDataSource(rs);
  InputStream jasperIS = getClass().getResourceAsStream("/reports/" + reportParamsEntity.getReportTemplateName() + ".jasper");
  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperIS, params, (JRDataSource)rsDataSource);
  int pageCount = jasperPrint.getPages().size();
  if ((pageCount > 2 && pageCount != 4) || reportParamsEntity.getPageSides() == 0) {
    String pdfPath;
    int hKey;
    if (separateByPageCount) {
      hKey = pageCount;
      pdfPath = basePath + "\\" + pageCount + "-pages";
    } else {
      hKey = 0;
      pdfPath = basePath;
    }
    Integer i = this.pagesHash.get(Integer.valueOf(hKey));
    if (i == null) {
      i = Integer.valueOf(0);
      File dir = new File(pdfPath);
      if (!dir.exists())
        dir.mkdirs();
    }
    Integer integer1 = i, integer2 = i = Integer.valueOf(i.intValue() + 1);
    JRPdfExporter exporter = new JRPdfExporter();
    exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "CP1251");
    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, pdfPath + "\\" + i + ".pdf");
    exporter.exportReport();
    this.pagesHash.put(Integer.valueOf(hKey), i);
  }
  rs.close();
  st.close();
  return pageCount;
}

private void printMultipleTaxsubjectsMsg(String caption, ReportParamsEntity reportParamsEntity, String basePath, Map<String, Object> params) throws Exception {
  params.put("multipleFile", Boolean.valueOf(true));
  this.reportStatus = new ReporStatustHelper();
  this.reportStatus.setPdfPath(basePath + "\\" + reportParamsEntity.getPageSides() + "-pages");
  CallableStatement st = this.oracon.prepareCall("SELECT count(distinct msgnom) FROM (" + reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", "") + ") q0");
  st.setInt(1, this.msgYear);
  st.setInt(2, reportParamsEntity.getPageSides());
  if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
    st.setInt(3, this.msgYear);
    st.setInt(4, reportParamsEntity.getPageSides());
  }
  ResultSet rs = st.executeQuery();
  if (!rs.next())
    throw new Exception("Грешна заявка за общ брой в printMultipleTaxsubjectsMsg");
  this.reportStatus.setTotalQueuedMessages(rs.getInt(1));
  rs.close();
  st.close();
  File dir = new File(this.reportStatus.getPdfPath());
  if (!dir.exists())
    dir.mkdirs();
  int i = this.reportStatus.getTotalQueuedMessages();
  int sqlOffset = 0;
  int part = 1;
  while (i > 0) {
    this.reportStatus.setPart(part);
    this.st_label.setText(caption + " - подготовка част " + this.reportStatus.getPart());
    if (i >= reportParamsEntity.getMaxPagesInFile()) {
      this.reportStatus.setPartTotalQueuedMessages(reportParamsEntity.getMaxPagesInFile());
    } else {
      this.reportStatus.setPartTotalQueuedMessages(i);
    }
    st = this.oracon.prepareCall("SELECT min(msgnom) minnom,max(msgnom) maxnom FROM (SELECT distinct msgnom FROM (" + reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", "") + ") q0 ORDER BY msgnom OFFSET " + sqlOffset + " LIMIT " + reportParamsEntity.getMaxPagesInFile() + ") q1");
    st.setInt(1, this.msgYear);
    st.setInt(2, reportParamsEntity.getPageSides());
    if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
      st.setInt(3, this.msgYear);
      st.setInt(4, reportParamsEntity.getPageSides());
    }
    rs = st.executeQuery();
    if (!rs.next())
      throw new Exception("Грешна заявка за частичен брой в printMultipleTaxsubjectsMsg");
    String partSQL = reportParamsEntity.getGroupSQL().replace("{:P_FILTER2}", " AND z.msgnom>=" + rs.getInt(1) + " AND z.msgnom<=" + rs.getInt(2) + " ");
    printMultiple(caption, reportParamsEntity, partSQL, params);
    sqlOffset += reportParamsEntity.getMaxPagesInFile();
    part++;
    i -= reportParamsEntity.getMaxPagesInFile();
  }
}

private void printMultiple(String caption, ReportParamsEntity reportParamsEntity, String sql, Map<String, Object> params) throws Exception {
  JRFileVirtualizer virtualizer = new JRFileVirtualizer(reportParamsEntity.getVirtualizerPages());
  params.put("REPORT_VIRTUALIZER", virtualizer);
  CallableStatement st = this.oracon.prepareCall(sql);
  st.setInt(1, this.msgYear);
  st.setInt(2, reportParamsEntity.getPageSides());
  if (reportParamsEntity.getReportQuerySQLParamCount() == 4) {
    st.setInt(3, this.msgYear);
    st.setInt(4, reportParamsEntity.getPageSides());
  }
  ResultSet rs = st.executeQuery();
  JRResultSetDataSource rsDataSource = new JRResultSetDataSource(rs);
  InputStream jasperIS = getClass().getResourceAsStream("/reports/" + reportParamsEntity.getReportTemplateName() + ".jasper");
  JasperReport report = (JasperReport)JRLoader.loadObject(jasperIS);
  AsynchronousFillHandle handle = AsynchronousFillHandle.createHandle(report, params, (JRDataSource)rsDataSource);
  this.reportStatus.startReder();
  handle.startFill();
  handle.addFillListener(new FillListener() {
        public void pageGenerated(JasperPrint jasperPrint, int i) {
          YearsaobView.this.reportStatus.nextPage();
        }

        public void pageUpdated(JasperPrint jasperPrint, int i) {}
      });
  handle.addListener(new AsynchronousFilllListener() {
        public void reportFinished(JasperPrint jasperPrint) {
          try {
            YearsaobView.this.reportStatus.setPartTotalRenderedPages(jasperPrint.getPages().size());
            YearsaobView.this.reportStatus.startExport();
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "CP1251");
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, YearsaobView.this.reportStatus.getFullPDFName());
            exporter.exportReport();
          } catch (JRException e) {
            e.printStackTrace();
          }
          YearsaobView.this.reportStatus.finish();
        }

        public void reportCancelled() {
          YearsaobView.this.reportStatus.finish();
        }

        public void reportFillError(Throwable throwable) {
          throwable.fillInStackTrace();
          throwable.printStackTrace();
          YearsaobView.this.reportStatus.finish();
        }
      });
  while (!this.reportStatus.isFinished()) {
    Thread.sleep(600L);
    if (this.reportStatus.inRenderState()) {
      this.st_label.setText(caption + this.reportStatus.getPageCount() + " / " + (this.reportStatus.getTotalQueuedMessages() * reportParamsEntity.getPageSides()) + " страници, " + this.reportStatus.getTotalQueuedMessages() + " съобщения");
      continue;
    }
    this.st_label.setText(caption + " - експорт част " + this.reportStatus.getPart() + ", " + this.reportStatus.getPartTotalRenderedPages() + " страници, " + this.reportStatus.getPartTotalQueuedMessages() + " съобщения");
  }
  if (this.reportStatus.getPartTotalRenderedPages() != reportParamsEntity.getPageSides() * this.reportStatus.getPartTotalQueuedMessages())
    throw new Exception("Генерирания брой страници не е верен !!!!!");
  Thread.sleep(1000L);
  virtualizer.cleanup();
}

public Boolean copydata() {
  String mYear = Integer.toString(this.msgYear);
  String filterByOblog = "1";
  SimpleDateFormat time_formatter = new SimpleDateFormat("HH:mm:ss");
  AnalyzerDAO analyzeDAO = new AnalyzerDAO(this.oracon);
  try {
    createtables();
    this.st_label.setText("1. Подготовка " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    CleanDAO cleanDAO = new CleanDAO(this.oracon, this.msgYear);
    cleanDAO.clean();
    if (this.analyzeCheck.isSelected()) {
      analyzeDAO.vacumTable("dgp_msg.msg_lica");
      analyzeDAO.vacumTable("dgp_msg.msg_zad");
      analyzeDAO.analyzeTable("debtinstalment");
      analyzeDAO.analyzeTable("baldebtinst");
      analyzeDAO.analyzeTable("debtsubject");
      analyzeDAO.analyzeTable("debtpartproperty");
      analyzeDAO.analyzeTable("taxdoc");
      analyzeDAO.analyzeTable("address");
      analyzeDAO.analyzeTable("city");
      analyzeDAO.analyzeTable("street");
      analyzeDAO.analyzeTable("falloff");
    }
    this.st_label.setText("2. Попълване на задължения за имоти за " + mYear + " " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    ImotCurrentZadalDAO imotCurZadDAO = new ImotCurrentZadalDAO(this.oracon, this.msgYear);
    imotCurZadDAO.setByOstatak(true);
    imotCurZadDAO.setForDeklar(true);
    imotCurZadDAO.execute();
    imotCurZadDAO.setForDeklar(false);
    imotCurZadDAO.execute();
    this.st_label.setText("3. Попълване на липсващи данъчни оценки " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    imotCurZadDAO.fillMissingDO();
    if (this.mps_cur_combo.getSelectedIndex() < 2) {
      this.st_label.setText("4. Попълване на задължения за ПС за " + mYear + " " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      MPSCurrentZadalDAO mpsCurrZadDAO = new MPSCurrentZadalDAO(this.oracon, this.msgYear, (this.mps_cur_combo.getSelectedIndex() == 1));
      mpsCurrZadDAO.setByOstatak(true);
      mpsCurrZadDAO.setForDeklar(true);
      mpsCurrZadDAO.execute();
      mpsCurrZadDAO.setForDeklar(false);
      mpsCurrZadDAO.execute();
      mpsCurrZadDAO.moveSecondPayPeriodToFirst();
      if (this.mps_old_combo.getSelectedIndex() < 3) {
        this.st_label.setText("5. Попълване на недобори за ПС " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
        MPSOldZadDAO mpsOldZadDao = new MPSOldZadDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
        mpsOldZadDao.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
        mpsOldZadDao.setDanvostFlag(this.davnost_combo.getSelectedIndex());
        mpsOldZadDao.setFilterByExistingZad(this.mps_old_combo.getSelectedIndex());
        mpsOldZadDao.execute();
        mpsOldZadDao = null;
      }
    }
    if (this.muniId == 1058L) {
      this.st_label.setText("5a. Канали и радиоточки за текуща и мин.години " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      RadioKanaliMalkoTarnovoZadalDAO radioKanalMTarnovoZadalDAO = new RadioKanaliMalkoTarnovoZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
      radioKanalMTarnovoZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
      radioKanalMTarnovoZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
      radioKanalMTarnovoZadalDAO.execute();
    } else if (this.muniId == 1064L) {
      this.st_label.setText("5a. Гаражи и радиоточки за текуща и мин.години " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      GarageSredecZadalDAO garageSredecZadalDAO = new GarageSredecZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
      garageSredecZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
      garageSredecZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
      garageSredecZadalDAO.execute();
    }
    if (this.other_combo.getSelectedIndex() < 3) {
      if (this.paycode_edit.getText().contains("8013")) {
        DogCurrentZadalDAO dogCurrZadDAO = new DogCurrentZadalDAO(this.oracon, this.msgYear, this.other_combo.getSelectedIndex());
        dogCurrZadDAO.setByOstatak(true);
        dogCurrZadDAO.setForDeklar(true);
        dogCurrZadDAO.execute();
        dogCurrZadDAO.setForDeklar(false);
        dogCurrZadDAO.execute();
      }
      this.st_label.setText("6. Попълване на недобори за други данъци " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      OtherOldZadalDAO otherOldZadalDAO = new OtherOldZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected(), this.paycode_edit.getText());
      otherOldZadalDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
      otherOldZadalDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
      otherOldZadalDAO.setFilterByExistingZad(this.other_combo.getSelectedIndex());
      otherOldZadalDAO.execute();
    }
    if (this.imot_old_combo.getSelectedIndex() < 3) {
      this.st_label.setText("7. Попълване на недобори за имоти " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      ImotOldZadalDAO imotOldZadDAO = new ImotOldZadalDAO(this.oracon, this.msgYear, this.nedd_by_year_check.isSelected());
      imotOldZadDAO.setFilterDavnostByYear(this.davnostByYearCheck.isSelected());
      imotOldZadDAO.setDanvostFlag(this.davnost_combo.getSelectedIndex());
      imotOldZadDAO.setFilterByExistingZad(this.imot_old_combo.getSelectedIndex());
      imotOldZadDAO.execute();
      imotOldZadDAO = null;
    }
    if (this.muniId == 1060L) {
      this.st_label.setText("7a. Групиране на недоборите " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      cleanDAO.groupNedobor();
    }
    this.st_label.setText("8. Попълване на данните за имоти/ПС " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    (new FillObjectDataDAO(this.oracon, this.msgYear)).execute();
    analyzeDAO.analyzeTable("dgp_msg.msg_zad");
    if (this.muniId == 1228L)
      cleanDAO.cleanWithoutImot();
    this.oracon.commit();
    this.st_label.setText("9. Изчистване на малките недобори " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    cleanDAO.deleteUnderValue(this.minsuma_edit.getText());
    cleanDAO.deleteUnderValueOtherCurYear(this.minsuma_edit.getText());
    if (this.muniId == 1056L && this.msgYear == 2019)
      cleanDAO.cleanALlUnderValue10();
    if (this.muniId != 1257L) {
      this.st_label.setText("10. Изтриване задълженията на ОБЩИНАТА " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      cleanDAO.deleteMunicipalityZadal();
    }
    cleanDAO.deleteOtherSobst();
    analyzeDAO.analyzeTable("taxsubject");
    analyzeDAO.analyzeTable("person");
    if (this.without_dead_check.isSelected()) {
      this.st_label.setText("11. Изтриване на починалите " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      cleanDAO.deleteDead();
    }
    if (this.muniId == 1227L || this.muniId == 1300L)
      (new DogCurrentZadalDAO(this.oracon, this.msgYear, this.other_combo.getSelectedIndex())).deleteOnlyDogNovaZagora();
    this.st_label.setText("12. Попълване данните за лицата " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    if (this.muniId == 1257L) {
      FillSubjectDataSamokovDAO fillSubjectDataDAO = new FillSubjectDataSamokovDAO(this.oracon, this.msgYear, this.muniId, this.alternativeSendMsgCheck.isSelected(), this.addr_type_combo.getSelectedIndex());
      fillSubjectDataDAO.execute();
    } else {
      FillSubjectDataDAO fillSubjectDataDAO = new FillSubjectDataDAO(this.oracon, this.msgYear, this.muniId, this.alternativeSendMsgCheck.isSelected(), this.addr_type_combo.getSelectedIndex());
      fillSubjectDataDAO.execute();
      if (this.muniId == 1054L) {
        fillSubjectDataDAO.setOtherMsgCodeBurgas();
        if (this.msgYear == 2018)
          cleanDAO.cleanMPSCurrentYearOnly();
      }
    }
    if (this.muniId == 1257L)
      cleanDAO.markChujdenciSamokov();
    this.st_label.setText("13. Анонимизиране на партиди " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    (new MaskPartidaDAO(this.oracon, this.msgYear, this.muniCode)).execute();
    if (this.with_tbotext_check.isSelected()) {
      this.st_label.setText("14. Попълване на текста на промил ТБО " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      if (this.muniId == 1060L) {
        imotCurZadDAO.fillTBOPromilTextPomorie();
      } else {
        imotCurZadDAO.fillTBOPromilText();
      }
    }
    if (this.muniId == 1060L || this.muniId == 1061L || this.muniId == 1226L) {
      this.st_label.setText("15. Попълване на текста на промил ДНИ " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      imotCurZadDAO.fillDNIPromilTextPomorie();
    } else if (this.muniId == 1058L || this.muniId == 1300L) {
      this.st_label.setText("15. Попълване на текста на промил ДНИ " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
      imotCurZadDAO.fillDNIPromil22Text();
    }
    this.st_label.setText("Финализиране " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
    analyzeDAO.vacumTable("dgp_msg.msg_lica");
    analyzeDAO.vacumTable("dgp_msg.msg_zad");
    this.st_label.setText("Данните са извлечени " + time_formatter.format(Long.valueOf(System.currentTimeMillis())));
  } catch (Exception ex) {
    ex.printStackTrace();
    JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
  }
  return Boolean.valueOf(true);
}

public Boolean dataPreProcessing() {
  try {
    this.st_label.setText("1. Инициализация...");
    (new CleanDAO(this.oracon, this.msgYear)).cleanPages();
    this.st_label.setText("2. Номериране");
    MsgNumberingDAO msgNumberingDAO = new MsgNumberingDAO(this.oracon, this.msgYear);
    if (this.nedd_by_year_check.isSelected()) {
      msgNumberingDAO.numberingByYear();
    } else {
      msgNumberingDAO.numbering();
    }
    this.st_label.setText("Данните са готови за печат");
  } catch (Exception ex) {
    ex.printStackTrace();
    JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
  }
  return Boolean.valueOf(true);
}

public void readMuniData() {
  try {
    this.oracon.prepareStatement("CREATE SCHEMA IF NOT EXISTS dgp_msg").executeUpdate();
    ResultSet rs = this.oracon.createStatement().executeQuery("SELECT c.municipality_id,c.configvalue,m.name,ebk_code FROM config c JOIN municipality m ON m.municipality_id=c.municipality_id WHERE c.name='TAXYEAR14'");
    if (rs.next()) {
      this.muniId = rs.getLong("municipality_id");
      this.msgYear = rs.getInt("configvalue");
      this.muniName = rs.getString("name");
      this.muniCode = rs.getString("ebk_code");
    }
    rs.close();
    rs = this.oracon.createStatement().executeQuery("SELECT bac.iban,b.bic,b.fullname FROM baccount bac JOIN bank b ON b.bank_id=bac.bank_id WHERE bac.isbase=1 AND isactive=1");
      if (rs.next()) {
        this.bankIban = rs.getString("iban");
        this.bankCode = rs.getString("bic");
        this.bankName = rs.getString("fullname");
      }
      rs.close();
      rs = this.oracon.createStatement().executeQuery("select coalesce(o.note,'') || ', ' || coalesce(o.address, '') offaddress from Office o where o.isactive = 1 and o.office_id=1");
      if (rs.next())
        this.kasaAddres = rs.getString("offaddress");
      rs.close();
      this.bottomNote = "След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено. Ако междувременно сте платили задълженията си, моля да ни извините за безпокойството !";
      this.bigDNIText = null;
      if (this.muniId == 1227L) {
        this.kasaAddres = "на касите на отдел \"Местни данъци и такси\"        ул.\"24-Май\" №1, гр.Нова Загора";
        this.bankName = "Алианц Банк България";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\",                             касите на EasyPay или офисите на Банка ДСК.";
        this.webSprAddress = " или на адрес https://mdt.nova-zagora.org";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1062L) {
        this.kasaAddres = "на каса с адрес с.Руен, ул.\"Първи Май\" №18           и в кметствата на селата от община Руен";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\".";
        this.webSprAddress = " на адрес https://mpdt.obstinaruen.com";
        this.bankName = "Алианц Банк България АД";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1061L) {
        this.kasaAddres = "на каса с адрес гр.Приморско,                     ул.\"Трети Март\" №56";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
        this.webSprAddress = " на адрес https://mdt.primorsko.bg:8543 ";
        this.bottomNote = "След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено. Ако междувременно сте платили задълженията си, моля да ни извините за безпокойството!<br>Телефони за контакти: 0550 33695; 0550 33626; 0550 33603 и 0550 32002.";
        this.bigDNIText = "Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в гр.Приморско и гр.Китен, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 5.0 /пет/ промила.<br>";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1058L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
        this.bankName = "БАНКА ДСК";
        this.webSprAddress = " на адрес https://62.176.74.163 ";
        this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153/24.02.2012г., ДНИ за жилищни имоти в гр.Малко Търново, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 4.5 промила.<br>";
        this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
      } else if (this.muniId == 1064L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
        this.bankName = "Юробанк България АД";
        this.webSprAddress = " на адрес https://mdt.sredets.bg ";
        this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
      } else if (this.muniId == 1226L) {
        this.webSprAddress = " на адрес https://mdt.kotel.bg ";
        this.kasaAddres = "на каса с адрес гр.Котел,ул.\"Алеко Богориди\" №1";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и офисите на Банка ДСК.";
        this.bankName = "ОБЩИНСКА БАНКА АД";
        this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в гр.Котел, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 5.0 /пет/ промила.<br>";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1228L) {
        this.webSprAddress = " на адрес https://mdt.tvarditsa.org ";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
        this.bankName = "ОБЩИНСКА БАНКА АД";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1057L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
        this.bankName = "Централна Кооперативна Банка АД";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1300L) {
        this.bigDNIText = "* Съгласно изменения в ЗМДТ в сила от 01.01.2019г. и РМС №153 от 24.02.2012г., ДНИ за жилищни имоти в с.Стефан Караджово, които не са основно жилище, не са отдадени под наем и не са регистрирани като места за настаняване по смисъла на Закона за туризма е в размер на 4.5 промила.<br>";
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             и касите на EasyPay.";
        this.kasaAddres = "на каса с адрес гр.Болярово,                          ул.\"Димитър Благоев\" №7";
        this.bankName = "ОБЕДИНЕНА БЪЛГАРСКА БАНКА";
        this.bankCode = "UBBSBGSF";
        this.kasaadr_edit.setText(this.kasaAddres);
        this.webSprAddress = " на адрес https://213.91.194.72 ";
      } else if (this.muniId == 1283L) {
        this.otherPay = "или във всеки пощенски клон с пощенски запис за данъчно плащане.";
        this.kasaAddres = "на каса с адрес с.Минерални Бани,                             бул.\"Васил Левски\" №3";
        this.bankName = "ТЪРГОВСКА БАНКА \"Д\" ";
        this.webSprAddress = " mineralnibani.bg ";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1055L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
        this.kasaAddres = "на каса с адрес гр.Айтос,                                           ул.\"Цар Освободител\" №3";
        this.bankName = "ТБ \"АЛИАНЦ БАНК БЪЛГАРИЯ\" АД";
        this.webSprAddress = " на адрес https://mdt.aytos.bg ";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1056L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                              и касите на EasyPay.";
        this.kasaAddres = "на каса с адрес гр.Камено,                                           ул.\"Освобождение\" №101";
        this.webSprAddress = " на адрес https://mdt.kameno.bg ";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1065L) {
        this.otherPay = "или във всяка пощенска станция на \"Български пощи\"                             касите на EasyPay или офисите на Банка ДСК.";
        this.kasaAddres = "на каса с адрес гр.Сунгурларе,                                           ул.\"Г.Димитров\" №2, ет.1";
        this.bankName = "УНИКРЕДИТ БУЛБАНК";
        this.webSprAddress = " на адрес https://mdt.sungurlare.org ";
        this.bottomNote += "<br>За справка позвънете на тел. 05571 5703";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else if (this.muniId == 1257L) {
        this.otherPay = "<b>или</b> чрез \"Български пощи\", Изипей, Фастпай<br>      и през интернет страницата на Общината.";
        this.kasaAddres = "на касите на Община Самоков                                             и в кметствата в селата на община Самоков";
        this.bankName = "ЦКБ АД КЛОН САМОКОВ";
        this.bottomNote = "Дължими са единствено сумите, които не са платени към датата на получаване на съобщението. След изтичането на всеки от сроковете върху невнесеното задължение се начислява лихва. Стойността на данъчната оценка отговаря на частта от имота, за която лицето е данъчнозадължено.";
        this.kasaadr_edit.setText(this.kasaAddres);
      } else {
        this.kasaadr_edit.setText("на каса с адрес " + this.kasaAddres);
        this.otherPay = "или във всеки пощенски клон с пощенски запис за данъчно плащане, интернет или чрез EasyPay";
      }
      this.wpay_edit.setText(this.otherPay);
      this.oracon.createStatement().execute(resourceToString("create_functions.sql"));
      this.oracon.commit();
    } catch (Exception ex) {
      ex.printStackTrace();
      JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
    }
  }

  public boolean isTableExists(String tableName, String shema) {
    boolean result = false;
    try {
      ResultSet rs = this.oracon.createStatement().executeQuery("SELECT count(*) FROM information_schema.tables WHERE table_name = '" + tableName + "' AND table_schema='" + shema + "'");
      if (rs.next() && rs.getInt(1) > 0)
        result = true;
      rs.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return result;
  }

  public boolean isColumnExists(String column, String table, String shema) {
    boolean result = false;
    try {
      ResultSet rs = this.oracon.createStatement().executeQuery("SELECT column_name FROM information_schema.columns WHERE table_name= '" + table + "' AND table_schema='" + shema + "' AND UPPER(column_name)='" + column.toUpperCase() + "'");
      result = rs.next();
      rs.close();
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return result;
  }

  public void createtables() throws Exception {
    try {
      if (isTableExists("msg_lica", "dgp_msg")) {
        if (!isColumnExists("email", "msg_lica", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ADD email character varying(255)");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("other_msg_code", "msg_lica", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ADD other_msg_code numeric(2,0) NOT NULL DEFAULT 0");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        PreparedStatement ps = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_lica ALTER COLUMN ein TYPE character varying(30)");
        ps.executeUpdate();
        ps.close();
        if (!isColumnExists("city_lice_id", "msg_lica", "dgp_msg")) {
          ps = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_lica RENAME COLUMN kn_l TO city_lice_id;");
          ps.executeUpdate();
          ps = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_lica alter COLUMN city_lice_id type  numeric using (case when city_lice_id='*' then -1 when coalesce(city_lice_id,'')='' then 0 else city_lice_id::numeric end);");
          ps.executeUpdate();
          ps.close();
        }
      } else {
        PreparedStatement ps = this.oracon.prepareStatement("CREATE TABLE dgp_msg.msg_lica ( taxsubject_id numeric NOT NULL, msgyear numeric(4,0) NOT NULL, ein character varying(20) NOT NULL, isperson numeric(1,0) NOT NULL, isdead numeric(1,0) NOT NULL, ime character varying(200) NOT NULL, dscode character varying(5) NOT NULL, myname character varying(50) NOT NULL, cityname character varying(100) NOT NULL, muniname character varying(50) NOT NULL, city_lice_id numeric NOT NULL, ulimel character varying(200), pin character varying(20), email character varying(255), other_msg_code numeric(2,0) NOT NULL DEFAULT 0, badaddress numeric(1,0) NOT NULL DEFAULT 0, pages numeric(5,0) NOT NULL DEFAULT 0,  CONSTRAINT msg_lica_pkey PRIMARY KEY (taxsubject_id, msgyear))");
        ps.executeUpdate();
        ps.close();
      }
      if (isTableExists("msg_zad", "dgp_msg")) {
        if (!isColumnExists("taxobject_id", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD taxobject_id  numeric");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("promsm", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ALTER COLUMN promsm TYPE character varying(300)");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("city_i_id", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_zad RENAME COLUMN kn_i TO city_i_id;");
          preparedStatement.executeUpdate();
          preparedStatement = this.oracon.prepareStatement(" ALTER TABLE dgp_msg.msg_zad alter COLUMN city_i_id type  numeric using city_i_id::numeric;");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("promdni", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN promdni character varying(200)");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("dan_nedd_min_year", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN dan_nedd_min_year numeric(4,0) default 0;");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("dan_nedd_max_year", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN dan_nedd_max_year numeric(4,0) default 0;");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("smet_nedd_min_year", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN smet_nedd_min_year numeric(4,0) default 0;");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
        if (!isColumnExists("smet_nedd_max_year", "msg_zad", "dgp_msg")) {
          PreparedStatement preparedStatement = this.oracon.prepareStatement("ALTER TABLE dgp_msg.msg_zad ADD COLUMN smet_nedd_max_year numeric(4,0) default 0;");
          preparedStatement.executeUpdate();
          preparedStatement.close();
        }
      } else {
        PreparedStatement preparedStatement = this.oracon.prepareStatement("CREATE TABLE dgp_msg.msg_zad (  id numeric NOT NULL,  taxsubject_id numeric NOT NULL,  msgyear numeric(4,0) NOT NULL,  paycode character varying(4) NOT NULL,  sortcode numeric NOT NULL,  foryear numeric(4,0) NOT NULL,  parnom character varying(30),  nasmi character varying(100),  city_i_id numeric,  ulime character varying(200),  doc numeric(13,2) NOT NULL,  promsm character varying(200),  mypromil numeric(22,5),  dan numeric(10,2) DEFAULT 0,  smet numeric(10,2) DEFAULT 0,  dan5 numeric(10,2) DEFAULT 0,  dan_1 numeric(10,2) DEFAULT 0,  dan_2 numeric(10,2) DEFAULT 0,  smet5 numeric(10,2) DEFAULT 0,  smet_1 numeric(10,2) DEFAULT 0,  smet_2 numeric(10,2) DEFAULT 0,  dan_nedd numeric(11,2) DEFAULT 0,  dan_lih numeric(11,2) DEFAULT 0,  smet_nedd numeric(11,2) DEFAULT 0,  smet_lih numeric(11,2) DEFAULT 0,  date_lix date,  srok1_1 date,  srok1_2 date,  taxdoc_id numeric,  taxobject_id  numeric,   zad_desc character varying(500),  msgnom numeric(7,0) NOT NULL DEFAULT 0,  zadnop numeric(4,0)NOT NULL DEFAULT 0,  maskpartida character varying(30),  promdni character varying(200),  dan_nedd_min_year numeric(4,0) default 0,  dan_nedd_max_year numeric(4,0) default 0,  smet_nedd_min_year numeric(4,0) default 0,  smet_nedd_max_year numeric(4,0) default 0,  CONSTRAINT msg_zad_pkey PRIMARY KEY (id))");
        preparedStatement.executeUpdate();
        preparedStatement.close();
        preparedStatement = this.oracon.prepareStatement("CREATE INDEX msg_zad_ind1 ON dgp_msg.msg_zad USING btree (msgyear,taxsubject_id,sortcode)");
        preparedStatement.executeUpdate();
        preparedStatement.close();
      }
      this.oracon.commit();
    } catch (Exception ex) {
      ex.printStackTrace();
      JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
    }
  }

  private String getJarFolder() {
    String p = (new File(".")).getAbsolutePath();
    return p.substring(0, p.length() - 1);
  }

  private void initComponents() {
    this.mainPanel = new JPanel();
    this.jPanel1 = new JPanel();
    this.jLabel1 = new JLabel();
    this.jLabel2 = new JLabel();
    this.dbip_edit = new JTextField();
    this.jLabel3 = new JLabel();
    this.dbport_edit = new JTextField();
    this.jLabel4 = new JLabel();
    this.dbname_edit = new JTextField();
    this.jLabel5 = new JLabel();
    this.dbuser_edit = new JTextField();
    this.jLabel6 = new JLabel();
    this.dbpass_edit = new JTextField();
    this.dbconnect_btn = new JButton();
    this.jLabel15 = new JLabel();
    this.einText = new JTextField();
    this.singleMsgPrintBtn = new JButton();
    this.jPanel2 = new JPanel();
    this.copydata_dtn = new JButton();
    this.minsuma_edit = new JTextField();
    this.jLabel10 = new JLabel();
    this.davnost_combo = new JComboBox();
    this.jLabel13 = new JLabel();
    this.nedd_by_year_check = new JCheckBox();
    this.jLabel7 = new JLabel();
    this.mps_old_combo = new JComboBox<>();
    this.mps_cur_combo = new JComboBox<>();
    this.jLabel9 = new JLabel();
    this.imot_old_combo = new JComboBox<>();
    this.jLabel12 = new JLabel();
    this.paycode_edit = new JTextField();
    this.other_combo = new JComboBox<>();
    this.addr_type_combo = new JComboBox<>();
    this.jLabel14 = new JLabel();
    this.without_dead_check = new JCheckBox();
    this.with_tbotext_check = new JCheckBox();
    this.alternativeSendMsgCheck = new JCheckBox();
    this.analyzeCheck = new JCheckBox();
    this.davnostByYearCheck = new JCheckBox();
    this.print_btn = new JButton();
    this.jLabel8 = new JLabel();
    this.wpay_edit = new JTextField();
    this.spr_check = new JCheckBox();
    this.jLabel11 = new JLabel();
    this.kasaadr_edit = new JTextField();
    this.dataprocessingBtn = new JButton();
    this.showPINCheck = new JCheckBox();
    this.statusPanel = new JPanel();
    JSeparator statusPanelSeparator = new JSeparator();
    this.statusMessageLabel = new JLabel();
    this.statusAnimationLabel = new JLabel();
    this.progressBar = new JProgressBar();
    this.st_label = new JLabel();
    this.mps_btngroup = new ButtonGroup();
    this.oldmsp_btngroup = new ButtonGroup();
    this.mainPanel.setName("mainPanel");
    this.jPanel1.setBorder(BorderFactory.createEtchedBorder());
    this.jPanel1.setName("jPanel1");
    ResourceMap resourceMap = ((YearsaobApp)Application.getInstance(YearsaobApp.class)).getContext().getResourceMap(YearsaobView.class);
    this.jLabel1.setText(resourceMap.getString("jLabel1.text", new Object[0]));
    this.jLabel1.setName("jLabel1");
    this.jLabel2.setText(resourceMap.getString("jLabel2.text", new Object[0]));
    this.jLabel2.setName("jLabel2");
    this.dbip_edit.setText(resourceMap.getString("dbip_edit.text", new Object[0]));
    this.dbip_edit.setName("dbip_edit");
    this.jLabel3.setText(resourceMap.getString("jLabel3.text", new Object[0]));
    this.jLabel3.setName("jLabel3");
    this.dbport_edit.setText(resourceMap.getString("dbport_edit.text", new Object[0]));
    this.dbport_edit.setName("dbport_edit");
    this.jLabel4.setText(resourceMap.getString("jLabel4.text", new Object[0]));
    this.jLabel4.setName("jLabel4");
    this.dbname_edit.setText(resourceMap.getString("dbname_edit.text", new Object[0]));
    this.dbname_edit.setName("dbname_edit");
    this.jLabel5.setText(resourceMap.getString("jLabel5.text", new Object[0]));
    this.jLabel5.setName("jLabel5");
    this.dbuser_edit.setText(resourceMap.getString("dbuser_edit.text", new Object[0]));
    this.dbuser_edit.setName("dbuser_edit");
    this.jLabel6.setText(resourceMap.getString("jLabel6.text", new Object[0]));
    this.jLabel6.setName("jLabel6");
    this.dbpass_edit.setText(resourceMap.getString("dbpass_edit.text", new Object[0]));
    this.dbpass_edit.setName("dbpass_edit");
    ApplicationActionMap applicationActionMap = ((YearsaobApp)Application.getInstance(YearsaobApp.class)).getContext().getActionMap(YearsaobView.class, this);
    this.dbconnect_btn.setAction(applicationActionMap.get("dbconn_action"));
    this.dbconnect_btn.setText(resourceMap.getString("dbconnect_btn.text", new Object[0]));
    this.dbconnect_btn.setName("dbconnect_btn");
    this.jLabel15.setText(resourceMap.getString("jLabel15.text", new Object[0]));
    this.jLabel15.setName("jLabel15");
    this.einText.setText(resourceMap.getString("einText.text", new Object[0]));
    this.einText.setName("einText");
    this.singleMsgPrintBtn.setAction(applicationActionMap.get("singleMsgPrintAction"));
    this.singleMsgPrintBtn.setText(resourceMap.getString("singleMsgPrintBtn.text", new Object[0]));
    this.singleMsgPrintBtn.setName("singleMsgPrintBtn");
    GroupLayout jPanel1Layout = new GroupLayout(this.jPanel1);
    this.jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.jLabel15)).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false).addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup().addComponent(this.jLabel5).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.dbuser_edit, -2, 89, -2).addGap(18, 18, 18).addComponent(this.jLabel6, -1, -1, 32767)).addGroup(GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup().addComponent(this.jLabel2).addGap(18, 18, 18).addComponent(this.dbip_edit, -2, 85, -2))).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addGroup(jPanel1Layout.createSequentialGroup().addGap(18, 18, 18).addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.dbport_edit, -2, 39, -2)).addGroup(jPanel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(this.dbpass_edit))).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(18, 18, 18).addComponent(this.jLabel4).addGap(10, 10, 10).addComponent(this.dbname_edit, -2, 81, -2)).addGroup(jPanel1Layout.createSequentialGroup().addGap(65, 65, 65).addComponent(this.dbconnect_btn))).addGap(0, 0, 32767))).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.einText, -2, 112, -2).addComponent(this.singleMsgPrintBtn)).addGap(87, 87, 87)));
    jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this.jLabel15).addComponent(this.einText, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this.dbip_edit, -2, -1, -2).addComponent(this.jLabel3).addComponent(this.dbport_edit, -2, -1, -2).addComponent(this.jLabel4).addComponent(this.dbname_edit, -2, -1, -2).addComponent(this.singleMsgPrintBtn)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this.dbuser_edit, -2, -1, -2).addComponent(this.jLabel6).addComponent(this.dbpass_edit, -2, -1, -2).addComponent(this.dbconnect_btn)).addContainerGap(-1, 32767)));
    this.jPanel2.setBorder(BorderFactory.createEtchedBorder());
    this.jPanel2.setName("jPanel2");
    this.copydata_dtn.setAction(applicationActionMap.get("copydata_action"));
    this.copydata_dtn.setText(resourceMap.getString("copydata_dtn.text", new Object[0]));
    this.copydata_dtn.setName("copydata_dtn");
    this.minsuma_edit.setText(resourceMap.getString("minsuma_edit.text", new Object[0]));
    this.minsuma_edit.setName("minsuma_edit");
    this.jLabel10.setText(resourceMap.getString("jLabel10.text", new Object[0]));
    this.jLabel10.setName("jLabel10");
    this.davnost_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Не изключвай нищо", "Без абсолютна давност", "Без протокол за отписване", "Без абс.давност и протокол", "Без 5 и 10год.давност и протокол" }));
    this.davnost_combo.setName("davnost_combo");
    this.jLabel13.setText(resourceMap.getString("jLabel13.text", new Object[0]));
    this.jLabel13.setName("jLabel13");
    this.nedd_by_year_check.setText(resourceMap.getString("nedd_by_year_check.text", new Object[0]));
    this.nedd_by_year_check.setName("nedd_by_year_check");
    this.jLabel7.setText(resourceMap.getString("jLabel7.text", new Object[0]));
    this.jLabel7.setName("jLabel7");
    this.mps_old_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Всички", "Само ако има имот/МПС за текущата годниа", "Само ако има същото МПС за текуща година", "НЕ" }));
    this.mps_old_combo.setName("mps_old_combo");
    this.mps_cur_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Всички", "Само ако има имот за текущата година", "НЕ" }));
    this.mps_cur_combo.setName("mps_cur_combo");
    this.jLabel9.setText(resourceMap.getString("jLabel9.text", new Object[0]));
    this.jLabel9.setName("jLabel9");
    this.imot_old_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Всички", "Само ако има имот/МПС за текуща година", "Само ако същия имот има облог за текуща година", "НЕ" }));
    this.imot_old_combo.setSelectedIndex(1);
    this.imot_old_combo.setName("imot_old_combo");
    this.jLabel12.setText(resourceMap.getString("jLabel12.text", new Object[0]));
    this.jLabel12.setName("jLabel12");
    this.paycode_edit.setText(resourceMap.getString("paycode_edit.text", new Object[0]));
    this.paycode_edit.setName("paycode_edit");
    this.other_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Всички", "Само ако има имот за текущата година", "Само ако има имот/МПС за текущата година", "НЕ" }));
    this.other_combo.setSelectedIndex(3);
    this.other_combo.setName("other_combo");
    this.addr_type_combo.setModel(new DefaultComboBoxModel<>(new String[] { "Настоящ", "Постоянен", "За кореспонденция" }));
    this.addr_type_combo.setSelectedIndex(2);
    this.addr_type_combo.setName("addr_type_combo");
    this.jLabel14.setText(resourceMap.getString("jLabel14.text", new Object[0]));
    this.jLabel14.setName("jLabel14");
    this.without_dead_check.setText(resourceMap.getString("without_dead_check.text", new Object[0]));
    this.without_dead_check.setName("without_dead_check");
    this.with_tbotext_check.setText(resourceMap.getString("with_tbotext_check.text", new Object[0]));
    this.with_tbotext_check.setName("with_tbotext_check");
    this.alternativeSendMsgCheck.setText(resourceMap.getString("alternativeSendMsgCheck.text", new Object[0]));
    this.alternativeSendMsgCheck.setName("alternativeSendMsgCheck");
    this.analyzeCheck.setSelected(true);
    this.analyzeCheck.setText(resourceMap.getString("analyzeCheck.text", new Object[0]));
    this.analyzeCheck.setName("analyzeCheck");
    this.davnostByYearCheck.setText(resourceMap.getString("davnostByYearCheck.text", new Object[0]));
    this.davnostByYearCheck.setName("davnostByYearCheck");
    GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
    this.jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addGap(22, 22, 22).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup().addComponent(this.copydata_dtn).addGap(19, 19, 19).addComponent(this.analyzeCheck).addGap(18, 18, 18).addComponent(this.without_dead_check).addGap(57, 57, 57).addComponent(this.with_tbotext_check)).addGroup(GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel13).addComponent(this.jLabel7)).addGap(50, 50, 50).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.mps_old_combo, 0, -1, 32767).addComponent(this.mps_cur_combo, 0, -1, 32767).addComponent(this.imot_old_combo, 0, 317, 32767).addComponent(this.other_combo, GroupLayout.Alignment.TRAILING, -2, 260, -2)).addGap(44, 44, 44).addComponent(this.davnostByYearCheck))).addContainerGap(-1, 32767)).addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel9).addGroup(jPanel2Layout.createSequentialGroup().addComponent(this.nedd_by_year_check).addGap(18, 18, 18).addComponent(this.davnost_combo, -2, 208, -2).addGap(18, 18, 18).addComponent(this.jLabel10, -2, 264, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.minsuma_edit, -2, 48, -2)).addGroup(jPanel2Layout.createSequentialGroup().addComponent(this.jLabel12).addGap(18, 18, 18).addComponent(this.paycode_edit, -2, 215, -2)).addGroup(jPanel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(this.jLabel14).addGap(18, 18, 18).addComponent(this.addr_type_combo, -2, 140, -2).addGap(43, 43, 43).addComponent(this.alternativeSendMsgCheck))).addGap(0, 0, 32767)))));
    jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.copydata_dtn).addComponent(this.without_dead_check).addComponent(this.with_tbotext_check).addComponent(this.analyzeCheck)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel13).addComponent(this.mps_cur_combo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.mps_old_combo, -2, -1, -2).addComponent(this.jLabel7)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel9).addComponent(this.imot_old_combo, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel12).addComponent(this.paycode_edit, -2, -1, -2).addComponent(this.other_combo, -2, -1, -2).addComponent(this.davnostByYearCheck)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 16, 32767).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.davnost_combo, -2, -1, -2).addComponent(this.jLabel10).addComponent(this.minsuma_edit, -2, -1, -2)).addComponent(this.nedd_by_year_check, GroupLayout.Alignment.TRAILING)).addGap(6, 6, 6).addGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.addr_type_combo, -2, -1, -2).addComponent(this.jLabel14).addComponent(this.alternativeSendMsgCheck)).addGap(19, 19, 19)));
    this.print_btn.setAction(applicationActionMap.get("print_action"));
    this.print_btn.setText(resourceMap.getString("print_btn.text", new Object[0]));
    this.print_btn.setName("print_btn");
    this.jLabel8.setText(resourceMap.getString("jLabel8.text", new Object[0]));
    this.jLabel8.setName("jLabel8");
    this.wpay_edit.setText(resourceMap.getString("wpay_edit.text", new Object[0]));
    this.wpay_edit.setName("wpay_edit");
    this.spr_check.setText(resourceMap.getString("spr_check.text", new Object[0]));
    this.spr_check.setName("spr_check");
    this.jLabel11.setText(resourceMap.getString("jLabel11.text", new Object[0]));
    this.jLabel11.setName("jLabel11");
    this.kasaadr_edit.setText(resourceMap.getString("kasaadr_edit.text", new Object[0]));
    this.kasaadr_edit.setName("kasaadr_edit");
    this.dataprocessingBtn.setAction(applicationActionMap.get("data_processing_action"));
    this.dataprocessingBtn.setText(resourceMap.getString("dataprocessingBtn.text", new Object[0]));
    this.dataprocessingBtn.setName("dataprocessingBtn");
    this.showPINCheck.setText(resourceMap.getString("showPINCheck.text", new Object[0]));
    this.showPINCheck.setName("showPINCheck");
    GroupLayout mainPanelLayout = new GroupLayout(this.mainPanel);
    this.mainPanel.setLayout(mainPanelLayout);
    mainPanelLayout.setHorizontalGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(mainPanelLayout.createSequentialGroup().addContainerGap().addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addGroup(mainPanelLayout.createSequentialGroup().addComponent(this.spr_check).addGap(31, 31, 31).addComponent(this.showPINCheck).addGap(0, 0, 32767)).addComponent(this.jPanel2, -1, -1, 32767).addGroup(GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup().addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(mainPanelLayout.createSequentialGroup().addGap(22, 22, 22).addComponent(this.dataprocessingBtn).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.print_btn)).addGroup(GroupLayout.Alignment.LEADING, mainPanelLayout.createSequentialGroup().addComponent(this.jLabel11).addGap(18, 18, 18).addComponent(this.kasaadr_edit)).addGroup(mainPanelLayout.createSequentialGroup().addComponent(this.jLabel8).addGap(18, 18, 18).addComponent(this.wpay_edit))).addGap(33, 33, 33))).addContainerGap()));
    mainPanelLayout.setVerticalGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(mainPanelLayout.createSequentialGroup().addContainerGap().addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel2, -1, -1, 32767).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.wpay_edit, -2, -1, -2).addComponent(this.jLabel8)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.spr_check).addComponent(this.showPINCheck)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(mainPanelLayout.createSequentialGroup().addComponent(this.jLabel11).addGap(29, 29, 29)).addGroup(GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup().addComponent(this.kasaadr_edit, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED))).addGroup(mainPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.dataprocessingBtn).addComponent(this.print_btn)).addContainerGap()));
    this.statusPanel.setName("statusPanel");
    statusPanelSeparator.setName("statusPanelSeparator");
    this.statusMessageLabel.setName("statusMessageLabel");
    this.statusAnimationLabel.setHorizontalAlignment(2);
    this.statusAnimationLabel.setName("statusAnimationLabel");
    this.progressBar.setName("progressBar");
    this.st_label.setText(resourceMap.getString("st_label.text", new Object[0]));
    this.st_label.setName("st_label");
    GroupLayout statusPanelLayout = new GroupLayout(this.statusPanel);
    this.statusPanel.setLayout(statusPanelLayout);
    statusPanelLayout.setHorizontalGroup(statusPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(statusPanelSeparator, -1, 808, 32767).addGroup(statusPanelLayout.createSequentialGroup().addContainerGap().addComponent(this.statusMessageLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.st_label, -2, 418, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(this.progressBar, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.statusAnimationLabel).addContainerGap()));
    statusPanelLayout.setVerticalGroup(statusPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(statusPanelLayout.createSequentialGroup().addComponent(statusPanelSeparator, -2, 2, -2).addGroup(statusPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(statusPanelLayout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(statusPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.statusMessageLabel).addComponent(this.statusAnimationLabel).addComponent(this.progressBar, -2, -1, -2)).addGap(3, 3, 3)).addGroup(statusPanelLayout.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.st_label).addContainerGap()))));
    setComponent(this.mainPanel);
    setStatusBar(this.statusPanel);
  }









  @Action
  public void dbconn_action() {
    if (this.oracon == null) {
      try {
        Class.forName("org.postgresql.Driver");
        this.oracon = DriverManager.getConnection("jdbc:postgresql://" + this.dbip_edit.getText().trim() + ":" + this.dbport_edit.getText().trim() + "/" + this.dbname_edit.getText().trim(), this.dbuser_edit.getText().trim(), this.dbpass_edit.getText().trim());
        this.oracon.setAutoCommit(false);
        readMuniData();
        this.dbconnect_btn.setText("Разкачане от БД");
      } catch (Exception ex) {
        this.oracon = null;
        ex.printStackTrace();
        JOptionPane.showMessageDialog(null, "Неуспеина връзка към базата данни.\n" + ex.getMessage());
      }
    } else {
      try {
        this.oracon.rollback();
        this.oracon.close();
      } catch (Exception ex) {
        JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
      } finally {
        this.oracon = null;
        this.dbconnect_btn.setText("Връзка с БД");
      }
    }
    getFrame().setTitle("Съобщения за " + this.msgYear);
  }

  @Action(block = Task.BlockingScope.WINDOW)
  public Task copydata_action() {
    return new Copydata_actionTask(getApplication());
  }

  private class Copydata_actionTask extends Task<Object, Void> {
    Copydata_actionTask(Application app) {
      super(app);
    }

    protected Object doInBackground() {
      return YearsaobView.this.copydata();
    }

    protected void succeeded(Object result) {}
  }

  @Action(block = Task.BlockingScope.WINDOW)
  public Task print_action() {
    return new Print_actionTask(getApplication());
  }

  private class Print_actionTask extends Task<Object, Void> {
    Print_actionTask(Application app) {
      super(app);
    }

    protected Object doInBackground() {
      YearsaobView.this.print();
      return null;
    }

    protected void succeeded(Object result) {}
  }

  @Action(block = Task.BlockingScope.WINDOW)
  public Task data_processing_action() {
    return new Data_processing_actionTask(getApplication());
  }

  private class Data_processing_actionTask extends Task<Object, Void> {
    Data_processing_actionTask(Application app) {
      super(app);
    }

    protected Object doInBackground() {
      return YearsaobView.this.dataPreProcessing();
    }

    protected void succeeded(Object result) {}
  }

  @Action
  public void singleMsgPrintAction() {
    String printPath = getJarFolder();
    Map<String, Object> params = new HashMap<>();
    params.put("msgyear", "" + this.msgYear);
    params.put("w3pay", this.wpay_edit.getText().trim());
    params.put("wwwspr", Boolean.valueOf(this.spr_check.isSelected()));
    params.put("show_pin", Boolean.valueOf(this.showPINCheck.isSelected()));
    params.put("wwwspr_address", this.webSprAddress);
    params.put("kasa_addr", this.kasaadr_edit.getText());
    params.put("IBAN", this.bankIban);
    params.put("banka", this.bankName);
    params.put("BIC", this.bankCode);
    params.put("ds_name", this.muniName);
    params.put("nomasking", Boolean.valueOf(false));
    try {
      PrintingHelperDAO printHelperDAO = new PrintingHelperDAO(this.oracon, this.msgYear);
      ReportParamsEntity reportParamsEntity = new ReportParamsEntity();
      reportParamsEntity.setVirtualizerPages(5000);
      reportParamsEntity.setPageSides(0);
      this.pagesHash = new HashMap<>();
      if (this.muniId != 1227L)
        if (this.muniId != 1057L)
          if (this.muniId != 1054L && this.muniId != 1060L) {
            String sql = "SELECT distinct l.taxsubject_id,l.ein,z.msgnom FROM dgp_msg.msg_lica l JOIN dgp_msg.msg_zad z ON l.taxsubject_id=z.taxsubject_id AND l.msgyear=z.msgyear WHERE l.msgyear=? AND l.ein='" + this.einText.getText().trim() + "' ";
            reportParamsEntity.setGroupSQL(sql);
            reportParamsEntity.setReportQuerySQLParamCount(2);
            reportParamsEntity.setReportTemplateName("saob_2");
            reportParamsEntity.setReportSQLFileName("print_saob_1.sql");
            printSinglePersonGroup("Печат съобщение за едно лице ", reportParamsEntity, printPath, false, params);
          }
      this.st_label.setText("Съобщенията са отпечатани");
    } catch (Exception ex) {
      ex.printStackTrace();
      JOptionPane.showMessageDialog(null, "Грешка: " + ex.getMessage());
    }
  }
}


/* Location:              D:\work\GS-Dobri\yearsaob2019.jar!\yearsaob\YearsaobView.class
 * Java compiler version: 7 (51.0)
 * JD-Core Version:       1.1.3
 */